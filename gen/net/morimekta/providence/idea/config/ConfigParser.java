// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class ConfigParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, EXTENDS_SETS_);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return config_file(b, l + 1);
  }

  public static final TokenSet[] EXTENDS_SETS_ = new TokenSet[] {
    create_token_set_(BINARY_B_64, BINARY_HEX, CONFIG, DEFINE_ENUM,
      DEFINE_VALUE, LIST, MAP, MAP_KEY,
      MESSAGE, PRIMITIVE, REF, REF_OR_ENUM,
      UNDEFINED, VALUE),
  };

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean alias(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "alias")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, ALIAS, r);
    return r;
  }

  /* ********************************************************** */
  // KW_B64 BINARY_START B64_CONTENT* BINARY_END
  public static boolean binary_b64(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "binary_b64")) return false;
    if (!nextTokenIs(b, KW_B64)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, BINARY_B_64, null);
    r = consumeTokens(b, 1, KW_B64, BINARY_START);
    p = r; // pin = 1
    r = r && report_error_(b, binary_b64_2(b, l + 1));
    r = p && consumeToken(b, BINARY_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // B64_CONTENT*
  private static boolean binary_b64_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "binary_b64_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!consumeToken(b, B64_CONTENT)) break;
      if (!empty_element_parsed_guard_(b, "binary_b64_2", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // KW_HEX BINARY_START HEX_CONTENT? BINARY_END
  public static boolean binary_hex(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "binary_hex")) return false;
    if (!nextTokenIs(b, KW_HEX)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, BINARY_HEX, null);
    r = consumeTokens(b, 1, KW_HEX, BINARY_START);
    p = r; // pin = 1
    r = r && report_error_(b, binary_hex_2(b, l + 1));
    r = p && consumeToken(b, BINARY_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // HEX_CONTENT?
  private static boolean binary_hex_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "binary_hex_2")) return false;
    consumeToken(b, HEX_CONTENT);
    return true;
  }

  /* ********************************************************** */
  // config_type (COLON extends)? message
  public static boolean config(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CONFIG, null);
    r = config_type(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, config_1(b, l + 1));
    r = p && message(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (COLON extends)?
  private static boolean config_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_1")) return false;
    config_1_0(b, l + 1);
    return true;
  }

  // COLON extends
  private static boolean config_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON);
    r = r && extends_$(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (COMMENT | include)* (COMMENT | define_spec)* config COMMENT*
  static boolean config_file(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_file")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = config_file_0(b, l + 1);
    r = r && config_file_1(b, l + 1);
    r = r && config(b, l + 1);
    r = r && config_file_3(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (COMMENT | include)*
  private static boolean config_file_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_file_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!config_file_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "config_file_0", c)) break;
    }
    return true;
  }

  // COMMENT | include
  private static boolean config_file_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_file_0_0")) return false;
    boolean r;
    r = consumeToken(b, COMMENT);
    if (!r) r = include(b, l + 1);
    return r;
  }

  // (COMMENT | define_spec)*
  private static boolean config_file_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_file_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!config_file_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "config_file_1", c)) break;
    }
    return true;
  }

  // COMMENT | define_spec
  private static boolean config_file_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_file_1_0")) return false;
    boolean r;
    r = consumeToken(b, COMMENT);
    if (!r) r = define_spec(b, l + 1);
    return r;
  }

  // COMMENT*
  private static boolean config_file_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_file_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!consumeToken(b, COMMENT)) break;
      if (!empty_element_parsed_guard_(b, "config_file_3", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // program_name DOT type_name
  public static boolean config_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "config_type")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = program_name(b, l + 1);
    r = r && consumeToken(b, DOT);
    r = r && type_name(b, l + 1);
    exit_section_(b, m, CONFIG_TYPE, r);
    return r;
  }

  /* ********************************************************** */
  // alias EQUALS define_value
  public static boolean define(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "define")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, DEFINE, null);
    r = alias(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, consumeToken(b, EQUALS));
    r = p && define_value(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // program_name DOT type_name DOT enum_value
  public static boolean define_enum(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "define_enum")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, DEFINE_ENUM, null);
    r = program_name(b, l + 1);
    r = r && consumeToken(b, DOT);
    r = r && type_name(b, l + 1);
    r = r && consumeToken(b, DOT);
    p = r; // pin = 4
    r = r && enum_value(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // MAP_START defines MAP_END
  public static boolean define_map(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "define_map")) return false;
    if (!nextTokenIs(b, MAP_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, DEFINE_MAP, null);
    r = consumeToken(b, MAP_START);
    p = r; // pin = 1
    r = r && report_error_(b, defines(b, l + 1));
    r = p && consumeToken(b, MAP_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // KW_DEF (define | define_map)
  public static boolean define_spec(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "define_spec")) return false;
    if (!nextTokenIs(b, KW_DEF)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, DEFINE_SPEC, null);
    r = consumeToken(b, KW_DEF);
    p = r; // pin = 1
    r = r && define_spec_1(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // define | define_map
  private static boolean define_spec_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "define_spec_1")) return false;
    boolean r;
    r = define(b, l + 1);
    if (!r) r = define_map(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // define_enum |
  //     config |
  //     map |
  //     list |
  //     primitive |
  //     ref
  public static boolean define_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "define_value")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, DEFINE_VALUE, "<define value>");
    r = define_enum(b, l + 1);
    if (!r) r = config(b, l + 1);
    if (!r) r = map(b, l + 1);
    if (!r) r = list(b, l + 1);
    if (!r) r = primitive(b, l + 1);
    if (!r) r = ref(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // (COMMENT | define)*
  public static boolean defines(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "defines")) return false;
    Marker m = enter_section_(b, l, _NONE_, DEFINES, "<defines>");
    while (true) {
      int c = current_position_(b);
      if (!defines_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "defines", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // COMMENT | define
  private static boolean defines_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "defines_0")) return false;
    boolean r;
    r = consumeToken(b, COMMENT);
    if (!r) r = define(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean enum_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_value")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, ENUM_VALUE, r);
    return r;
  }

  /* ********************************************************** */
  // ref_id (DOT ref_path)?
  public static boolean extends_$(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "extends_$")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ref_id(b, l + 1);
    r = r && extends_1(b, l + 1);
    exit_section_(b, m, EXTENDS, r);
    return r;
  }

  // (DOT ref_path)?
  private static boolean extends_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "extends_1")) return false;
    extends_1_0(b, l + 1);
    return true;
  }

  // DOT ref_path
  private static boolean extends_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "extends_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, DOT);
    r = r && ref_path(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // field_name reusable_def? (
  //                (message | map) |
  //                EQUALS extends (message | map) |
  //                EQUALS (undefined | value)
  //     ) SEMICOLON?
  public static boolean field(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, FIELD, null);
    r = field_name(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, field_1(b, l + 1));
    r = p && report_error_(b, field_2(b, l + 1)) && r;
    r = p && field_3(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // reusable_def?
  private static boolean field_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_1")) return false;
    reusable_def(b, l + 1);
    return true;
  }

  // (message | map) |
  //                EQUALS extends (message | map) |
  //                EQUALS (undefined | value)
  private static boolean field_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = field_2_0(b, l + 1);
    if (!r) r = field_2_1(b, l + 1);
    if (!r) r = field_2_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // message | map
  private static boolean field_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_2_0")) return false;
    boolean r;
    r = message(b, l + 1);
    if (!r) r = map(b, l + 1);
    return r;
  }

  // EQUALS extends (message | map)
  private static boolean field_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_2_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EQUALS);
    r = r && extends_$(b, l + 1);
    r = r && field_2_1_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // message | map
  private static boolean field_2_1_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_2_1_2")) return false;
    boolean r;
    r = message(b, l + 1);
    if (!r) r = map(b, l + 1);
    return r;
  }

  // EQUALS (undefined | value)
  private static boolean field_2_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_2_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EQUALS);
    r = r && field_2_2_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // undefined | value
  private static boolean field_2_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_2_2_1")) return false;
    boolean r;
    r = undefined(b, l + 1);
    if (!r) r = value(b, l + 1);
    return r;
  }

  // SEMICOLON?
  private static boolean field_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_3")) return false;
    consumeToken(b, SEMICOLON);
    return true;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean field_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, FIELD_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // (COMMENT | field)*
  public static boolean fields(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fields")) return false;
    Marker m = enter_section_(b, l, _NONE_, FIELDS, "<fields>");
    while (true) {
      int c = current_position_(b);
      if (!fields_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "fields", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // COMMENT | field
  private static boolean fields_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fields_0")) return false;
    boolean r;
    r = consumeToken(b, COMMENT);
    if (!r) r = field(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // KW_INCLUDE included_file KW_AS alias
  public static boolean include(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "include")) return false;
    if (!nextTokenIs(b, KW_INCLUDE)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, INCLUDE, null);
    r = consumeToken(b, KW_INCLUDE);
    p = r; // pin = 1
    r = r && report_error_(b, included_file(b, l + 1));
    r = p && report_error_(b, consumeToken(b, KW_AS)) && r;
    r = p && alias(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // STRING
  public static boolean included_file(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "included_file")) return false;
    if (!nextTokenIs(b, STRING)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, STRING);
    exit_section_(b, m, INCLUDED_FILE, r);
    return r;
  }

  /* ********************************************************** */
  // LIST_START list_items LIST_END
  public static boolean list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "list")) return false;
    if (!nextTokenIs(b, LIST_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, LIST, null);
    r = consumeToken(b, LIST_START);
    p = r; // pin = 1
    r = r && report_error_(b, list_items(b, l + 1));
    r = p && consumeToken(b, LIST_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // (COMMENT | value COMMA?)*
  public static boolean list_items(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "list_items")) return false;
    Marker m = enter_section_(b, l, _NONE_, LIST_ITEMS, "<list items>");
    while (true) {
      int c = current_position_(b);
      if (!list_items_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "list_items", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // COMMENT | value COMMA?
  private static boolean list_items_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "list_items_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMENT);
    if (!r) r = list_items_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // value COMMA?
  private static boolean list_items_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "list_items_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = value(b, l + 1);
    r = r && list_items_0_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // COMMA?
  private static boolean list_items_0_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "list_items_0_1_1")) return false;
    consumeToken(b, COMMA);
    return true;
  }

  /* ********************************************************** */
  // MAP_START map_entries MAP_END
  public static boolean map(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map")) return false;
    if (!nextTokenIs(b, MAP_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, MAP, null);
    r = consumeToken(b, MAP_START);
    p = r; // pin = 1
    r = r && report_error_(b, map_entries(b, l + 1));
    r = p && consumeToken(b, MAP_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // (COMMENT | map_entry (COMMA | SEMICOLON)?)*
  public static boolean map_entries(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_entries")) return false;
    Marker m = enter_section_(b, l, _NONE_, MAP_ENTRIES, "<map entries>");
    while (true) {
      int c = current_position_(b);
      if (!map_entries_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "map_entries", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // COMMENT | map_entry (COMMA | SEMICOLON)?
  private static boolean map_entries_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_entries_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMENT);
    if (!r) r = map_entries_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // map_entry (COMMA | SEMICOLON)?
  private static boolean map_entries_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_entries_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = map_entry(b, l + 1);
    r = r && map_entries_0_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (COMMA | SEMICOLON)?
  private static boolean map_entries_0_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_entries_0_1_1")) return false;
    map_entries_0_1_1_0(b, l + 1);
    return true;
  }

  // COMMA | SEMICOLON
  private static boolean map_entries_0_1_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_entries_0_1_1_0")) return false;
    boolean r;
    r = consumeToken(b, COMMA);
    if (!r) r = consumeToken(b, SEMICOLON);
    return r;
  }

  /* ********************************************************** */
  // map_key COLON (undefined | value)
  public static boolean map_entry(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_entry")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, MAP_ENTRY, "<map entry>");
    r = map_key(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, consumeToken(b, COLON));
    r = p && map_entry_2(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // undefined | value
  private static boolean map_entry_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_entry_2")) return false;
    boolean r;
    r = undefined(b, l + 1);
    if (!r) r = value(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // primitive | ref_or_enum
  public static boolean map_key(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_key")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, MAP_KEY, "<map key>");
    r = primitive(b, l + 1);
    if (!r) r = ref_or_enum(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // MAP_START fields MAP_END
  public static boolean message(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message")) return false;
    if (!nextTokenIs(b, MAP_START)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, MAP_START);
    r = r && fields(b, l + 1);
    r = r && consumeToken(b, MAP_END);
    exit_section_(b, m, MESSAGE, r);
    return r;
  }

  /* ********************************************************** */
  // KW_TRUE |
  //     KW_FALSE |
  //     NUMBER |
  //     STRING |
  //     binary_b64 |
  //     binary_hex
  public static boolean primitive(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primitive")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, PRIMITIVE, "<primitive>");
    r = consumeToken(b, KW_TRUE);
    if (!r) r = consumeToken(b, KW_FALSE);
    if (!r) r = consumeToken(b, NUMBER);
    if (!r) r = consumeToken(b, STRING);
    if (!r) r = binary_b64(b, l + 1);
    if (!r) r = binary_hex(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean program_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "program_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, PROGRAM_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // ref_id (DOT ref_path)?
  public static boolean ref(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ref_id(b, l + 1);
    r = r && ref_1(b, l + 1);
    exit_section_(b, m, REF, r);
    return r;
  }

  // (DOT ref_path)?
  private static boolean ref_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_1")) return false;
    ref_1_0(b, l + 1);
    return true;
  }

  // DOT ref_path
  private static boolean ref_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, DOT);
    r = r && ref_path(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean ref_field(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_field")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, REF_FIELD, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean ref_id(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_id")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, REF_ID, r);
    return r;
  }

  /* ********************************************************** */
  // ref_id (DOT ref_path)?
  public static boolean ref_or_enum(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_or_enum")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ref_id(b, l + 1);
    r = r && ref_or_enum_1(b, l + 1);
    exit_section_(b, m, REF_OR_ENUM, r);
    return r;
  }

  // (DOT ref_path)?
  private static boolean ref_or_enum_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_or_enum_1")) return false;
    ref_or_enum_1_0(b, l + 1);
    return true;
  }

  // DOT ref_path
  private static boolean ref_or_enum_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_or_enum_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, DOT);
    r = r && ref_path(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // ref_field (DOT ref_path)?
  public static boolean ref_path(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_path")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ref_field(b, l + 1);
    r = r && ref_path_1(b, l + 1);
    exit_section_(b, m, REF_PATH, r);
    return r;
  }

  // (DOT ref_path)?
  private static boolean ref_path_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_path_1")) return false;
    ref_path_1_0(b, l + 1);
    return true;
  }

  // DOT ref_path
  private static boolean ref_path_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ref_path_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, DOT);
    r = r && ref_path(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // REUSABLE alias
  public static boolean reusable_def(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "reusable_def")) return false;
    if (!nextTokenIs(b, REUSABLE)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, REUSABLE_DEF, null);
    r = consumeToken(b, REUSABLE);
    p = r; // pin = 1
    r = r && alias(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean type_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, TYPE_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // KW_UNDEFINED
  public static boolean undefined(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "undefined")) return false;
    if (!nextTokenIs(b, KW_UNDEFINED)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_UNDEFINED);
    exit_section_(b, m, UNDEFINED, r);
    return r;
  }

  /* ********************************************************** */
  // primitive |
  //     ref_or_enum |
  //     message |
  //     map |
  //     list
  public static boolean value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "value")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, VALUE, "<value>");
    r = primitive(b, l + 1);
    if (!r) r = ref_or_enum(b, l + 1);
    if (!r) r = message(b, l + 1);
    if (!r) r = map(b, l + 1);
    if (!r) r = list(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

}
