// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ConfigDefineEnum extends ConfigValue {

  @Nullable
  ConfigEnumValue getEnumValue();

  @NotNull
  ConfigProgramName getProgramName();

  @NotNull
  ConfigTypeName getTypeName();

}
