// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;

public interface ConfigFieldName extends ConfigFieldNameElement {

  String getName();

  PsiElement setName(String name);

  boolean hasPossibleContainedType();

  List<ThriftFieldType> getPossibleFields();

}
