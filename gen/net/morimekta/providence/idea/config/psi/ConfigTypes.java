// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import net.morimekta.providence.idea.config.psi.impl.*;

public interface ConfigTypes {

  IElementType ALIAS = new ConfigElementType("ALIAS");
  IElementType BINARY_B_64 = new ConfigElementType("BINARY_B_64");
  IElementType BINARY_HEX = new ConfigElementType("BINARY_HEX");
  IElementType CONFIG = new ConfigElementType("CONFIG");
  IElementType CONFIG_TYPE = new ConfigElementType("CONFIG_TYPE");
  IElementType DEFINE = new ConfigElementType("DEFINE");
  IElementType DEFINES = new ConfigElementType("DEFINES");
  IElementType DEFINE_ENUM = new ConfigElementType("DEFINE_ENUM");
  IElementType DEFINE_MAP = new ConfigElementType("DEFINE_MAP");
  IElementType DEFINE_SPEC = new ConfigElementType("DEFINE_SPEC");
  IElementType DEFINE_VALUE = new ConfigElementType("DEFINE_VALUE");
  IElementType ENUM_VALUE = new ConfigElementType("ENUM_VALUE");
  IElementType EXTENDS = new ConfigElementType("EXTENDS");
  IElementType FIELD = new ConfigElementType("FIELD");
  IElementType FIELDS = new ConfigElementType("FIELDS");
  IElementType FIELD_NAME = new ConfigElementType("FIELD_NAME");
  IElementType INCLUDE = new ConfigElementType("INCLUDE");
  IElementType INCLUDED_FILE = new ConfigElementType("INCLUDED_FILE");
  IElementType LIST = new ConfigElementType("LIST");
  IElementType LIST_ITEMS = new ConfigElementType("LIST_ITEMS");
  IElementType MAP = new ConfigElementType("MAP");
  IElementType MAP_ENTRIES = new ConfigElementType("MAP_ENTRIES");
  IElementType MAP_ENTRY = new ConfigElementType("MAP_ENTRY");
  IElementType MAP_KEY = new ConfigElementType("MAP_KEY");
  IElementType MESSAGE = new ConfigElementType("MESSAGE");
  IElementType PRIMITIVE = new ConfigElementType("PRIMITIVE");
  IElementType PROGRAM_NAME = new ConfigElementType("PROGRAM_NAME");
  IElementType REF = new ConfigElementType("REF");
  IElementType REF_FIELD = new ConfigElementType("REF_FIELD");
  IElementType REF_ID = new ConfigElementType("REF_ID");
  IElementType REF_OR_ENUM = new ConfigElementType("REF_OR_ENUM");
  IElementType REF_PATH = new ConfigElementType("REF_PATH");
  IElementType REUSABLE_DEF = new ConfigElementType("REUSABLE_DEF");
  IElementType TYPE_NAME = new ConfigElementType("TYPE_NAME");
  IElementType UNDEFINED = new ConfigElementType("UNDEFINED");
  IElementType VALUE = new ConfigElementType("VALUE");

  IElementType B64_CONTENT = new ConfigTokenType("B64_CONTENT");
  IElementType BINARY_END = new ConfigTokenType("BINARY_END");
  IElementType BINARY_START = new ConfigTokenType("BINARY_START");
  IElementType COLON = new ConfigTokenType("COLON");
  IElementType COMMA = new ConfigTokenType("COMMA");
  IElementType COMMENT = new ConfigTokenType("COMMENT");
  IElementType DOT = new ConfigTokenType("DOT");
  IElementType EQUALS = new ConfigTokenType("EQUALS");
  IElementType HEX_CONTENT = new ConfigTokenType("HEX_CONTENT");
  IElementType IDENTIFIER = new ConfigTokenType("IDENTIFIER");
  IElementType KW_AS = new ConfigTokenType("KW_AS");
  IElementType KW_B64 = new ConfigTokenType("KW_B64");
  IElementType KW_DEF = new ConfigTokenType("KW_DEF");
  IElementType KW_FALSE = new ConfigTokenType("KW_FALSE");
  IElementType KW_HEX = new ConfigTokenType("KW_HEX");
  IElementType KW_INCLUDE = new ConfigTokenType("KW_INCLUDE");
  IElementType KW_TRUE = new ConfigTokenType("KW_TRUE");
  IElementType KW_UNDEFINED = new ConfigTokenType("KW_UNDEFINED");
  IElementType LIST_END = new ConfigTokenType("LIST_END");
  IElementType LIST_START = new ConfigTokenType("LIST_START");
  IElementType MAP_END = new ConfigTokenType("MAP_END");
  IElementType MAP_START = new ConfigTokenType("MAP_START");
  IElementType NUMBER = new ConfigTokenType("NUMBER");
  IElementType REUSABLE = new ConfigTokenType("REUSABLE");
  IElementType SEMICOLON = new ConfigTokenType("SEMICOLON");
  IElementType STRING = new ConfigTokenType("STRING");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ALIAS) {
        return new ConfigAliasImpl(node);
      }
      else if (type == BINARY_B_64) {
        return new ConfigBinaryB64Impl(node);
      }
      else if (type == BINARY_HEX) {
        return new ConfigBinaryHexImpl(node);
      }
      else if (type == CONFIG) {
        return new ConfigConfigImpl(node);
      }
      else if (type == CONFIG_TYPE) {
        return new ConfigConfigTypeImpl(node);
      }
      else if (type == DEFINE) {
        return new ConfigDefineImpl(node);
      }
      else if (type == DEFINES) {
        return new ConfigDefinesImpl(node);
      }
      else if (type == DEFINE_ENUM) {
        return new ConfigDefineEnumImpl(node);
      }
      else if (type == DEFINE_MAP) {
        return new ConfigDefineMapImpl(node);
      }
      else if (type == DEFINE_SPEC) {
        return new ConfigDefineSpecImpl(node);
      }
      else if (type == ENUM_VALUE) {
        return new ConfigEnumValueImpl(node);
      }
      else if (type == EXTENDS) {
        return new ConfigExtendsImpl(node);
      }
      else if (type == FIELD) {
        return new ConfigFieldImpl(node);
      }
      else if (type == FIELDS) {
        return new ConfigFieldsImpl(node);
      }
      else if (type == FIELD_NAME) {
        return new ConfigFieldNameImpl(node);
      }
      else if (type == INCLUDE) {
        return new ConfigIncludeImpl(node);
      }
      else if (type == INCLUDED_FILE) {
        return new ConfigIncludedFileImpl(node);
      }
      else if (type == LIST) {
        return new ConfigListImpl(node);
      }
      else if (type == LIST_ITEMS) {
        return new ConfigListItemsImpl(node);
      }
      else if (type == MAP) {
        return new ConfigMapImpl(node);
      }
      else if (type == MAP_ENTRIES) {
        return new ConfigMapEntriesImpl(node);
      }
      else if (type == MAP_ENTRY) {
        return new ConfigMapEntryImpl(node);
      }
      else if (type == MESSAGE) {
        return new ConfigMessageImpl(node);
      }
      else if (type == PRIMITIVE) {
        return new ConfigPrimitiveImpl(node);
      }
      else if (type == PROGRAM_NAME) {
        return new ConfigProgramNameImpl(node);
      }
      else if (type == REF) {
        return new ConfigRefImpl(node);
      }
      else if (type == REF_FIELD) {
        return new ConfigRefFieldImpl(node);
      }
      else if (type == REF_ID) {
        return new ConfigRefIdImpl(node);
      }
      else if (type == REF_OR_ENUM) {
        return new ConfigRefOrEnumImpl(node);
      }
      else if (type == REF_PATH) {
        return new ConfigRefPathImpl(node);
      }
      else if (type == REUSABLE_DEF) {
        return new ConfigReusableDefImpl(node);
      }
      else if (type == TYPE_NAME) {
        return new ConfigTypeNameImpl(node);
      }
      else if (type == UNDEFINED) {
        return new ConfigUndefinedImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
