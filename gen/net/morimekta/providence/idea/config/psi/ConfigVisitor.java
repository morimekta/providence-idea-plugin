// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class ConfigVisitor extends PsiElementVisitor {

  public void visitAlias(@NotNull ConfigAlias o) {
    visitPsiElement(o);
  }

  public void visitBinaryB64(@NotNull ConfigBinaryB64 o) {
    visitPrimitive(o);
  }

  public void visitBinaryHex(@NotNull ConfigBinaryHex o) {
    visitPrimitive(o);
  }

  public void visitConfig(@NotNull ConfigConfig o) {
    visitValue(o);
  }

  public void visitConfigType(@NotNull ConfigConfigType o) {
    visitPsiElement(o);
  }

  public void visitDefine(@NotNull ConfigDefine o) {
    visitPsiElement(o);
  }

  public void visitDefineEnum(@NotNull ConfigDefineEnum o) {
    visitValue(o);
  }

  public void visitDefineMap(@NotNull ConfigDefineMap o) {
    visitPsiElement(o);
  }

  public void visitDefineSpec(@NotNull ConfigDefineSpec o) {
    visitPsiElement(o);
  }

  public void visitDefineValue(@NotNull ConfigDefineValue o) {
    visitValue(o);
  }

  public void visitDefines(@NotNull ConfigDefines o) {
    visitPsiElement(o);
  }

  public void visitEnumValue(@NotNull ConfigEnumValue o) {
    visitPsiElement(o);
  }

  public void visitExtends(@NotNull ConfigExtends o) {
    visitPsiElement(o);
  }

  public void visitField(@NotNull ConfigField o) {
    visitFieldElement(o);
  }

  public void visitFieldName(@NotNull ConfigFieldName o) {
    visitFieldNameElement(o);
  }

  public void visitFields(@NotNull ConfigFields o) {
    visitPsiElement(o);
  }

  public void visitInclude(@NotNull ConfigInclude o) {
    visitPsiElement(o);
  }

  public void visitIncludedFile(@NotNull ConfigIncludedFile o) {
    visitIncludedFileElement(o);
  }

  public void visitList(@NotNull ConfigList o) {
    visitValue(o);
  }

  public void visitListItems(@NotNull ConfigListItems o) {
    visitPsiElement(o);
  }

  public void visitMap(@NotNull ConfigMap o) {
    visitValue(o);
  }

  public void visitMapEntries(@NotNull ConfigMapEntries o) {
    visitPsiElement(o);
  }

  public void visitMapEntry(@NotNull ConfigMapEntry o) {
    visitPsiElement(o);
  }

  public void visitMapKey(@NotNull ConfigMapKey o) {
    visitValue(o);
  }

  public void visitMessage(@NotNull ConfigMessage o) {
    visitValue(o);
    // visitMessageElement(o);
  }

  public void visitPrimitive(@NotNull ConfigPrimitive o) {
    visitValue(o);
  }

  public void visitProgramName(@NotNull ConfigProgramName o) {
    visitPsiElement(o);
  }

  public void visitRef(@NotNull ConfigRef o) {
    visitValue(o);
  }

  public void visitRefField(@NotNull ConfigRefField o) {
    visitPsiElement(o);
  }

  public void visitRefId(@NotNull ConfigRefId o) {
    visitPsiElement(o);
  }

  public void visitRefOrEnum(@NotNull ConfigRefOrEnum o) {
    visitValue(o);
  }

  public void visitRefPath(@NotNull ConfigRefPath o) {
    visitPsiElement(o);
  }

  public void visitReusableDef(@NotNull ConfigReusableDef o) {
    visitPsiElement(o);
  }

  public void visitTypeName(@NotNull ConfigTypeName o) {
    visitTypeNameElement(o);
  }

  public void visitUndefined(@NotNull ConfigUndefined o) {
    visitValue(o);
  }

  public void visitValue(@NotNull ConfigValue o) {
    visitPsiElement(o);
  }

  public void visitFieldElement(@NotNull ConfigFieldElement o) {
    visitPsiElement(o);
  }

  public void visitFieldNameElement(@NotNull ConfigFieldNameElement o) {
    visitPsiElement(o);
  }

  public void visitIncludedFileElement(@NotNull ConfigIncludedFileElement o) {
    visitPsiElement(o);
  }

  public void visitTypeNameElement(@NotNull ConfigTypeNameElement o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
