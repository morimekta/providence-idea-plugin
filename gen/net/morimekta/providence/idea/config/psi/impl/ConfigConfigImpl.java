// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.*;
import net.morimekta.providence.idea.config.psi.*;

public class ConfigConfigImpl extends ConfigValueImpl implements ConfigConfig {

  public ConfigConfigImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ConfigVisitor visitor) {
    visitor.visitConfig(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ConfigVisitor) accept((ConfigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public ConfigConfigType getConfigType() {
    return findNotNullChildByClass(ConfigConfigType.class);
  }

  @Override
  @Nullable
  public ConfigExtends getExtends() {
    return findChildByClass(ConfigExtends.class);
  }

  @Override
  @Nullable
  public ConfigMessage getMessage() {
    return findChildByClass(ConfigMessage.class);
  }

}
