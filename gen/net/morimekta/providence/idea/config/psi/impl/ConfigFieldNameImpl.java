// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.*;
import net.morimekta.providence.idea.config.psi.*;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;

public class ConfigFieldNameImpl extends ConfigFieldNameElementImpl implements ConfigFieldName {

  public ConfigFieldNameImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ConfigVisitor visitor) {
    visitor.visitFieldName(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ConfigVisitor) accept((ConfigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  public String getName() {
    return ConfigPsiImplUtil.getName(this);
  }

  @Override
  public PsiElement setName(String name) {
    return ConfigPsiImplUtil.setName(this, name);
  }

  @Override
  public boolean hasPossibleContainedType() {
    return ConfigPsiImplUtil.hasPossibleContainedType(this);
  }

  @Override
  public List<ThriftFieldType> getPossibleFields() {
    return ConfigPsiImplUtil.getPossibleFields(this);
  }

}
