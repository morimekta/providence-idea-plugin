// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.*;
import net.morimekta.providence.idea.config.psi.*;

public class ConfigIncludedFileImpl extends ConfigIncludedFileElementImpl implements ConfigIncludedFile {

  public ConfigIncludedFileImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ConfigVisitor visitor) {
    visitor.visitIncludedFile(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ConfigVisitor) accept((ConfigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  public String getName() {
    return ConfigPsiImplUtil.getName(this);
  }

  @Override
  public PsiElement setName(String name) {
    return ConfigPsiImplUtil.setName(this, name);
  }

}
