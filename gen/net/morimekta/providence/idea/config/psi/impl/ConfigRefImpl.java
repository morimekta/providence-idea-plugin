// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.*;
import net.morimekta.providence.idea.config.psi.*;

public class ConfigRefImpl extends ConfigValueImpl implements ConfigRef {

  public ConfigRefImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ConfigVisitor visitor) {
    visitor.visitRef(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ConfigVisitor) accept((ConfigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public ConfigRefId getRefId() {
    return findNotNullChildByClass(ConfigRefId.class);
  }

  @Override
  @Nullable
  public ConfigRefPath getRefPath() {
    return findChildByClass(ConfigRefPath.class);
  }

}
