// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.config.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import net.morimekta.providence.idea.config.psi.*;

public abstract class ConfigValueImpl extends ASTWrapperPsiElement implements ConfigValue {

  public ConfigValueImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ConfigVisitor visitor) {
    visitor.visitValue(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ConfigVisitor) accept((ConfigVisitor)visitor);
    else super.accept(visitor);
  }

}
