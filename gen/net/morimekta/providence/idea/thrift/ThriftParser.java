// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class ThriftParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, EXTENDS_SETS_);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return thrift_file(b, l + 1);
  }

  public static final TokenSet[] EXTENDS_SETS_ = new TokenSet[] {
    create_token_set_(FIELD, MESSAGE_FIELD, METHOD_PARAM),
    create_token_set_(CONST, ENUM, MESSAGE, SERVICE,
      SPEC, TYPEDEF),
    create_token_set_(KEY_TYPE, LIST_TYPE, MAP_TYPE, PRIMITIVE_TYPE,
      TYPE, TYPE_REFERENCE),
    create_token_set_(CONST_LIST, CONST_MAP, CONST_MAP_KEY, CONST_NULL,
      CONST_PRIMITIVE, CONST_REFERENCE, CONST_VALUE),
  };

  /* ********************************************************** */
  // annotation_key (EQUALS annotation_value)?
  public static boolean annotation_entry(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation_entry")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, ANNOTATION_ENTRY, null);
    r = annotation_key(b, l + 1);
    p = r; // pin = 1
    r = r && annotation_entry_1(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (EQUALS annotation_value)?
  private static boolean annotation_entry_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation_entry_1")) return false;
    annotation_entry_1_0(b, l + 1);
    return true;
  }

  // EQUALS annotation_value
  private static boolean annotation_entry_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation_entry_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EQUALS);
    r = r && annotation_value(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER (DOT IDENTIFIER)*
  public static boolean annotation_key(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation_key")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    r = r && annotation_key_1(b, l + 1);
    exit_section_(b, m, ANNOTATION_KEY, r);
    return r;
  }

  // (DOT IDENTIFIER)*
  private static boolean annotation_key_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation_key_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!annotation_key_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "annotation_key_1", c)) break;
    }
    return true;
  }

  // DOT IDENTIFIER
  private static boolean annotation_key_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation_key_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, DOT, IDENTIFIER);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // LITERAL
  public static boolean annotation_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation_value")) return false;
    if (!nextTokenIs(b, LITERAL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LITERAL);
    exit_section_(b, m, ANNOTATION_VALUE, r);
    return r;
  }

  /* ********************************************************** */
  // PARAM_START (annotation_entry COMMA | comment)* annotation_entry comment* PARAM_END
  public static boolean annotations(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotations")) return false;
    if (!nextTokenIs(b, PARAM_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, ANNOTATIONS, null);
    r = consumeToken(b, PARAM_START);
    p = r; // pin = 1
    r = r && report_error_(b, annotations_1(b, l + 1));
    r = p && report_error_(b, annotation_entry(b, l + 1)) && r;
    r = p && report_error_(b, annotations_3(b, l + 1)) && r;
    r = p && consumeToken(b, PARAM_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (annotation_entry COMMA | comment)*
  private static boolean annotations_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotations_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!annotations_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "annotations_1", c)) break;
    }
    return true;
  }

  // annotation_entry COMMA | comment
  private static boolean annotations_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotations_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = annotations_1_0_0(b, l + 1);
    if (!r) r = comment(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // annotation_entry COMMA
  private static boolean annotations_1_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotations_1_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = annotation_entry(b, l + 1);
    r = r && consumeToken(b, COMMA);
    exit_section_(b, m, null, r);
    return r;
  }

  // comment*
  private static boolean annotations_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotations_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!comment(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "annotations_3", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // LINE_COMMENT | DOC_COMMENT
  public static boolean comment(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "comment")) return false;
    if (!nextTokenIs(b, "<comment>", DOC_COMMENT, LINE_COMMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, COMMENT, "<comment>");
    r = consumeToken(b, LINE_COMMENT);
    if (!r) r = consumeToken(b, DOC_COMMENT);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // KW_CONST type const_name EQUALS const_value SEMICOLON?
  public static boolean const_$(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_$")) return false;
    if (!nextTokenIs(b, KW_CONST)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CONST, null);
    r = consumeToken(b, KW_CONST);
    p = r; // pin = 1
    r = r && report_error_(b, type(b, l + 1));
    r = p && report_error_(b, const_name(b, l + 1)) && r;
    r = p && report_error_(b, consumeToken(b, EQUALS)) && r;
    r = p && report_error_(b, const_value(b, l + 1)) && r;
    r = p && const_5(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // SEMICOLON?
  private static boolean const_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_5")) return false;
    consumeToken(b, SEMICOLON);
    return true;
  }

  /* ********************************************************** */
  // LIST_START (comment | const_list_entry)* LIST_END
  public static boolean const_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_list")) return false;
    if (!nextTokenIs(b, LIST_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CONST_LIST, null);
    r = consumeToken(b, LIST_START);
    p = r; // pin = 1
    r = r && report_error_(b, const_list_1(b, l + 1));
    r = p && consumeToken(b, LIST_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (comment | const_list_entry)*
  private static boolean const_list_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_list_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!const_list_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "const_list_1", c)) break;
    }
    return true;
  }

  // comment | const_list_entry
  private static boolean const_list_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_list_1_0")) return false;
    boolean r;
    r = comment(b, l + 1);
    if (!r) r = const_list_entry(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // const_value (COMMA | SEMICOLON)?
  public static boolean const_list_entry(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_list_entry")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CONST_LIST_ENTRY, "<const list entry>");
    r = const_value(b, l + 1);
    r = r && const_list_entry_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (COMMA | SEMICOLON)?
  private static boolean const_list_entry_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_list_entry_1")) return false;
    const_list_entry_1_0(b, l + 1);
    return true;
  }

  // COMMA | SEMICOLON
  private static boolean const_list_entry_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_list_entry_1_0")) return false;
    boolean r;
    r = consumeToken(b, COMMA);
    if (!r) r = consumeToken(b, SEMICOLON);
    return r;
  }

  /* ********************************************************** */
  // MAP_START (comment | const_map_entry)* MAP_END
  public static boolean const_map(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_map")) return false;
    if (!nextTokenIs(b, MAP_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CONST_MAP, null);
    r = consumeToken(b, MAP_START);
    p = r; // pin = 1
    r = r && report_error_(b, const_map_1(b, l + 1));
    r = p && consumeToken(b, MAP_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (comment | const_map_entry)*
  private static boolean const_map_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_map_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!const_map_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "const_map_1", c)) break;
    }
    return true;
  }

  // comment | const_map_entry
  private static boolean const_map_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_map_1_0")) return false;
    boolean r;
    r = comment(b, l + 1);
    if (!r) r = const_map_entry(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // const_map_key COLON const_value (COMMA | SEMICOLON)?
  public static boolean const_map_entry(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_map_entry")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CONST_MAP_ENTRY, "<const map entry>");
    r = const_map_key(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, consumeToken(b, COLON));
    r = p && report_error_(b, const_value(b, l + 1)) && r;
    r = p && const_map_entry_3(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (COMMA | SEMICOLON)?
  private static boolean const_map_entry_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_map_entry_3")) return false;
    const_map_entry_3_0(b, l + 1);
    return true;
  }

  // COMMA | SEMICOLON
  private static boolean const_map_entry_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_map_entry_3_0")) return false;
    boolean r;
    r = consumeToken(b, COMMA);
    if (!r) r = consumeToken(b, SEMICOLON);
    return r;
  }

  /* ********************************************************** */
  // const_primitive | const_reference
  public static boolean const_map_key(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_map_key")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, CONST_MAP_KEY, "<const map key>");
    r = const_primitive(b, l + 1);
    if (!r) r = const_reference(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean const_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, CONST_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // KW_NULL
  public static boolean const_null(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_null")) return false;
    if (!nextTokenIs(b, KW_NULL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_NULL);
    exit_section_(b, m, CONST_NULL, r);
    return r;
  }

  /* ********************************************************** */
  // KW_TRUE | KW_FALSE | ZERO | ID | NUMBER | LITERAL
  public static boolean const_primitive(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_primitive")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CONST_PRIMITIVE, "<const primitive>");
    r = consumeToken(b, KW_TRUE);
    if (!r) r = consumeToken(b, KW_FALSE);
    if (!r) r = consumeToken(b, ZERO);
    if (!r) r = consumeToken(b, ID);
    if (!r) r = consumeToken(b, NUMBER);
    if (!r) r = consumeToken(b, LITERAL);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // reference_program DOT reference_type_name DOT reference_value |
  //                         reference_type_name DOT reference_value
  public static boolean const_reference(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_reference")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = const_reference_0(b, l + 1);
    if (!r) r = const_reference_1(b, l + 1);
    exit_section_(b, m, CONST_REFERENCE, r);
    return r;
  }

  // reference_program DOT reference_type_name DOT reference_value
  private static boolean const_reference_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_reference_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = reference_program(b, l + 1);
    r = r && consumeToken(b, DOT);
    r = r && reference_type_name(b, l + 1);
    r = r && consumeToken(b, DOT);
    r = r && reference_value(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // reference_type_name DOT reference_value
  private static boolean const_reference_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_reference_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = reference_type_name(b, l + 1);
    r = r && consumeToken(b, DOT);
    r = r && reference_value(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // const_null | const_primitive | const_reference | const_list | const_map
  public static boolean const_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "const_value")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, CONST_VALUE, "<const value>");
    r = const_null(b, l + 1);
    if (!r) r = const_primitive(b, l + 1);
    if (!r) r = const_reference(b, l + 1);
    if (!r) r = const_list(b, l + 1);
    if (!r) r = const_map(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // KW_ENUM type_name enum_values annotations? SEMICOLON?
  public static boolean enum_$(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_$")) return false;
    if (!nextTokenIs(b, KW_ENUM)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, ENUM, null);
    r = consumeToken(b, KW_ENUM);
    p = r; // pin = 1
    r = r && report_error_(b, type_name(b, l + 1));
    r = p && report_error_(b, enum_values(b, l + 1)) && r;
    r = p && report_error_(b, enum_3(b, l + 1)) && r;
    r = p && enum_4(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // annotations?
  private static boolean enum_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_3")) return false;
    annotations(b, l + 1);
    return true;
  }

  // SEMICOLON?
  private static boolean enum_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_4")) return false;
    consumeToken(b, SEMICOLON);
    return true;
  }

  /* ********************************************************** */
  // value_name (EQUALS value_id)? annotations? (COMMA | SEMICOLON)?
  public static boolean enum_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_value")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, ENUM_VALUE, null);
    r = value_name(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, enum_value_1(b, l + 1));
    r = p && report_error_(b, enum_value_2(b, l + 1)) && r;
    r = p && enum_value_3(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (EQUALS value_id)?
  private static boolean enum_value_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_value_1")) return false;
    enum_value_1_0(b, l + 1);
    return true;
  }

  // EQUALS value_id
  private static boolean enum_value_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_value_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EQUALS);
    r = r && value_id(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // annotations?
  private static boolean enum_value_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_value_2")) return false;
    annotations(b, l + 1);
    return true;
  }

  // (COMMA | SEMICOLON)?
  private static boolean enum_value_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_value_3")) return false;
    enum_value_3_0(b, l + 1);
    return true;
  }

  // COMMA | SEMICOLON
  private static boolean enum_value_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_value_3_0")) return false;
    boolean r;
    r = consumeToken(b, COMMA);
    if (!r) r = consumeToken(b, SEMICOLON);
    return r;
  }

  /* ********************************************************** */
  // MAP_START (comment | enum_value)* MAP_END
  public static boolean enum_values(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_values")) return false;
    if (!nextTokenIs(b, MAP_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, ENUM_VALUES, null);
    r = consumeToken(b, MAP_START);
    p = r; // pin = 1
    r = r && report_error_(b, enum_values_1(b, l + 1));
    r = p && consumeToken(b, MAP_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (comment | enum_value)*
  private static boolean enum_values_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_values_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!enum_values_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "enum_values_1", c)) break;
    }
    return true;
  }

  // comment | enum_value
  private static boolean enum_values_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_values_1_0")) return false;
    boolean r;
    r = comment(b, l + 1);
    if (!r) r = enum_value(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // KW_EXTENDS type_reference
  public static boolean extends_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "extends_type")) return false;
    if (!nextTokenIs(b, KW_EXTENDS)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, EXTENDS_TYPE, null);
    r = consumeToken(b, KW_EXTENDS);
    p = r; // pin = 1
    r = r && type_reference(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // message_field | method_param
  public static boolean field(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, FIELD, "<field>");
    r = message_field(b, l + 1);
    if (!r) r = method_param(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // ID
  public static boolean field_id(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_id")) return false;
    if (!nextTokenIs(b, ID)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ID);
    exit_section_(b, m, FIELD_ID, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean field_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, FIELD_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // !(field_id | MAP_END | COMMA | SEMICOLON)
  static boolean field_recover(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_recover")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !field_recover_0(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // field_id | MAP_END | COMMA | SEMICOLON
  private static boolean field_recover_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_recover_0")) return false;
    boolean r;
    r = field_id(b, l + 1);
    if (!r) r = consumeToken(b, MAP_END);
    if (!r) r = consumeToken(b, COMMA);
    if (!r) r = consumeToken(b, SEMICOLON);
    return r;
  }

  /* ********************************************************** */
  // KW_OPTIONAL | KW_REQUIRED
  public static boolean field_requirement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "field_requirement")) return false;
    if (!nextTokenIs(b, "<field requirement>", KW_OPTIONAL, KW_REQUIRED)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FIELD_REQUIREMENT, "<field requirement>");
    r = consumeToken(b, KW_OPTIONAL);
    if (!r) r = consumeToken(b, KW_REQUIRED);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // (comment | message_field (COMMA | SEMICOLON)?)*
  public static boolean fields(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fields")) return false;
    Marker m = enter_section_(b, l, _NONE_, FIELDS, "<fields>");
    while (true) {
      int c = current_position_(b);
      if (!fields_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "fields", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // comment | message_field (COMMA | SEMICOLON)?
  private static boolean fields_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fields_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = comment(b, l + 1);
    if (!r) r = fields_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // message_field (COMMA | SEMICOLON)?
  private static boolean fields_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fields_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = message_field(b, l + 1);
    r = r && fields_0_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (COMMA | SEMICOLON)?
  private static boolean fields_0_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fields_0_1_1")) return false;
    fields_0_1_1_0(b, l + 1);
    return true;
  }

  // COMMA | SEMICOLON
  private static boolean fields_0_1_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fields_0_1_1_0")) return false;
    boolean r;
    r = consumeToken(b, COMMA);
    if (!r) r = consumeToken(b, SEMICOLON);
    return r;
  }

  /* ********************************************************** */
  // LITERAL
  public static boolean file_path(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "file_path")) return false;
    if (!nextTokenIs(b, LITERAL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LITERAL);
    exit_section_(b, m, FILE_PATH, r);
    return r;
  }

  /* ********************************************************** */
  // (KW_IMPLEMENTS | KW_OF) type_reference
  public static boolean implements_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "implements_type")) return false;
    if (!nextTokenIs(b, "<implements type>", KW_IMPLEMENTS, KW_OF)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, IMPLEMENTS_TYPE, "<implements type>");
    r = implements_type_0(b, l + 1);
    p = r; // pin = 1
    r = r && type_reference(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // KW_IMPLEMENTS | KW_OF
  private static boolean implements_type_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "implements_type_0")) return false;
    boolean r;
    r = consumeToken(b, KW_IMPLEMENTS);
    if (!r) r = consumeToken(b, KW_OF);
    return r;
  }

  /* ********************************************************** */
  // KW_INCLUDE file_path
  public static boolean include(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "include")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, INCLUDE, "<include>");
    r = consumeToken(b, KW_INCLUDE);
    p = r; // pin = 1
    r = r && file_path(b, l + 1);
    exit_section_(b, l, m, r, p, spec_recover_parser_);
    return r || p;
  }

  /* ********************************************************** */
  // primitive_type | type_reference
  public static boolean key_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "key_type")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, KEY_TYPE, "<key type>");
    r = primitive_type(b, l + 1);
    if (!r) r = type_reference(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean language_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "language_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, LANGUAGE_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // (KW_LIST | KW_SET) GENERIC_START type GENERIC_END
  public static boolean list_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "list_type")) return false;
    if (!nextTokenIs(b, "<list type>", KW_LIST, KW_SET)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _COLLAPSE_, LIST_TYPE, "<list type>");
    r = list_type_0(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, consumeToken(b, GENERIC_START));
    r = p && report_error_(b, type(b, l + 1)) && r;
    r = p && consumeToken(b, GENERIC_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // KW_LIST | KW_SET
  private static boolean list_type_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "list_type_0")) return false;
    boolean r;
    r = consumeToken(b, KW_LIST);
    if (!r) r = consumeToken(b, KW_SET);
    return r;
  }

  /* ********************************************************** */
  // KW_MAP GENERIC_START key_type COMMA type GENERIC_END
  public static boolean map_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "map_type")) return false;
    if (!nextTokenIs(b, KW_MAP)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, MAP_TYPE, null);
    r = consumeTokens(b, 1, KW_MAP, GENERIC_START);
    p = r; // pin = 1
    r = r && report_error_(b, key_type(b, l + 1));
    r = p && report_error_(b, consumeToken(b, COMMA)) && r;
    r = p && report_error_(b, type(b, l + 1)) && r;
    r = p && consumeToken(b, GENERIC_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // message_variant type_name implements_type? message_fields annotations? SEMICOLON?
  public static boolean message(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, MESSAGE, "<message>");
    r = message_variant(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, type_name(b, l + 1));
    r = p && report_error_(b, message_2(b, l + 1)) && r;
    r = p && report_error_(b, message_fields(b, l + 1)) && r;
    r = p && report_error_(b, message_4(b, l + 1)) && r;
    r = p && message_5(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // implements_type?
  private static boolean message_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_2")) return false;
    implements_type(b, l + 1);
    return true;
  }

  // annotations?
  private static boolean message_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_4")) return false;
    annotations(b, l + 1);
    return true;
  }

  // SEMICOLON?
  private static boolean message_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_5")) return false;
    consumeToken(b, SEMICOLON);
    return true;
  }

  /* ********************************************************** */
  // (field_id COLON)? field_requirement? type field_name
  //         (EQUALS const_value)?
  //         annotations?
  public static boolean message_field(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_field")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, MESSAGE_FIELD, "<message field>");
    r = message_field_0(b, l + 1);
    r = r && message_field_1(b, l + 1);
    r = r && type(b, l + 1);
    r = r && field_name(b, l + 1);
    r = r && message_field_4(b, l + 1);
    r = r && message_field_5(b, l + 1);
    exit_section_(b, l, m, r, false, field_recover_parser_);
    return r;
  }

  // (field_id COLON)?
  private static boolean message_field_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_field_0")) return false;
    message_field_0_0(b, l + 1);
    return true;
  }

  // field_id COLON
  private static boolean message_field_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_field_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = field_id(b, l + 1);
    r = r && consumeToken(b, COLON);
    exit_section_(b, m, null, r);
    return r;
  }

  // field_requirement?
  private static boolean message_field_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_field_1")) return false;
    field_requirement(b, l + 1);
    return true;
  }

  // (EQUALS const_value)?
  private static boolean message_field_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_field_4")) return false;
    message_field_4_0(b, l + 1);
    return true;
  }

  // EQUALS const_value
  private static boolean message_field_4_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_field_4_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EQUALS);
    r = r && const_value(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // annotations?
  private static boolean message_field_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_field_5")) return false;
    annotations(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // MAP_START fields MAP_END
  public static boolean message_fields(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_fields")) return false;
    if (!nextTokenIs(b, MAP_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, MESSAGE_FIELDS, null);
    r = consumeToken(b, MAP_START);
    p = r; // pin = 1
    r = r && report_error_(b, fields(b, l + 1));
    r = p && consumeToken(b, MAP_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // KW_STRUCT | KW_UNION | KW_EXCEPTION | KW_INTERFACE
  public static boolean message_variant(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message_variant")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, MESSAGE_VARIANT, "<message variant>");
    r = consumeToken(b, KW_STRUCT);
    if (!r) r = consumeToken(b, KW_UNION);
    if (!r) r = consumeToken(b, KW_EXCEPTION);
    if (!r) r = consumeToken(b, KW_INTERFACE);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean method_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "method_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, METHOD_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // (field_id COLON)? field_requirement? type field_name
  //         (EQUALS const_value)?
  //         annotations?
  public static boolean method_param(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "method_param")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, METHOD_PARAM, "<method param>");
    r = method_param_0(b, l + 1);
    r = r && method_param_1(b, l + 1);
    r = r && type(b, l + 1);
    r = r && field_name(b, l + 1);
    r = r && method_param_4(b, l + 1);
    r = r && method_param_5(b, l + 1);
    exit_section_(b, l, m, r, false, param_recover_parser_);
    return r;
  }

  // (field_id COLON)?
  private static boolean method_param_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "method_param_0")) return false;
    method_param_0_0(b, l + 1);
    return true;
  }

  // field_id COLON
  private static boolean method_param_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "method_param_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = field_id(b, l + 1);
    r = r && consumeToken(b, COLON);
    exit_section_(b, m, null, r);
    return r;
  }

  // field_requirement?
  private static boolean method_param_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "method_param_1")) return false;
    field_requirement(b, l + 1);
    return true;
  }

  // (EQUALS const_value)?
  private static boolean method_param_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "method_param_4")) return false;
    method_param_4_0(b, l + 1);
    return true;
  }

  // EQUALS const_value
  private static boolean method_param_4_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "method_param_4_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EQUALS);
    r = r && const_value(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // annotations?
  private static boolean method_param_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "method_param_5")) return false;
    annotations(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // KW_NAMESPACE language_name namespace_package?
  public static boolean namespace(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "namespace")) return false;
    if (!nextTokenIs(b, KW_NAMESPACE)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, NAMESPACE, null);
    r = consumeToken(b, KW_NAMESPACE);
    p = r; // pin = 1
    r = r && report_error_(b, language_name(b, l + 1));
    r = p && namespace_2(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // namespace_package?
  private static boolean namespace_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "namespace_2")) return false;
    namespace_package(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // IDENTIFIER (DOT IDENTIFIER)*
  public static boolean namespace_package(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "namespace_package")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    r = r && namespace_package_1(b, l + 1);
    exit_section_(b, m, NAMESPACE_PACKAGE, r);
    return r;
  }

  // (DOT IDENTIFIER)*
  private static boolean namespace_package_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "namespace_package_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!namespace_package_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "namespace_package_1", c)) break;
    }
    return true;
  }

  // DOT IDENTIFIER
  private static boolean namespace_package_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "namespace_package_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, DOT, IDENTIFIER);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // !(field_id | PARAM_END | COMMA | SEMICOLON)
  static boolean param_recover(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "param_recover")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !param_recover_0(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // field_id | PARAM_END | COMMA | SEMICOLON
  private static boolean param_recover_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "param_recover_0")) return false;
    boolean r;
    r = field_id(b, l + 1);
    if (!r) r = consumeToken(b, PARAM_END);
    if (!r) r = consumeToken(b, COMMA);
    if (!r) r = consumeToken(b, SEMICOLON);
    return r;
  }

  /* ********************************************************** */
  // (comment | method_param (COMMA | SEMICOLON)?)*
  public static boolean params(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "params")) return false;
    Marker m = enter_section_(b, l, _NONE_, PARAMS, "<params>");
    while (true) {
      int c = current_position_(b);
      if (!params_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "params", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // comment | method_param (COMMA | SEMICOLON)?
  private static boolean params_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "params_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = comment(b, l + 1);
    if (!r) r = params_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // method_param (COMMA | SEMICOLON)?
  private static boolean params_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "params_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = method_param(b, l + 1);
    r = r && params_0_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (COMMA | SEMICOLON)?
  private static boolean params_0_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "params_0_1_1")) return false;
    params_0_1_1_0(b, l + 1);
    return true;
  }

  // COMMA | SEMICOLON
  private static boolean params_0_1_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "params_0_1_1_0")) return false;
    boolean r;
    r = consumeToken(b, COMMA);
    if (!r) r = consumeToken(b, SEMICOLON);
    return r;
  }

  /* ********************************************************** */
  // KW_BOOL | KW_BYTE | KW_I8 | KW_I16 | KW_I32 | KW_I64 | KW_DOUBLE | KW_STRING | KW_BINARY
  public static boolean primitive_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primitive_type")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PRIMITIVE_TYPE, "<primitive type>");
    r = consumeToken(b, KW_BOOL);
    if (!r) r = consumeToken(b, KW_BYTE);
    if (!r) r = consumeToken(b, KW_I8);
    if (!r) r = consumeToken(b, KW_I16);
    if (!r) r = consumeToken(b, KW_I32);
    if (!r) r = consumeToken(b, KW_I64);
    if (!r) r = consumeToken(b, KW_DOUBLE);
    if (!r) r = consumeToken(b, KW_STRING);
    if (!r) r = consumeToken(b, KW_BINARY);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean reference_program(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "reference_program")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, REFERENCE_PROGRAM, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean reference_type_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "reference_type_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, REFERENCE_TYPE_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean reference_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "reference_value")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, REFERENCE_VALUE, r);
    return r;
  }

  /* ********************************************************** */
  // KW_SERVICE type_name extends_type? service_methods annotations? SEMICOLON?
  public static boolean service(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service")) return false;
    if (!nextTokenIs(b, KW_SERVICE)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, SERVICE, null);
    r = consumeToken(b, KW_SERVICE);
    p = r; // pin = 1
    r = r && report_error_(b, type_name(b, l + 1));
    r = p && report_error_(b, service_2(b, l + 1)) && r;
    r = p && report_error_(b, service_methods(b, l + 1)) && r;
    r = p && report_error_(b, service_4(b, l + 1)) && r;
    r = p && service_5(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // extends_type?
  private static boolean service_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_2")) return false;
    extends_type(b, l + 1);
    return true;
  }

  // annotations?
  private static boolean service_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_4")) return false;
    annotations(b, l + 1);
    return true;
  }

  // SEMICOLON?
  private static boolean service_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_5")) return false;
    consumeToken(b, SEMICOLON);
    return true;
  }

  /* ********************************************************** */
  // KW_THROWS PARAM_START params PARAM_END
  public static boolean service_exceptions(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_exceptions")) return false;
    if (!nextTokenIs(b, KW_THROWS)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, SERVICE_EXCEPTIONS, null);
    r = consumeTokens(b, 1, KW_THROWS, PARAM_START);
    p = r; // pin = 1
    r = r && report_error_(b, params(b, l + 1));
    r = p && consumeToken(b, PARAM_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // service_return_type method_name service_params service_exceptions? annotations? SEMICOLON?
  public static boolean service_method(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_method")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, SERVICE_METHOD, "<service method>");
    r = service_return_type(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, method_name(b, l + 1));
    r = p && report_error_(b, service_params(b, l + 1)) && r;
    r = p && report_error_(b, service_method_3(b, l + 1)) && r;
    r = p && report_error_(b, service_method_4(b, l + 1)) && r;
    r = p && service_method_5(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // service_exceptions?
  private static boolean service_method_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_method_3")) return false;
    service_exceptions(b, l + 1);
    return true;
  }

  // annotations?
  private static boolean service_method_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_method_4")) return false;
    annotations(b, l + 1);
    return true;
  }

  // SEMICOLON?
  private static boolean service_method_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_method_5")) return false;
    consumeToken(b, SEMICOLON);
    return true;
  }

  /* ********************************************************** */
  // MAP_START (comment | service_method)* MAP_END
  public static boolean service_methods(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_methods")) return false;
    if (!nextTokenIs(b, MAP_START)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, MAP_START);
    r = r && service_methods_1(b, l + 1);
    r = r && consumeToken(b, MAP_END);
    exit_section_(b, m, SERVICE_METHODS, r);
    return r;
  }

  // (comment | service_method)*
  private static boolean service_methods_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_methods_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!service_methods_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "service_methods_1", c)) break;
    }
    return true;
  }

  // comment | service_method
  private static boolean service_methods_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_methods_1_0")) return false;
    boolean r;
    r = comment(b, l + 1);
    if (!r) r = service_method(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // PARAM_START params PARAM_END
  public static boolean service_params(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_params")) return false;
    if (!nextTokenIs(b, PARAM_START)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, SERVICE_PARAMS, null);
    r = consumeToken(b, PARAM_START);
    p = r; // pin = 1
    r = r && report_error_(b, params(b, l + 1));
    r = p && consumeToken(b, PARAM_END) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // KW_ONEWAY? KW_VOID | type
  public static boolean service_return_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_return_type")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, SERVICE_RETURN_TYPE, "<service return type>");
    r = service_return_type_0(b, l + 1);
    if (!r) r = type(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // KW_ONEWAY? KW_VOID
  private static boolean service_return_type_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_return_type_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = service_return_type_0_0(b, l + 1);
    r = r && consumeToken(b, KW_VOID);
    exit_section_(b, m, null, r);
    return r;
  }

  // KW_ONEWAY?
  private static boolean service_return_type_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "service_return_type_0_0")) return false;
    consumeToken(b, KW_ONEWAY);
    return true;
  }

  /* ********************************************************** */
  // typedef | enum | message | service | const
  public static boolean spec(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "spec")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, SPEC, "<spec>");
    r = typedef(b, l + 1);
    if (!r) r = enum_$(b, l + 1);
    if (!r) r = message(b, l + 1);
    if (!r) r = service(b, l + 1);
    if (!r) r = const_$(b, l + 1);
    exit_section_(b, l, m, r, false, spec_recover_parser_);
    return r;
  }

  /* ********************************************************** */
  // !(KW_NAMESPACE | KW_INCLUDE | KW_STRUCT | KW_EXCEPTION | KW_UNION | KW_INTERFACE | KW_ENUM | KW_TYPEDEF | KW_SERVICE | KW_CONST)
  static boolean spec_recover(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "spec_recover")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !spec_recover_0(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // KW_NAMESPACE | KW_INCLUDE | KW_STRUCT | KW_EXCEPTION | KW_UNION | KW_INTERFACE | KW_ENUM | KW_TYPEDEF | KW_SERVICE | KW_CONST
  private static boolean spec_recover_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "spec_recover_0")) return false;
    boolean r;
    r = consumeToken(b, KW_NAMESPACE);
    if (!r) r = consumeToken(b, KW_INCLUDE);
    if (!r) r = consumeToken(b, KW_STRUCT);
    if (!r) r = consumeToken(b, KW_EXCEPTION);
    if (!r) r = consumeToken(b, KW_UNION);
    if (!r) r = consumeToken(b, KW_INTERFACE);
    if (!r) r = consumeToken(b, KW_ENUM);
    if (!r) r = consumeToken(b, KW_TYPEDEF);
    if (!r) r = consumeToken(b, KW_SERVICE);
    if (!r) r = consumeToken(b, KW_CONST);
    return r;
  }

  /* ********************************************************** */
  // (comment | namespace | include)* (comment | spec)*
  static boolean thrift_file(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "thrift_file")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = thrift_file_0(b, l + 1);
    r = r && thrift_file_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (comment | namespace | include)*
  private static boolean thrift_file_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "thrift_file_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!thrift_file_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "thrift_file_0", c)) break;
    }
    return true;
  }

  // comment | namespace | include
  private static boolean thrift_file_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "thrift_file_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = comment(b, l + 1);
    if (!r) r = namespace(b, l + 1);
    if (!r) r = include(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (comment | spec)*
  private static boolean thrift_file_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "thrift_file_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!thrift_file_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "thrift_file_1", c)) break;
    }
    return true;
  }

  // comment | spec
  private static boolean thrift_file_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "thrift_file_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = comment(b, l + 1);
    if (!r) r = spec(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // primitive_type | list_type | map_type | type_reference
  public static boolean type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, TYPE, "<type>");
    r = primitive_type(b, l + 1);
    if (!r) r = list_type(b, l + 1);
    if (!r) r = map_type(b, l + 1);
    if (!r) r = type_reference(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean type_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, TYPE_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // reference_program DOT reference_type_name |
  //                         reference_type_name
  public static boolean type_reference(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_reference")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = type_reference_0(b, l + 1);
    if (!r) r = reference_type_name(b, l + 1);
    exit_section_(b, m, TYPE_REFERENCE, r);
    return r;
  }

  // reference_program DOT reference_type_name
  private static boolean type_reference_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_reference_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = reference_program(b, l + 1);
    r = r && consumeToken(b, DOT);
    r = r && reference_type_name(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // KW_TYPEDEF type type_name SEMICOLON?
  public static boolean typedef(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typedef")) return false;
    if (!nextTokenIs(b, KW_TYPEDEF)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, TYPEDEF, null);
    r = consumeToken(b, KW_TYPEDEF);
    p = r; // pin = 1
    r = r && report_error_(b, type(b, l + 1));
    r = p && report_error_(b, type_name(b, l + 1)) && r;
    r = p && typedef_3(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // SEMICOLON?
  private static boolean typedef_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typedef_3")) return false;
    consumeToken(b, SEMICOLON);
    return true;
  }

  /* ********************************************************** */
  // ID | ZERO
  public static boolean value_id(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "value_id")) return false;
    if (!nextTokenIs(b, "<value id>", ID, ZERO)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, VALUE_ID, "<value id>");
    r = consumeToken(b, ID);
    if (!r) r = consumeToken(b, ZERO);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean value_name(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "value_name")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, VALUE_NAME, r);
    return r;
  }

  static final Parser field_recover_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return field_recover(b, l + 1);
    }
  };
  static final Parser param_recover_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return param_recover(b, l + 1);
    }
  };
  static final Parser spec_recover_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return spec_recover(b, l + 1);
    }
  };
}
