// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ThriftEnum extends ThriftSpec {

  @Nullable
  ThriftAnnotations getAnnotations();

  @Nullable
  ThriftEnumValues getEnumValues();

  @Nullable
  ThriftTypeName getTypeName();

}
