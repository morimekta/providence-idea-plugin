// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import com.intellij.navigation.ItemPresentation;

public interface ThriftFieldName extends ThriftFieldNameElement {

  String getName();

  PsiElement setName(String name);

  PsiElement getNameIdentifier();

  ItemPresentation getPresentation();

}
