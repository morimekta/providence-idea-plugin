// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ThriftMapType extends ThriftType {

  @NotNull
  List<ThriftType> getTypeList();

  @Nullable
  ThriftType getKeyType();

  @Nullable
  ThriftType getValueType();

}
