// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ThriftMessage extends ThriftSpec {

  @Nullable
  ThriftAnnotations getAnnotations();

  @Nullable
  ThriftImplementsType getImplementsType();

  @Nullable
  ThriftMessageFields getMessageFields();

  @NotNull
  ThriftMessageVariant getMessageVariant();

  @Nullable
  ThriftTypeName getTypeName();

}
