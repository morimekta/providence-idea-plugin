// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ThriftMessageField extends ThriftField {

  @Nullable
  ThriftAnnotations getAnnotations();

  @Nullable
  ThriftConstValue getConstValue();

  @Nullable
  ThriftFieldId getFieldId();

  @NotNull
  ThriftFieldName getFieldName();

  @Nullable
  ThriftFieldRequirement getFieldRequirement();

  @NotNull
  ThriftType getType();

}
