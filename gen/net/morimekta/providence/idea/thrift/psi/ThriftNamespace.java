// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import javax.annotation.Nonnull;

public interface ThriftNamespace extends PsiElement {

  @Nullable
  ThriftLanguageName getLanguageName();

  @Nullable
  ThriftNamespacePackage getNamespacePackage();

  @Nonnull
  String getNamespaceIdentifier();

  @Nonnull
  String getNamespaceLanguage();

}
