// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ThriftServiceMethod extends PsiElement {

  @Nullable
  ThriftAnnotations getAnnotations();

  @Nullable
  ThriftMethodName getMethodName();

  @Nullable
  ThriftServiceExceptions getServiceExceptions();

  @Nullable
  ThriftServiceParams getServiceParams();

  @NotNull
  ThriftServiceReturnType getServiceReturnType();

}
