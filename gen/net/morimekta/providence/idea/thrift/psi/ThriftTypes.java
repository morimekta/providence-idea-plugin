// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import net.morimekta.providence.idea.thrift.psi.impl.*;

public interface ThriftTypes {

  IElementType ANNOTATIONS = new ThriftElementType("ANNOTATIONS");
  IElementType ANNOTATION_ENTRY = new ThriftElementType("ANNOTATION_ENTRY");
  IElementType ANNOTATION_KEY = new ThriftElementType("ANNOTATION_KEY");
  IElementType ANNOTATION_VALUE = new ThriftElementType("ANNOTATION_VALUE");
  IElementType COMMENT = new ThriftElementType("COMMENT");
  IElementType CONST = new ThriftElementType("CONST");
  IElementType CONST_LIST = new ThriftElementType("CONST_LIST");
  IElementType CONST_LIST_ENTRY = new ThriftElementType("CONST_LIST_ENTRY");
  IElementType CONST_MAP = new ThriftElementType("CONST_MAP");
  IElementType CONST_MAP_ENTRY = new ThriftElementType("CONST_MAP_ENTRY");
  IElementType CONST_MAP_KEY = new ThriftElementType("CONST_MAP_KEY");
  IElementType CONST_NAME = new ThriftElementType("CONST_NAME");
  IElementType CONST_NULL = new ThriftElementType("CONST_NULL");
  IElementType CONST_PRIMITIVE = new ThriftElementType("CONST_PRIMITIVE");
  IElementType CONST_REFERENCE = new ThriftElementType("CONST_REFERENCE");
  IElementType CONST_VALUE = new ThriftElementType("CONST_VALUE");
  IElementType ENUM = new ThriftElementType("ENUM");
  IElementType ENUM_VALUE = new ThriftElementType("ENUM_VALUE");
  IElementType ENUM_VALUES = new ThriftElementType("ENUM_VALUES");
  IElementType EXTENDS_TYPE = new ThriftElementType("EXTENDS_TYPE");
  IElementType FIELD = new ThriftElementType("FIELD");
  IElementType FIELDS = new ThriftElementType("FIELDS");
  IElementType FIELD_ID = new ThriftElementType("FIELD_ID");
  IElementType FIELD_NAME = new ThriftElementType("FIELD_NAME");
  IElementType FIELD_REQUIREMENT = new ThriftElementType("FIELD_REQUIREMENT");
  IElementType FILE_PATH = new ThriftElementType("FILE_PATH");
  IElementType IMPLEMENTS_TYPE = new ThriftElementType("IMPLEMENTS_TYPE");
  IElementType INCLUDE = new ThriftElementType("INCLUDE");
  IElementType KEY_TYPE = new ThriftElementType("KEY_TYPE");
  IElementType LANGUAGE_NAME = new ThriftElementType("LANGUAGE_NAME");
  IElementType LIST_TYPE = new ThriftElementType("LIST_TYPE");
  IElementType MAP_TYPE = new ThriftElementType("MAP_TYPE");
  IElementType MESSAGE = new ThriftElementType("MESSAGE");
  IElementType MESSAGE_FIELD = new ThriftElementType("MESSAGE_FIELD");
  IElementType MESSAGE_FIELDS = new ThriftElementType("MESSAGE_FIELDS");
  IElementType MESSAGE_VARIANT = new ThriftElementType("MESSAGE_VARIANT");
  IElementType METHOD_NAME = new ThriftElementType("METHOD_NAME");
  IElementType METHOD_PARAM = new ThriftElementType("METHOD_PARAM");
  IElementType NAMESPACE = new ThriftElementType("NAMESPACE");
  IElementType NAMESPACE_PACKAGE = new ThriftElementType("NAMESPACE_PACKAGE");
  IElementType PARAMS = new ThriftElementType("PARAMS");
  IElementType PRIMITIVE_TYPE = new ThriftElementType("PRIMITIVE_TYPE");
  IElementType REFERENCE_PROGRAM = new ThriftElementType("REFERENCE_PROGRAM");
  IElementType REFERENCE_TYPE_NAME = new ThriftElementType("REFERENCE_TYPE_NAME");
  IElementType REFERENCE_VALUE = new ThriftElementType("REFERENCE_VALUE");
  IElementType SERVICE = new ThriftElementType("SERVICE");
  IElementType SERVICE_EXCEPTIONS = new ThriftElementType("SERVICE_EXCEPTIONS");
  IElementType SERVICE_METHOD = new ThriftElementType("SERVICE_METHOD");
  IElementType SERVICE_METHODS = new ThriftElementType("SERVICE_METHODS");
  IElementType SERVICE_PARAMS = new ThriftElementType("SERVICE_PARAMS");
  IElementType SERVICE_RETURN_TYPE = new ThriftElementType("SERVICE_RETURN_TYPE");
  IElementType SPEC = new ThriftElementType("SPEC");
  IElementType TYPE = new ThriftElementType("TYPE");
  IElementType TYPEDEF = new ThriftElementType("TYPEDEF");
  IElementType TYPE_NAME = new ThriftElementType("TYPE_NAME");
  IElementType TYPE_REFERENCE = new ThriftElementType("TYPE_REFERENCE");
  IElementType VALUE_ID = new ThriftElementType("VALUE_ID");
  IElementType VALUE_NAME = new ThriftElementType("VALUE_NAME");

  IElementType COLON = new ThriftTokenType("COLON");
  IElementType COMMA = new ThriftTokenType("COMMA");
  IElementType DOC_COMMENT = new ThriftTokenType("DOC_COMMENT");
  IElementType DOT = new ThriftTokenType("DOT");
  IElementType EQUALS = new ThriftTokenType("EQUALS");
  IElementType GENERIC_END = new ThriftTokenType("GENERIC_END");
  IElementType GENERIC_START = new ThriftTokenType("GENERIC_START");
  IElementType ID = new ThriftTokenType("ID");
  IElementType IDENTIFIER = new ThriftTokenType("IDENTIFIER");
  IElementType KW_BINARY = new ThriftTokenType("KW_BINARY");
  IElementType KW_BOOL = new ThriftTokenType("KW_BOOL");
  IElementType KW_BYTE = new ThriftTokenType("KW_BYTE");
  IElementType KW_CONST = new ThriftTokenType("KW_CONST");
  IElementType KW_DOUBLE = new ThriftTokenType("KW_DOUBLE");
  IElementType KW_ENUM = new ThriftTokenType("KW_ENUM");
  IElementType KW_EXCEPTION = new ThriftTokenType("KW_EXCEPTION");
  IElementType KW_EXTENDS = new ThriftTokenType("KW_EXTENDS");
  IElementType KW_FALSE = new ThriftTokenType("KW_FALSE");
  IElementType KW_I16 = new ThriftTokenType("KW_I16");
  IElementType KW_I32 = new ThriftTokenType("KW_I32");
  IElementType KW_I64 = new ThriftTokenType("KW_I64");
  IElementType KW_I8 = new ThriftTokenType("KW_I8");
  IElementType KW_IMPLEMENTS = new ThriftTokenType("KW_IMPLEMENTS");
  IElementType KW_INCLUDE = new ThriftTokenType("KW_INCLUDE");
  IElementType KW_INTERFACE = new ThriftTokenType("KW_INTERFACE");
  IElementType KW_LIST = new ThriftTokenType("KW_LIST");
  IElementType KW_MAP = new ThriftTokenType("KW_MAP");
  IElementType KW_NAMESPACE = new ThriftTokenType("KW_NAMESPACE");
  IElementType KW_NULL = new ThriftTokenType("KW_NULL");
  IElementType KW_OF = new ThriftTokenType("KW_OF");
  IElementType KW_ONEWAY = new ThriftTokenType("KW_ONEWAY");
  IElementType KW_OPTIONAL = new ThriftTokenType("KW_OPTIONAL");
  IElementType KW_REQUIRED = new ThriftTokenType("KW_REQUIRED");
  IElementType KW_SERVICE = new ThriftTokenType("KW_SERVICE");
  IElementType KW_SET = new ThriftTokenType("KW_SET");
  IElementType KW_STRING = new ThriftTokenType("KW_STRING");
  IElementType KW_STRUCT = new ThriftTokenType("KW_STRUCT");
  IElementType KW_THROWS = new ThriftTokenType("KW_THROWS");
  IElementType KW_TRUE = new ThriftTokenType("KW_TRUE");
  IElementType KW_TYPEDEF = new ThriftTokenType("KW_TYPEDEF");
  IElementType KW_UNION = new ThriftTokenType("KW_UNION");
  IElementType KW_VOID = new ThriftTokenType("KW_VOID");
  IElementType LINE_COMMENT = new ThriftTokenType("LINE_COMMENT");
  IElementType LIST_END = new ThriftTokenType("LIST_END");
  IElementType LIST_START = new ThriftTokenType("LIST_START");
  IElementType LITERAL = new ThriftTokenType("LITERAL");
  IElementType MAP_END = new ThriftTokenType("MAP_END");
  IElementType MAP_START = new ThriftTokenType("MAP_START");
  IElementType NUMBER = new ThriftTokenType("NUMBER");
  IElementType PARAM_END = new ThriftTokenType("PARAM_END");
  IElementType PARAM_START = new ThriftTokenType("PARAM_START");
  IElementType SEMICOLON = new ThriftTokenType("SEMICOLON");
  IElementType ZERO = new ThriftTokenType("ZERO");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ANNOTATIONS) {
        return new ThriftAnnotationsImpl(node);
      }
      else if (type == ANNOTATION_ENTRY) {
        return new ThriftAnnotationEntryImpl(node);
      }
      else if (type == ANNOTATION_KEY) {
        return new ThriftAnnotationKeyImpl(node);
      }
      else if (type == ANNOTATION_VALUE) {
        return new ThriftAnnotationValueImpl(node);
      }
      else if (type == COMMENT) {
        return new ThriftCommentImpl(node);
      }
      else if (type == CONST) {
        return new ThriftConstImpl(node);
      }
      else if (type == CONST_LIST) {
        return new ThriftConstListImpl(node);
      }
      else if (type == CONST_LIST_ENTRY) {
        return new ThriftConstListEntryImpl(node);
      }
      else if (type == CONST_MAP) {
        return new ThriftConstMapImpl(node);
      }
      else if (type == CONST_MAP_ENTRY) {
        return new ThriftConstMapEntryImpl(node);
      }
      else if (type == CONST_NAME) {
        return new ThriftConstNameImpl(node);
      }
      else if (type == CONST_NULL) {
        return new ThriftConstNullImpl(node);
      }
      else if (type == CONST_PRIMITIVE) {
        return new ThriftConstPrimitiveImpl(node);
      }
      else if (type == CONST_REFERENCE) {
        return new ThriftConstReferenceImpl(node);
      }
      else if (type == ENUM) {
        return new ThriftEnumImpl(node);
      }
      else if (type == ENUM_VALUE) {
        return new ThriftEnumValueImpl(node);
      }
      else if (type == ENUM_VALUES) {
        return new ThriftEnumValuesImpl(node);
      }
      else if (type == EXTENDS_TYPE) {
        return new ThriftExtendsTypeImpl(node);
      }
      else if (type == FIELDS) {
        return new ThriftFieldsImpl(node);
      }
      else if (type == FIELD_ID) {
        return new ThriftFieldIdImpl(node);
      }
      else if (type == FIELD_NAME) {
        return new ThriftFieldNameImpl(node);
      }
      else if (type == FIELD_REQUIREMENT) {
        return new ThriftFieldRequirementImpl(node);
      }
      else if (type == FILE_PATH) {
        return new ThriftFilePathImpl(node);
      }
      else if (type == IMPLEMENTS_TYPE) {
        return new ThriftImplementsTypeImpl(node);
      }
      else if (type == INCLUDE) {
        return new ThriftIncludeImpl(node);
      }
      else if (type == LANGUAGE_NAME) {
        return new ThriftLanguageNameImpl(node);
      }
      else if (type == LIST_TYPE) {
        return new ThriftListTypeImpl(node);
      }
      else if (type == MAP_TYPE) {
        return new ThriftMapTypeImpl(node);
      }
      else if (type == MESSAGE) {
        return new ThriftMessageImpl(node);
      }
      else if (type == MESSAGE_FIELD) {
        return new ThriftMessageFieldImpl(node);
      }
      else if (type == MESSAGE_FIELDS) {
        return new ThriftMessageFieldsImpl(node);
      }
      else if (type == MESSAGE_VARIANT) {
        return new ThriftMessageVariantImpl(node);
      }
      else if (type == METHOD_NAME) {
        return new ThriftMethodNameImpl(node);
      }
      else if (type == METHOD_PARAM) {
        return new ThriftMethodParamImpl(node);
      }
      else if (type == NAMESPACE) {
        return new ThriftNamespaceImpl(node);
      }
      else if (type == NAMESPACE_PACKAGE) {
        return new ThriftNamespacePackageImpl(node);
      }
      else if (type == PARAMS) {
        return new ThriftParamsImpl(node);
      }
      else if (type == PRIMITIVE_TYPE) {
        return new ThriftPrimitiveTypeImpl(node);
      }
      else if (type == REFERENCE_PROGRAM) {
        return new ThriftReferenceProgramImpl(node);
      }
      else if (type == REFERENCE_TYPE_NAME) {
        return new ThriftReferenceTypeNameImpl(node);
      }
      else if (type == REFERENCE_VALUE) {
        return new ThriftReferenceValueImpl(node);
      }
      else if (type == SERVICE) {
        return new ThriftServiceImpl(node);
      }
      else if (type == SERVICE_EXCEPTIONS) {
        return new ThriftServiceExceptionsImpl(node);
      }
      else if (type == SERVICE_METHOD) {
        return new ThriftServiceMethodImpl(node);
      }
      else if (type == SERVICE_METHODS) {
        return new ThriftServiceMethodsImpl(node);
      }
      else if (type == SERVICE_PARAMS) {
        return new ThriftServiceParamsImpl(node);
      }
      else if (type == SERVICE_RETURN_TYPE) {
        return new ThriftServiceReturnTypeImpl(node);
      }
      else if (type == SPEC) {
        return new ThriftSpecImpl(node);
      }
      else if (type == TYPEDEF) {
        return new ThriftTypedefImpl(node);
      }
      else if (type == TYPE_NAME) {
        return new ThriftTypeNameImpl(node);
      }
      else if (type == TYPE_REFERENCE) {
        return new ThriftTypeReferenceImpl(node);
      }
      else if (type == VALUE_ID) {
        return new ThriftValueIdImpl(node);
      }
      else if (type == VALUE_NAME) {
        return new ThriftValueNameImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
