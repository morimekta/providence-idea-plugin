// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class ThriftVisitor extends PsiElementVisitor {

  public void visitAnnotationEntry(@NotNull ThriftAnnotationEntry o) {
    visitPsiElement(o);
  }

  public void visitAnnotationKey(@NotNull ThriftAnnotationKey o) {
    visitPsiElement(o);
  }

  public void visitAnnotationValue(@NotNull ThriftAnnotationValue o) {
    visitPsiElement(o);
  }

  public void visitAnnotations(@NotNull ThriftAnnotations o) {
    visitPsiElement(o);
  }

  public void visitComment(@NotNull ThriftComment o) {
    visitPsiElement(o);
  }

  public void visitConst(@NotNull ThriftConst o) {
    visitSpec(o);
  }

  public void visitConstList(@NotNull ThriftConstList o) {
    visitConstValue(o);
  }

  public void visitConstListEntry(@NotNull ThriftConstListEntry o) {
    visitPsiElement(o);
  }

  public void visitConstMap(@NotNull ThriftConstMap o) {
    visitConstValue(o);
  }

  public void visitConstMapEntry(@NotNull ThriftConstMapEntry o) {
    visitPsiElement(o);
  }

  public void visitConstMapKey(@NotNull ThriftConstMapKey o) {
    visitConstValue(o);
  }

  public void visitConstName(@NotNull ThriftConstName o) {
    visitPsiElement(o);
  }

  public void visitConstNull(@NotNull ThriftConstNull o) {
    visitConstValue(o);
  }

  public void visitConstPrimitive(@NotNull ThriftConstPrimitive o) {
    visitConstValue(o);
  }

  public void visitConstReference(@NotNull ThriftConstReference o) {
    visitConstValue(o);
  }

  public void visitConstValue(@NotNull ThriftConstValue o) {
    visitPsiElement(o);
  }

  public void visitEnum(@NotNull ThriftEnum o) {
    visitSpec(o);
  }

  public void visitEnumValue(@NotNull ThriftEnumValue o) {
    visitPsiElement(o);
  }

  public void visitEnumValues(@NotNull ThriftEnumValues o) {
    visitPsiElement(o);
  }

  public void visitExtendsType(@NotNull ThriftExtendsType o) {
    visitPsiElement(o);
  }

  public void visitField(@NotNull ThriftField o) {
    visitFieldType(o);
  }

  public void visitFieldId(@NotNull ThriftFieldId o) {
    visitPsiElement(o);
  }

  public void visitFieldName(@NotNull ThriftFieldName o) {
    visitFieldNameElement(o);
  }

  public void visitFieldRequirement(@NotNull ThriftFieldRequirement o) {
    visitPsiElement(o);
  }

  public void visitFields(@NotNull ThriftFields o) {
    visitPsiElement(o);
  }

  public void visitFilePath(@NotNull ThriftFilePath o) {
    visitIncludedFileElement(o);
  }

  public void visitImplementsType(@NotNull ThriftImplementsType o) {
    visitPsiElement(o);
  }

  public void visitInclude(@NotNull ThriftInclude o) {
    visitPsiElement(o);
  }

  public void visitKeyType(@NotNull ThriftKeyType o) {
    visitType(o);
  }

  public void visitLanguageName(@NotNull ThriftLanguageName o) {
    visitPsiElement(o);
  }

  public void visitListType(@NotNull ThriftListType o) {
    visitType(o);
  }

  public void visitMapType(@NotNull ThriftMapType o) {
    visitType(o);
  }

  public void visitMessage(@NotNull ThriftMessage o) {
    visitSpec(o);
  }

  public void visitMessageField(@NotNull ThriftMessageField o) {
    visitField(o);
  }

  public void visitMessageFields(@NotNull ThriftMessageFields o) {
    visitPsiElement(o);
  }

  public void visitMessageVariant(@NotNull ThriftMessageVariant o) {
    visitPsiElement(o);
  }

  public void visitMethodName(@NotNull ThriftMethodName o) {
    visitPsiElement(o);
  }

  public void visitMethodParam(@NotNull ThriftMethodParam o) {
    visitField(o);
  }

  public void visitNamespace(@NotNull ThriftNamespace o) {
    visitPsiElement(o);
  }

  public void visitNamespacePackage(@NotNull ThriftNamespacePackage o) {
    visitPsiElement(o);
  }

  public void visitParams(@NotNull ThriftParams o) {
    visitPsiElement(o);
  }

  public void visitPrimitiveType(@NotNull ThriftPrimitiveType o) {
    visitType(o);
  }

  public void visitReferenceProgram(@NotNull ThriftReferenceProgram o) {
    visitPsiElement(o);
  }

  public void visitReferenceTypeName(@NotNull ThriftReferenceTypeName o) {
    visitReferenceTypeNameElement(o);
  }

  public void visitReferenceValue(@NotNull ThriftReferenceValue o) {
    visitReferenceValueElement(o);
  }

  public void visitService(@NotNull ThriftService o) {
    visitSpec(o);
  }

  public void visitServiceExceptions(@NotNull ThriftServiceExceptions o) {
    visitPsiElement(o);
  }

  public void visitServiceMethod(@NotNull ThriftServiceMethod o) {
    visitPsiElement(o);
  }

  public void visitServiceMethods(@NotNull ThriftServiceMethods o) {
    visitPsiElement(o);
  }

  public void visitServiceParams(@NotNull ThriftServiceParams o) {
    visitPsiElement(o);
  }

  public void visitServiceReturnType(@NotNull ThriftServiceReturnType o) {
    visitPsiElement(o);
  }

  public void visitSpec(@NotNull ThriftSpec o) {
    visitSpecElement(o);
  }

  public void visitType(@NotNull ThriftType o) {
    visitPsiElement(o);
  }

  public void visitTypeName(@NotNull ThriftTypeName o) {
    visitTypeNameElement(o);
  }

  public void visitTypeReference(@NotNull ThriftTypeReference o) {
    visitType(o);
  }

  public void visitTypedef(@NotNull ThriftTypedef o) {
    visitSpec(o);
  }

  public void visitValueId(@NotNull ThriftValueId o) {
    visitPsiElement(o);
  }

  public void visitValueName(@NotNull ThriftValueName o) {
    visitValueNameElement(o);
  }

  public void visitFieldNameElement(@NotNull ThriftFieldNameElement o) {
    visitPsiElement(o);
  }

  public void visitFieldType(@NotNull ThriftFieldType o) {
    visitPsiElement(o);
  }

  public void visitIncludedFileElement(@NotNull ThriftIncludedFileElement o) {
    visitPsiElement(o);
  }

  public void visitReferenceTypeNameElement(@NotNull ThriftReferenceTypeNameElement o) {
    visitPsiElement(o);
  }

  public void visitReferenceValueElement(@NotNull ThriftReferenceValueElement o) {
    visitPsiElement(o);
  }

  public void visitSpecElement(@NotNull ThriftSpecElement o) {
    visitPsiElement(o);
  }

  public void visitTypeNameElement(@NotNull ThriftTypeNameElement o) {
    visitPsiElement(o);
  }

  public void visitValueNameElement(@NotNull ThriftValueNameElement o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
