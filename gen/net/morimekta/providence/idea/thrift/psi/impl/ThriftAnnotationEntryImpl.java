// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftAnnotationEntryImpl extends ASTWrapperPsiElement implements ThriftAnnotationEntry {

  public ThriftAnnotationEntryImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitAnnotationEntry(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public ThriftAnnotationKey getAnnotationKey() {
    return findNotNullChildByClass(ThriftAnnotationKey.class);
  }

  @Override
  @Nullable
  public ThriftAnnotationValue getAnnotationValue() {
    return findChildByClass(ThriftAnnotationValue.class);
  }

}
