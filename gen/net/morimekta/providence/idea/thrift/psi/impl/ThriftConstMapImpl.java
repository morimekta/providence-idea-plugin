// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftConstMapImpl extends ThriftConstValueImpl implements ThriftConstMap {

  public ThriftConstMapImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitConstMap(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<ThriftComment> getCommentList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ThriftComment.class);
  }

  @Override
  @NotNull
  public List<ThriftConstMapEntry> getConstMapEntryList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ThriftConstMapEntry.class);
  }

}
