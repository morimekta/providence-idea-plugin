// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftConstReferenceImpl extends ThriftConstValueImpl implements ThriftConstReference {

  public ThriftConstReferenceImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitConstReference(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ThriftReferenceProgram getReferenceProgram() {
    return findChildByClass(ThriftReferenceProgram.class);
  }

  @Override
  @NotNull
  public ThriftReferenceTypeName getReferenceTypeName() {
    return findNotNullChildByClass(ThriftReferenceTypeName.class);
  }

  @Override
  @NotNull
  public ThriftReferenceValue getReferenceValue() {
    return findNotNullChildByClass(ThriftReferenceValue.class);
  }

  @Override
  public String getProgramName() {
    return ThriftPsiImplUtil.getProgramName(this);
  }

  @Override
  public ThriftFile getProgramThriftFile() {
    return ThriftPsiImplUtil.getProgramThriftFile(this);
  }

  @Override
  public String getTypeName() {
    return ThriftPsiImplUtil.getTypeName(this);
  }

}
