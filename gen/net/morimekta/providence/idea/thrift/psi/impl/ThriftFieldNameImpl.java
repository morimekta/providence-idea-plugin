// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import net.morimekta.providence.idea.thrift.psi.*;
import com.intellij.navigation.ItemPresentation;

public class ThriftFieldNameImpl extends ThriftFieldNameElementImpl implements ThriftFieldName {

  public ThriftFieldNameImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitFieldName(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  public String getName() {
    return ThriftPsiImplUtil.getName(this);
  }

  @Override
  public PsiElement setName(String name) {
    return ThriftPsiImplUtil.setName(this, name);
  }

  @Override
  public PsiElement getNameIdentifier() {
    return ThriftPsiImplUtil.getNameIdentifier(this);
  }

  @Override
  public ItemPresentation getPresentation() {
    return ThriftPsiImplUtil.getPresentation(this);
  }

}
