// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftMapTypeImpl extends ThriftTypeImpl implements ThriftMapType {

  public ThriftMapTypeImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitMapType(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<ThriftType> getTypeList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ThriftType.class);
  }

  @Override
  @Nullable
  public ThriftType getKeyType() {
    List<ThriftType> p1 = getTypeList();
    return p1.size() < 1 ? null : p1.get(0);
  }

  @Override
  @Nullable
  public ThriftType getValueType() {
    List<ThriftType> p1 = getTypeList();
    return p1.size() < 2 ? null : p1.get(1);
  }

}
