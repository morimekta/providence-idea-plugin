// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftMessageFieldImpl extends ThriftFieldImpl implements ThriftMessageField {

  public ThriftMessageFieldImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitMessageField(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ThriftAnnotations getAnnotations() {
    return findChildByClass(ThriftAnnotations.class);
  }

  @Override
  @Nullable
  public ThriftConstValue getConstValue() {
    return findChildByClass(ThriftConstValue.class);
  }

  @Override
  @Nullable
  public ThriftFieldId getFieldId() {
    return findChildByClass(ThriftFieldId.class);
  }

  @Override
  @NotNull
  public ThriftFieldName getFieldName() {
    return findNotNullChildByClass(ThriftFieldName.class);
  }

  @Override
  @Nullable
  public ThriftFieldRequirement getFieldRequirement() {
    return findChildByClass(ThriftFieldRequirement.class);
  }

  @Override
  @NotNull
  public ThriftType getType() {
    return findNotNullChildByClass(ThriftType.class);
  }

}
