// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftMessageImpl extends ThriftSpecImpl implements ThriftMessage {

  public ThriftMessageImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitMessage(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ThriftAnnotations getAnnotations() {
    return findChildByClass(ThriftAnnotations.class);
  }

  @Override
  @Nullable
  public ThriftImplementsType getImplementsType() {
    return findChildByClass(ThriftImplementsType.class);
  }

  @Override
  @Nullable
  public ThriftMessageFields getMessageFields() {
    return findChildByClass(ThriftMessageFields.class);
  }

  @Override
  @NotNull
  public ThriftMessageVariant getMessageVariant() {
    return findNotNullChildByClass(ThriftMessageVariant.class);
  }

  @Override
  @Nullable
  public ThriftTypeName getTypeName() {
    return findChildByClass(ThriftTypeName.class);
  }

}
