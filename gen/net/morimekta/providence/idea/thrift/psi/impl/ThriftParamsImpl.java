// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftParamsImpl extends ASTWrapperPsiElement implements ThriftParams {

  public ThriftParamsImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitParams(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<ThriftComment> getCommentList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ThriftComment.class);
  }

  @Override
  @NotNull
  public List<ThriftMethodParam> getMethodParamList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ThriftMethodParam.class);
  }

}
