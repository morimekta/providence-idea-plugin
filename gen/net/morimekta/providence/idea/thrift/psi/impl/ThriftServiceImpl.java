// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftServiceImpl extends ThriftSpecImpl implements ThriftService {

  public ThriftServiceImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitService(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ThriftAnnotations getAnnotations() {
    return findChildByClass(ThriftAnnotations.class);
  }

  @Override
  @Nullable
  public ThriftExtendsType getExtendsType() {
    return findChildByClass(ThriftExtendsType.class);
  }

  @Override
  @Nullable
  public ThriftServiceMethods getServiceMethods() {
    return findChildByClass(ThriftServiceMethods.class);
  }

  @Override
  @Nullable
  public ThriftTypeName getTypeName() {
    return findChildByClass(ThriftTypeName.class);
  }

}
