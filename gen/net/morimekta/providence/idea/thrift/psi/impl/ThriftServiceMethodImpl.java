// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftServiceMethodImpl extends ASTWrapperPsiElement implements ThriftServiceMethod {

  public ThriftServiceMethodImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitServiceMethod(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ThriftAnnotations getAnnotations() {
    return findChildByClass(ThriftAnnotations.class);
  }

  @Override
  @Nullable
  public ThriftMethodName getMethodName() {
    return findChildByClass(ThriftMethodName.class);
  }

  @Override
  @Nullable
  public ThriftServiceExceptions getServiceExceptions() {
    return findChildByClass(ThriftServiceExceptions.class);
  }

  @Override
  @Nullable
  public ThriftServiceParams getServiceParams() {
    return findChildByClass(ThriftServiceParams.class);
  }

  @Override
  @NotNull
  public ThriftServiceReturnType getServiceReturnType() {
    return findNotNullChildByClass(ThriftServiceReturnType.class);
  }

}
