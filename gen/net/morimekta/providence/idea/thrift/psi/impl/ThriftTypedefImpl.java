// This is a generated file. Not intended for manual editing.
package net.morimekta.providence.idea.thrift.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.*;
import net.morimekta.providence.idea.thrift.psi.*;

public class ThriftTypedefImpl extends ThriftSpecImpl implements ThriftTypedef {

  public ThriftTypedefImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ThriftVisitor visitor) {
    visitor.visitTypedef(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ThriftVisitor) accept((ThriftVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ThriftType getType() {
    return findChildByClass(ThriftType.class);
  }

  @Override
  @Nullable
  public ThriftTypeName getTypeName() {
    return findChildByClass(ThriftTypeName.class);
  }

}
