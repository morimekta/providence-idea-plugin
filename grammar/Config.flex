/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import net.morimekta.providence.idea.config.psi.ConfigTypes;
import com.intellij.psi.TokenType;

%%

%class ConfigLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

WHITE_SPACE=[\ \n\r\t]
COMMENT=("#")[^\r\n]*

ID=[_a-zA-Z][_a-zA-Z0-9]*
ID_SEP=[\.]
L_START=[\[]
L_END=[\]]
M_START=[\{]
M_END=[\}]
B_START=[(]
B_END=[)]

COMMA=[,]
COLON=[:]
EQUALS=[=]
SEMICO=[;]

STRING = [\\\"](([\\][\\\"\\\'nrftu])|[^\\\"\n\r])*[\\\"]
NUMBER = (-?(0|[1-9][0-9]*|0[0-7]+|0x[0-9a-fA-F]+)|-?(0?\.[0-9]+|[1-9][0-9]*\.[0-9]*)([eE][+-]?[0-9][0-9]*)?)

HEX=("hex")
HEX_CNT=[0-9a-fA-F]+
B64=("b64")
B64_CNT=[A-Za-z0-9+/_-]+[=]{0,2}

INCLUDE=("include")
AS=("as")
DEF=("def")
UNDEF=("undefined")
TRUE=("true")
FALSE=("false")

%state WANT_B64_START
%state WANT_B64_CONTENT
%state WANT_HEX_START
%state WANT_HEX_CONTENT
%state WANT_HEX_END

%state WANT_IDENTIFIER
%state WANT_IDENTIFIER2

%%

// -------------------------- BINARY --------------------------

<WANT_B64_START> {
  {B_START} { yybegin(WANT_B64_CONTENT);
              return ConfigTypes.BINARY_START; }
  {WHITE_SPACE}+ { return TokenType.WHITE_SPACE; }
  .         { yystate_pop();
              yypushback(yytext().length()); }
}
<WANT_B64_CONTENT> {
  {B64_CNT} { return ConfigTypes.B64_CONTENT; }
}

<WANT_HEX_START> {
  {B_START} { yybegin(WANT_HEX_CONTENT);
              return ConfigTypes.BINARY_START; }
  .         { yystate_pop();
              yypushback(yytext().length()); }
}
<WANT_HEX_CONTENT> {
  {HEX_CNT} { yybegin(WANT_HEX_END);
              return ConfigTypes.HEX_CONTENT; }
}

<WANT_B64_CONTENT, WANT_HEX_CONTENT, WANT_HEX_END> {
  {B_END}   { yystate_pop();
              return ConfigTypes.BINARY_END; }
}

<WANT_B64_START, WANT_B64_CONTENT> {
  {COMMENT} { return ConfigTypes.COMMENT; }
  {WHITE_SPACE}+ { return TokenType.WHITE_SPACE; }
}

// -------------------------- IDENTIFIER --------------------------

<WANT_IDENTIFIER> {
  {ID_SEP}  { yybegin(WANT_IDENTIFIER2);
              return ConfigTypes.DOT; }
  .         { yystate_pop();
              yypushback(yytext().length()); }
}
<WANT_IDENTIFIER2> {
  {ID_SEP}  { return ConfigTypes.DOT; }
  {ID}      { yybegin(WANT_IDENTIFIER);
              return ConfigTypes.IDENTIFIER; }
  .         { yystate_pop();
              yypushback(yytext().length()); }
}

// -------------------------- REUSABLE --------------------------

[&]         { return ConfigTypes.REUSABLE; }

// -------------------------- MAPS & LISTS --------------------------

{M_START}   { return ConfigTypes.MAP_START; }
{M_END}     { return ConfigTypes.MAP_END; }

{L_START}   { return ConfigTypes.LIST_START; }
{L_END}     { return ConfigTypes.LIST_END; }

// -------------------------- ROOT --------------------------

{INCLUDE}   { return ConfigTypes.KW_INCLUDE; }
{AS}        { return ConfigTypes.KW_AS; }
{DEF}       { return ConfigTypes.KW_DEF; }

// -------------------------- VALUES --------------------------

{UNDEF}     { return ConfigTypes.KW_UNDEFINED; }
{TRUE}      { return ConfigTypes.KW_TRUE; }
{FALSE}     { return ConfigTypes.KW_FALSE; }

{B64}       { yystate_push(WANT_B64_START);
              return ConfigTypes.KW_B64; }
{HEX}       { yystate_push(WANT_HEX_START);
              return ConfigTypes.KW_HEX; }
{ID}        { yystate_push(WANT_IDENTIFIER);
              return ConfigTypes.IDENTIFIER; }

{NUMBER}    { return ConfigTypes.NUMBER; }
{STRING}    { return ConfigTypes.STRING; }

{SEMICO}    { return ConfigTypes.SEMICOLON; }
{COMMA}     { return ConfigTypes.COMMA; }
{EQUALS}    { return ConfigTypes.EQUALS; }
{COLON}     { return ConfigTypes.COLON; }

{COMMENT}   { return ConfigTypes.COMMENT; }
{WHITE_SPACE}+ {
              return TokenType.WHITE_SPACE; }

// -------------------------- BAD STUFF --------------------------

// Non-terminated string literal.
[\"][^\"\n\r]*[\n\r] { return TokenType.BAD_CHARACTER; }

// Actually non-syntax character.
.           { return TokenType.BAD_CHARACTER; }