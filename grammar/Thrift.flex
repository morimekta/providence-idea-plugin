/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import net.morimekta.providence.idea.thrift.psi.ThriftTypes;
import com.intellij.psi.TokenType;

%%

%class ThriftLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

WHITE_SPACE   = [\ \t]
NEWLINE       = [\r]?[\n]
SHELL_COMMENT = [#]
JAVA_COMMENT  = [/][/]

IDENTIFIER    = [_a-zA-Z][_a-zA-Z0-9]*
IDENTIFIER_DOT= [\.]
LIST_START    = [\[]
LIST_END      = [\]]
MAP_START     = [\{]
MAP_END       = [\}]
PARAM_START   = [(]
PARAM_END     = [)]
GENERIC_START = [<]
GENERIC_END   = [>]

EQUALS        = [=]
COMMA         = [,]
SEMICOLON     = [;]
COLON         = [:]

LITERAL       = [\'](([\\][\\\"\'nrftu])|[^\\\'\n\r])*[\']
STRING        = [\"](([\\][\\\"\'nrftu])|[^\\\"\n\r])*[\"]
ZERO          = [0]
ID            = [1-9][0-9]*
NUMBER        = (-?([1-9][0-9]*|0[0-7]+|0x[0-9a-fA-F]+)|-?(0?\.[0-9]+|[1-9][0-9]*\.[0-9]*)([eE][+-]?[0-9][0-9]*)?)

DOC_BLOCK     = [/][*][*]+[/]
DOC_START     = [/][*]+
DOC_END       = [*]+[/]

// comment content that does not contain '*' (but may contain '/')
//              or that starts with '*', but does not end the comment.
DOC_WORD      = ([^\ \t\n\r*]+|[*]+[^\n\r/*]+)([^\n\r*]+|[*]+[^\n\r/*]+)*
// comment list of '*' for EOL.
DOC_STAR_EOL  = [*]+[\ \t]*

%state WANT_IDENTIFIER
%state WANT_IDENTIFIER2
%state DOC_COMMENT

%%

<DOC_COMMENT> {
  {DOC_END} |
  {DOC_WORD}+ {DOC_END} {
              yybegin(YYINITIAL);
              return ThriftTypes.DOC_COMMENT; }
  {DOC_STAR_EOL} {NEWLINE} |
  {DOC_WORD}+ {DOC_STAR_EOL}? {NEWLINE} {
              return ThriftTypes.DOC_COMMENT; }

  {WHITE_SPACE}*{NEWLINE} |
  {WHITE_SPACE}+ {
              return TokenType.WHITE_SPACE; }

  .         { return TokenType.BAD_CHARACTER; }
}
<WANT_IDENTIFIER> {
  {IDENTIFIER_DOT} {
              yybegin(WANT_IDENTIFIER2);
              return ThriftTypes.DOT; }
  .         { yybegin(YYINITIAL);
              yypushback(yytext().length()); }
}
<WANT_IDENTIFIER2> {
  {IDENTIFIER_DOT} {
              return ThriftTypes.DOT; }
  {IDENTIFIER} {
              yybegin(WANT_IDENTIFIER);
              return ThriftTypes.IDENTIFIER; }
  .         { yybegin(YYINITIAL);
              yypushback(yytext().length()); }
}

// THRIFT KEYWORDS

"namespace" { return ThriftTypes.KW_NAMESPACE; }
"include"   { return ThriftTypes.KW_INCLUDE; }
"enum"      { return ThriftTypes.KW_ENUM; }
"struct"    { return ThriftTypes.KW_STRUCT; }
"union"     { return ThriftTypes.KW_UNION; }
"exception" { return ThriftTypes.KW_EXCEPTION; }
// .providence only
"interface" { return ThriftTypes.KW_INTERFACE; }

"const"     { return ThriftTypes.KW_CONST; }
"typedef"   { return ThriftTypes.KW_TYPEDEF; }
"service"   { return ThriftTypes.KW_SERVICE; }

"extends"   { return ThriftTypes.KW_EXTENDS; }
"throws"    { return ThriftTypes.KW_THROWS; }
"oneway"    { return ThriftTypes.KW_ONEWAY; }
"void"      { return ThriftTypes.KW_VOID; }
// .providence only
"implements" { return ThriftTypes.KW_IMPLEMENTS; }
"of"        { return ThriftTypes.KW_OF; }

"optional"  { return ThriftTypes.KW_OPTIONAL; }
"required"  { return ThriftTypes.KW_REQUIRED; }

"bool"      { return ThriftTypes.KW_BOOL; }
"byte"      { return ThriftTypes.KW_BYTE; }
"i8"        { return ThriftTypes.KW_I8; }
"i16"       { return ThriftTypes.KW_I16; }
"i32"       { return ThriftTypes.KW_I32; }
"i64"       { return ThriftTypes.KW_I64; }
"double"    { return ThriftTypes.KW_DOUBLE; }
"string"    { return ThriftTypes.KW_STRING; }
"binary"    { return ThriftTypes.KW_BINARY; }

"list"      { return ThriftTypes.KW_LIST; }
"set"       { return ThriftTypes.KW_SET; }
"map"       { return ThriftTypes.KW_MAP; }

"true"      { return ThriftTypes.KW_TRUE; }
"false"     { return ThriftTypes.KW_FALSE; }
"null"      { return ThriftTypes.KW_NULL; }

// CONST KEYWORDS

{IDENTIFIER} { yybegin(WANT_IDENTIFIER);
              return ThriftTypes.IDENTIFIER; }

{ZERO}      { return ThriftTypes.ZERO; }
{ID}        { return ThriftTypes.ID; }
{NUMBER}    { return ThriftTypes.NUMBER; }

{STRING}|{LITERAL} {
              return ThriftTypes.LITERAL; }

{SEMICOLON} { return ThriftTypes.SEMICOLON; }

{EQUALS}    { return ThriftTypes.EQUALS; }
{COMMA}     { return ThriftTypes.COMMA; }
{SEMICOLON} { return ThriftTypes.SEMICOLON; }
{COLON}     { return ThriftTypes.COLON; }

{MAP_START}     { return ThriftTypes.MAP_START; }
{MAP_END}       { return ThriftTypes.MAP_END; }
{PARAM_START}   { return ThriftTypes.PARAM_START; }
{PARAM_END}     { return ThriftTypes.PARAM_END; }
{LIST_START}    { return ThriftTypes.LIST_START; }
{LIST_END}      { return ThriftTypes.LIST_END; }
{GENERIC_START} { return ThriftTypes.GENERIC_START; }
{GENERIC_END}   { return ThriftTypes.GENERIC_END; }

// -----------------------------------

{SHELL_COMMENT} ~{NEWLINE} |
{JAVA_COMMENT} ~{NEWLINE} |
{SHELL_COMMENT} [^\r\n]* |
{JAVA_COMMENT} [^\r\n]* { return ThriftTypes.LINE_COMMENT; }

{DOC_BLOCK} { return ThriftTypes.DOC_COMMENT; }
{DOC_START} ({WHITE_SPACE}|{DOC_WORD})* {DOC_END} {
              return ThriftTypes.DOC_COMMENT; }
{DOC_START} ({WHITE_SPACE}|{DOC_WORD})* {DOC_STAR_EOL}? {NEWLINE} {
              yybegin(DOC_COMMENT);
              return ThriftTypes.DOC_COMMENT; }

{WHITE_SPACE}*{NEWLINE} |
{WHITE_SPACE}+  { return TokenType.WHITE_SPACE; }

// Mangled block comment.
[/][*]+|[*]+[/]|[/] { return TokenType.BAD_CHARACTER; }
// Mangled string or literal.
[\'][^\'\n\r]*{NEWLINE} { return TokenType.BAD_CHARACTER; }
[\"][^\"\n\r]*{NEWLINE} { return TokenType.BAD_CHARACTER; }

// Actually non-syntax character.
.           { return TokenType.BAD_CHARACTER; }
