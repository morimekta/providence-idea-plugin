<idea-plugin>
    <id>net.morimekta.providence.idea</id>
    <name>Providence</name>
    <vendor email="oss@morimekta.net" url="http://morimekta.net/providence">morimekta.net</vendor>

    <description><![CDATA[
      Providence is an implementation of the Apache Thrift serialization IDL
      system (mainly for Java). It also has utilities for handling type-safe
      config. The plugin handles both the Thrift IDL files, and the providence
      config files.
    ]]></description>

    <change-notes><![CDATA[
      <ul>
        <li>2020-02-08 : Release 0.6.0: Using providence 2.6.0. Built by gradle.
        <li>2018-09-18 : Release 0.5.0: Supporting providence-2.0 (using beta1). Support for interfaces and union of.
        <li>2018-09-18 : Release 0.4.0: Using providence 1.6.1
        <li>2018-03-18 : Release 0.3.1: Fixing certain no-such file errors. Fix doc comments.
        <li>2018-03-04 : Release 0.3.0: Field auto-completion in config. Lots of cleanups and efficiency.
        <li>2018-02-24 : Release 0.2.0: Included File References. Less slowdowns. Block Renaming.
        <li>2018-02-23 : Release 0.1.0: Field References.
        <li>2018-02-21 : Mostly usable and exported to GitHub.
        <li>2018-02-13 : More thrift helpers.
        <li>2018-02-10 : Thrift file syntax.
        <li>2018-02-03 : Initial setup.
      </ul>
    ]]>
    </change-notes>

    <!-- please see http://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/plugin_compatibility.html
         on how to target different products -->
    <!-- uncomment to enable plugin in all products
    <depends>com.intellij.modules.lang</depends>
    -->

    <extensions defaultExtensionNs="com.intellij">
        <!-- Add your extensions here -->

        <!-- THRIFT -->
        <fileTypeFactory               implementation="net.morimekta.providence.idea.thrift.ThriftTypeFactory" />
        <colorSettingsPage             implementation="net.morimekta.providence.idea.thrift.ThriftColorSettingsPage"/>
        <psi.referenceContributor      implementation="net.morimekta.providence.idea.thrift.ThriftReferenceContributor"/>

        <lang.parserDefinition         language="providence" implementationClass="net.morimekta.providence.idea.thrift.ThriftParserDefinition"/>
        <lang.syntaxHighlighterFactory language="providence" implementationClass="net.morimekta.providence.idea.thrift.ThriftSyntaxHighlighterFactory"/>
        <lang.commenter                language="providence" implementationClass="net.morimekta.providence.idea.thrift.ThriftCommenter" />
        <lang.formatter                language="providence" implementationClass="net.morimekta.providence.idea.thrift.ThriftFormattingModelBuilder"/>
        <lang.findUsagesProvider       language="providence" implementationClass="net.morimekta.providence.idea.thrift.ThriftFindUsagesProvider"/>
        <lang.refactoringSupport       language="providence" implementationClass="net.morimekta.providence.idea.common.NoRefactoringSupportProvider"/>
        <lang.braceMatcher             language="providence" implementationClass="net.morimekta.providence.idea.thrift.ThriftPairedBraceMatcher" />
        <annotator                     language="providence" implementationClass="net.morimekta.providence.idea.thrift.ThriftAnnotator"/>
        <completion.contributor        language="providence" implementationClass="net.morimekta.providence.idea.thrift.ThriftCompletionContributor"/>
        <codeInsight.lineMarkerProvider language="JAVA" implementationClass="net.morimekta.providence.idea.thrift.ThriftImplementationLineMarkerProvider"/>

        <!-- CONFIG -->
        <fileTypeFactory               implementation="net.morimekta.providence.idea.config.ConfigTypeFactory" />
        <colorSettingsPage             implementation="net.morimekta.providence.idea.config.ConfigColorSettingsPage"/>
        <psi.referenceContributor      implementation="net.morimekta.providence.idea.config.ConfigReferenceContributor"/>

        <lang.parserDefinition         language="providence-config" implementationClass="net.morimekta.providence.idea.config.ConfigParserDefinition"/>
        <lang.syntaxHighlighterFactory language="providence-config" implementationClass="net.morimekta.providence.idea.config.ConfigSyntaxHighlighterFactory"/>
        <lang.commenter                language="providence-config" implementationClass="net.morimekta.providence.idea.config.ConfigCommenter" />
        <lang.formatter                language="providence-config" implementationClass="net.morimekta.providence.idea.config.ConfigFormattingModelBuilder"/>
        <lang.braceMatcher             language="providence-config" implementationClass="net.morimekta.providence.idea.config.ConfigPairedBraceMatcher" />
        <lang.refactoringSupport       language="providence-config" implementationClass="net.morimekta.providence.idea.common.NoRefactoringSupportProvider"/>
        <annotator                     language="providence-config" implementationClass="net.morimekta.providence.idea.config.ConfigAnnotator" />
        <completion.contributor        language="providence-config" implementationClass="net.morimekta.providence.idea.config.ConfigCompletionContributor" />

        <!-- COMMON -->
        <vetoRenameCondition implementation="net.morimekta.providence.idea.common.VetoRenamingCondition"/>
    </extensions>

    <actions>
        <!-- Add your actions here -->
    </actions>

</idea-plugin>
