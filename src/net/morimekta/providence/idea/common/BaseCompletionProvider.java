/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.common;

import com.intellij.codeInsight.completion.CompletionInitializationContext;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;

public abstract class BaseCompletionProvider extends CompletionProvider<CompletionParameters> {
    private static final String MAGIC_CURSOR_STRING = CompletionInitializationContext.DUMMY_IDENTIFIER_TRIMMED;

    protected static String getCompletePrefixString(String annotatedValue) {
        int magic = annotatedValue.indexOf(MAGIC_CURSOR_STRING);
        if (magic >= 0) {
            if (magic > 0) {
                return annotatedValue.substring(0, magic);
            }
            return "";
        }

        return annotatedValue;
    }

    protected static String getStripPrefix(CompletionResultSet resultSet, String completePrefix) {
        String prefix = resultSet.getPrefixMatcher().getPrefix();
        if (prefix.length() > 0) {
            int pos = completePrefix.lastIndexOf(prefix);
            if (pos > 0) {
                return completePrefix.substring(0, pos);
            }
        }
        return "";
    }
}
