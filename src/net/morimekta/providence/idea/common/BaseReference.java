/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.common;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiPolyVariantReference;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.ResolveResult;
import com.intellij.psi.impl.source.resolve.ResolveCache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;

public abstract class BaseReference<Psi extends PsiElement> extends PsiReferenceBase<Psi> implements PsiPolyVariantReference {
    private final ResolveCache cache;

    public BaseReference(@Nonnull Psi element, ResolveCache cache) {
        super(element);
        this.cache = cache;
        update();
    }

    protected abstract void update();

    @NotNull
    protected abstract ResolveResult[] multiResolveCached(boolean incomplete);

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        if (cache == null) {
            return multiResolveCached(incompleteCode);
        }

        return cache.resolveWithCaching(this,
                                        (ref, incomplete) -> this.multiResolveCached(incomplete),
                                        true,
                                        incompleteCode,
                                        myElement.getContainingFile());
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        ResolveResult[] resolveResults = multiResolve(false);
        return resolveResults.length == 1 ? resolveResults[0].getElement() : null;
    }

    @NotNull
    @Override
    public Object[] getVariants() {
        return new Object[0];
    }
}
