/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.common;

import com.google.common.collect.ImmutableList;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.indexing.FileBasedIndex;
import net.morimekta.providence.idea.thrift.ThriftFileType;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import org.jetbrains.annotations.NonNls;

import java.util.Collection;
import java.util.List;

public class ProvidenceIdeaUtil {
    public static PsiFile resolveFile(@NonNls PsiDirectory directory, @NonNls String path) {
        if (directory == null || path.isEmpty()) {
            return null;
        }

        if (path.startsWith("../")) {
            return resolveFile(directory.getParentDirectory(), path.substring(3));
        } else if (path.contains("/")) {
            int idx = path.indexOf("/");
            String dirName = path.substring(0, idx);

            String rest = path.substring(idx + 1);
            PsiDirectory subdir = directory.findSubdirectory(dirName);
            if (subdir != null) {
                return resolveFile(subdir, rest);
            }
        } else {
            return directory.findFile(path);
        }

        return null;
    }

    public static List<ThriftFile> getThriftFilesForTypePrefix(Project project, String typePrefix) {
        String[] tuple = typePrefix.split("[.]", 2);
        if (tuple.length == 2) {
            return getThriftFilesForProgram(project, tuple[0]);
        }

        Collection<VirtualFile> virtualFiles = FileTypeIndex.getFiles(ThriftFileType.INSTANCE,
                                                                      GlobalSearchScope.allScope(project));

        ImmutableList.Builder<ThriftFile> fieldNames = ImmutableList.builder();
        for (VirtualFile virtualFile : virtualFiles) {
            ThriftFile thriftFile = (ThriftFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (thriftFile != null && thriftFile.getThriftProgramName().equals(tuple[0])) {
                fieldNames.add(thriftFile);
            }
        }
        return fieldNames.build();
    }

    public static List<ThriftFile> getThriftFilesForJavaPackage(Project project, String javaPackage) {
        Collection<VirtualFile> virtualFiles = FileTypeIndex.getFiles(ThriftFileType.INSTANCE,
                                                                      GlobalSearchScope.allScope(project));
        ImmutableList.Builder<ThriftFile> thriftFiles = ImmutableList.builder();
        for (VirtualFile virtualFile : virtualFiles) {
            ThriftFile thriftFile = (ThriftFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (thriftFile != null &&
                thriftFile.getJavaNamespace() != null &&
                thriftFile.getJavaNamespace().equals(javaPackage)) {
                thriftFiles.add(thriftFile);
            }
        }
        return thriftFiles.build();
    }

    private static List<ThriftFile> getThriftFilesForProgram(Project project, String program) {
        Collection<VirtualFile> virtualFiles =
                FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, ThriftFileType.INSTANCE,
                                                                GlobalSearchScope.allScope(project));
        ImmutableList.Builder<ThriftFile> thriftFiles = ImmutableList.builder();
        for (VirtualFile virtualFile : virtualFiles) {
            ThriftFile thriftFile = (ThriftFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (thriftFile != null && thriftFile.getThriftProgramName().equals(program)) {
                thriftFiles.add(thriftFile);
            }
        }
        return thriftFiles.build();
    }
}
