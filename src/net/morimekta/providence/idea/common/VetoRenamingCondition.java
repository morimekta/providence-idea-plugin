/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.common;

import com.intellij.openapi.util.Condition;
import com.intellij.psi.PsiElement;
import net.morimekta.providence.idea.config.psi.ConfigFieldName;
import net.morimekta.providence.idea.config.psi.ConfigIncludedFile;
import net.morimekta.providence.idea.config.psi.ConfigTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldName;
import net.morimekta.providence.idea.thrift.psi.ThriftReferenceTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftReferenceValue;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftValueName;

public class VetoRenamingCondition implements Condition<PsiElement> {
    @Override
    public boolean value(PsiElement element) {
        return element instanceof ThriftFieldName ||
               element instanceof ThriftTypeName ||
               element instanceof ThriftValueName ||
               element instanceof ThriftReferenceValue ||
               element instanceof ThriftReferenceTypeName ||

               element instanceof ConfigFieldName ||
               element instanceof ConfigTypeName ||
               element instanceof ConfigIncludedFile;
    }
}
