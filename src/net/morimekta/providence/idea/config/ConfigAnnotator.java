/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import net.morimekta.providence.idea.config.psi.ConfigConfigType;
import net.morimekta.providence.idea.config.psi.ConfigDefineEnum;
import net.morimekta.providence.idea.config.psi.ConfigEnumValue;
import net.morimekta.providence.idea.config.psi.ConfigFieldName;
import net.morimekta.providence.idea.config.psi.ConfigFile;
import net.morimekta.providence.idea.config.psi.ConfigInclude;
import net.morimekta.providence.idea.config.psi.ConfigRef;
import net.morimekta.providence.idea.config.psi.ConfigRefField;
import net.morimekta.providence.idea.config.psi.ConfigRefOrEnum;
import net.morimekta.providence.idea.config.psi.ConfigReusableDef;
import net.morimekta.providence.idea.config.psi.ConfigTypeName;
import net.morimekta.providence.idea.config.reference.ConfigTypeReference;
import org.jetbrains.annotations.NotNull;

public class ConfigAnnotator implements Annotator {
    @Override
    public void annotate(@NotNull PsiElement element,
                         @NotNull AnnotationHolder holder) {
        annotateSyntaxHighlight(element, holder);

        if (element instanceof ConfigTypeName) {
            ConfigTypeReference reference = (ConfigTypeReference) element.getReference();
            if (element.getParent() instanceof ConfigConfigType) {
                if (reference != null && reference.multiResolve(false).length == 0) {
                    holder.createWarningAnnotation(element, "Unknown Config Type: \"" + element.getText() + "\"");
                }
            } else if (element.getParent() instanceof ConfigDefineEnum) {
                if (reference != null && reference.resolve() == null) {
                    holder.createWarningAnnotation(element, "Unknown Enum Type: \"" + element.getText() + "\"");
                }
            }
        } else if (element instanceof ConfigInclude) {
            ConfigInclude include = (ConfigInclude) element;
            if (include.getIncludedFile() != null &&
                include.getIncludedConfigFile() == null) {
                holder.createErrorAnnotation(
                        include.getIncludedFile(),
                        "No such config file " + include.getIncludedFile().getText());
            }
        } else if (element instanceof ConfigReusableDef) {
            holder.createWarningAnnotation(element, "Reusable objects are deprecated, use def instead.");
        } else if (element instanceof ConfigRefOrEnum) {
            ConfigRefOrEnum croe = (ConfigRefOrEnum) element;
            if (!isEnumValue((ConfigRefOrEnum) element)) {
                if (!((ConfigFile) croe.getContainingFile()).getReferenceNames().contains(croe.getRefId().getText())) {
                    holder.createErrorAnnotation(
                            element,
                            "No such reference \"" + croe.getRefId().getText() + "\"");
                }
            }
        } else if (element instanceof ConfigRef) {
            ConfigRef ref = (ConfigRef) element;
            if (!((ConfigFile) element.getContainingFile()).getReferenceNames().contains(ref.getRefId().getText())) {
                holder.createErrorAnnotation(element, "No such reference \"" + ref.getRefId().getText() + "\"");
            }
        } else if (element instanceof ConfigFieldName) {
            ConfigFieldName name = (ConfigFieldName) element;

            if (name.hasPossibleContainedType()) {
                if (name.getPossibleFields().size() == 0) {
                    holder.createWarningAnnotation(element, "Unknown field \"" + name.getName() + "\"");
                }
            }
        }
    }

    private void annotateSyntaxHighlight(@NotNull PsiElement element,
                                         @NotNull AnnotationHolder holder) {
        if (element instanceof ConfigConfigType) {
            holder.createInfoAnnotation(element, null)
                  .setTextAttributes(ConfigSyntaxHighlighter.TYPE_NAME);
        } else if (element instanceof ConfigFieldName ||
                   element instanceof ConfigRefField) {
            holder.createInfoAnnotation(element, null)
                  .setTextAttributes(ConfigSyntaxHighlighter.FIELD_NAME);
        } else if (element instanceof ConfigRef) {
            holder.createInfoAnnotation(element, null)
                  .setTextAttributes(ConfigSyntaxHighlighter.REFERENCE);
        } else if (element instanceof ConfigEnumValue) {
            holder.createInfoAnnotation(element, null)
                  .setTextAttributes(ConfigSyntaxHighlighter.VALUE_NAME);
        } else if (element instanceof ConfigRefOrEnum) {
            if (isEnumValue((ConfigRefOrEnum) element)) {
                holder.createInfoAnnotation(element, null)
                      .setTextAttributes(ConfigSyntaxHighlighter.VALUE_NAME);
            }
        }
    }

    private boolean isEnumValue(ConfigRefOrEnum croe) {
        if (croe.getRefPath() != null) return false;

        // TODO: Figure out by field type.
        String name = croe.getRefId().getText();
        if (name.equals(name.toUpperCase())) {
            // assume it's an enum ...
            return true;
        } else {
            return !((ConfigFile) croe.getContainingFile()).getReferenceNames().contains(name);
        }
    }
}
