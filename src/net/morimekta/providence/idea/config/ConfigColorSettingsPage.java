/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class ConfigColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Comment", ConfigSyntaxHighlighter.COMMENT),
            new AttributesDescriptor("Braces", ConfigSyntaxHighlighter.BRACE),
            new AttributesDescriptor("Separators", ConfigSyntaxHighlighter.SEPARATOR),
            new AttributesDescriptor("Keyword", ConfigSyntaxHighlighter.KEYWORD),
            new AttributesDescriptor("Type Name", ConfigSyntaxHighlighter.TYPE_NAME),
            new AttributesDescriptor("Field Name", ConfigSyntaxHighlighter.FIELD_NAME),
            new AttributesDescriptor("Value Name", ConfigSyntaxHighlighter.VALUE_NAME),
            new AttributesDescriptor("Reference", ConfigSyntaxHighlighter.REFERENCE),
            new AttributesDescriptor("Number", ConfigSyntaxHighlighter.NUMBER),
            new AttributesDescriptor("String", ConfigSyntaxHighlighter.STRING),
            new AttributesDescriptor("Bad Character", ConfigSyntaxHighlighter.BAD_CHAR),
            };

    @Nullable
    @Override
    public Icon getIcon() {
        return ConfigIcon.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new ConfigSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "include \"file.config\" as ref\n" +
               "\n" +
               "# comment\n" +
               "def direct = 1234\n" +
               "def {\n" +
               "  braced = \"string\"\n" +
               "  binary = hex(01020304)\n" +
               "}\n" +
               "\n" +
               "# more comments\n" +
               "program.ConfigType : ref {\n" +
               "  extended {\n" +
               "    field = braced\n" +
               "  }\n" +
               "  binary = b64( AbCdEf== ) # comment at end\n" +
               "  float = 1.23456e-5\n" +
               "}\n";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Providence Config";
    }
}
