/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config;

import com.intellij.formatting.Alignment;
import com.intellij.formatting.FormattingModel;
import com.intellij.formatting.FormattingModelBuilder;
import com.intellij.formatting.FormattingModelProvider;
import com.intellij.formatting.SpacingBuilder;
import com.intellij.formatting.Wrap;
import com.intellij.formatting.WrapType;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.tree.TokenSet;
import net.morimekta.providence.idea.config.psi.ConfigTypes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static net.morimekta.providence.idea.config.psi.ConfigTypes.COLON;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.COMMA;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.CONFIG;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.DEFINE;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.DEFINE_MAP;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.DEFINE_SPEC;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.FIELD;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.INCLUDE;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.KW_AS;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.MAP_END;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.MAP_ENTRY;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.MAP_START;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.SEMICOLON;
import static net.morimekta.providence.idea.config.psi.ConfigTypes.TYPE_NAME;

public class ConfigFormattingModelBuilder implements FormattingModelBuilder {
    @NotNull
    @Override
    public FormattingModel createModel(PsiElement psiElement, CodeStyleSettings codeStyleSettings) {
        return FormattingModelProvider.createFormattingModelForPsiFile(
                psiElement.getContainingFile(),
                new ConfigBlock(psiElement.getNode(),
                                Wrap.createWrap(WrapType.NONE, false),
                                Alignment.createAlignment(),
                                createSpaceBuilder(codeStyleSettings)),
                codeStyleSettings);
    }

    private static SpacingBuilder createSpaceBuilder(CodeStyleSettings settings) {
        return new SpacingBuilder(settings, ConfigLanguage.INSTANCE)
                .around(TokenSet.create(ConfigTypes.EQUALS, KW_AS))
                .spaces(1)
                .before(TokenSet.create(ConfigTypes.MAP_START, ConfigTypes.LIST_START))
                .spaces(1)
                .before(TokenSet.create(COLON, COMMA, SEMICOLON))
                .none()
                .after(TokenSet.create(COLON, COMMA))
                .spaces(1)
                .between(TYPE_NAME, TokenSet.create(MAP_START, COLON))
                .spaces(1)

                // Line separation
                .before(TokenSet.create(MAP_END))
                .lineBreakInCode()
                .after(TokenSet.create(MAP_END, SEMICOLON))
                .lineBreakInCode()

                .before(TokenSet.create(INCLUDE, DEFINE_SPEC, FIELD, MAP_ENTRY))
                .lineBreakInCode()
                .after(TokenSet.create(INCLUDE, DEFINE_MAP, DEFINE, FIELD, MAP_ENTRY))
                .lineBreakInCode()

                // Line spacing.
                .between(INCLUDE, TokenSet.create(DEFINE, CONFIG))
                .blankLines(1)
                .between(DEFINE, TokenSet.create(CONFIG))
                .blankLines(1);
    }

    @Nullable
    @Override
    public TextRange getRangeAffectingIndent(PsiFile psiFile, int i, ASTNode astNode) {
        return null;
    }
}
