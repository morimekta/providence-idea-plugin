/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config;

import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceRegistrar;
import net.morimekta.providence.idea.config.psi.ConfigTypes;
import net.morimekta.providence.idea.config.reference.ConfigFieldReferenceProvider;
import net.morimekta.providence.idea.config.reference.ConfigIncludeReferenceProvider;
import net.morimekta.providence.idea.config.reference.ConfigTypeReferenceProvider;
import org.jetbrains.annotations.NotNull;

public class ConfigReferenceContributor extends PsiReferenceContributor {
    @Override
    public void registerReferenceProviders(@NotNull PsiReferenceRegistrar registrar) {
        registrar.registerReferenceProvider(
                PlatformPatterns.psiElement(ConfigTypes.TYPE_NAME),
                ConfigTypeReferenceProvider.INSTANCE);
        registrar.registerReferenceProvider(
                PlatformPatterns.psiElement(ConfigTypes.FIELD_NAME),
                ConfigFieldReferenceProvider.INSTANCE);
        registrar.registerReferenceProvider(
                PlatformPatterns.psiElement(ConfigTypes.INCLUDED_FILE),
                ConfigIncludeReferenceProvider.INSTANCE);
    }

}
