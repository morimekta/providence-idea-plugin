/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import net.morimekta.providence.idea.config.psi.ConfigTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.EMPTY_ARRAY;
import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class ConfigSyntaxHighlighter extends SyntaxHighlighterBase {
    public static final TextAttributesKey COMMENT   = createTextAttributesKey(
            "CONFIG_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
    public static final TextAttributesKey BRACE     = createTextAttributesKey(
            "CONFIG_BRACE", DefaultLanguageHighlighterColors.BRACES);
    public static final TextAttributesKey SEPARATOR = createTextAttributesKey(
            "CONFIG_SEPARATOR", DefaultLanguageHighlighterColors.COMMA);

    public static final TextAttributesKey KEYWORD    = createTextAttributesKey(
            "CONFIG_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey TYPE_NAME  = createTextAttributesKey(
            "CONFIG_TYPE_NAME", DefaultLanguageHighlighterColors.CLASS_NAME);
    public static final TextAttributesKey FIELD_NAME = createTextAttributesKey(
            "CONFIG_FIELD_NAME", DefaultLanguageHighlighterColors.INSTANCE_FIELD);
    public static final TextAttributesKey VALUE_NAME = createTextAttributesKey(
            "CONFIF_ENUM_VALUE", DefaultLanguageHighlighterColors.CONSTANT);
    public static final TextAttributesKey REFERENCE  = createTextAttributesKey(
            "CONFIG_REFERENCE", DefaultLanguageHighlighterColors.IDENTIFIER);
    public static final TextAttributesKey NUMBER     = createTextAttributesKey(
            "CONFIG_NUMBER", DefaultLanguageHighlighterColors.NUMBER);
    public static final TextAttributesKey STRING     = createTextAttributesKey(
            "CONFIG_STRING", DefaultLanguageHighlighterColors.STRING);

    public static final TextAttributesKey BAD_CHAR   = createTextAttributesKey(
            "CONFIG_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

    private static final TextAttributesKey[] COMMENTS   = new TextAttributesKey[]{COMMENT};
    private static final TextAttributesKey[] BRACES     = new TextAttributesKey[]{BRACE};
    private static final TextAttributesKey[] SEPARATORS = new TextAttributesKey[]{SEPARATOR};

    private static final TextAttributesKey[] KEYWORDS    = new TextAttributesKey[]{KEYWORD};
    private static final TextAttributesKey[] NUMBERS     = new TextAttributesKey[]{NUMBER};
    private static final TextAttributesKey[] STRINGS     = new TextAttributesKey[]{STRING};

    private static final TextAttributesKey[] BAD_CHARACTERS = new TextAttributesKey[]{BAD_CHAR};

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new ConfigLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType type) {
        if (type.equals(TokenType.BAD_CHARACTER)) {
            return BAD_CHARACTERS;
        } else if (type.equals(ConfigTypes.COMMENT)) {
            return COMMENTS;
        } else if (type.equals(ConfigTypes.KW_INCLUDE) ||
                   type.equals(ConfigTypes.KW_AS) ||
                   type.equals(ConfigTypes.KW_DEF) ||
                   type.equals(ConfigTypes.KW_UNDEFINED) ||
                   type.equals(ConfigTypes.KW_TRUE) ||
                   type.equals(ConfigTypes.KW_FALSE)) {
            return KEYWORDS;
        } else if (type.equals(ConfigTypes.NUMBER)) {
            return NUMBERS;
        } else if (type.equals(ConfigTypes.STRING) ||
                   type.equals(ConfigTypes.KW_B64) ||
                   type.equals(ConfigTypes.KW_HEX) ||
                   type.equals(ConfigTypes.B64_CONTENT) ||
                   type.equals(ConfigTypes.HEX_CONTENT)) {
            return STRINGS;
        } else if (type.equals(ConfigTypes.COMMA) ||
                   type.equals(ConfigTypes.SEMICOLON) ||
                   type.equals(ConfigTypes.COLON) ||
                   type.equals(ConfigTypes.EQUALS)) {
            return SEPARATORS;
        } else if (type.equals(ConfigTypes.MAP_START) ||
                   type.equals(ConfigTypes.MAP_END) ||
                   type.equals(ConfigTypes.BINARY_START) ||
                   type.equals(ConfigTypes.BINARY_END) ||
                   type.equals(ConfigTypes.LIST_START) ||
                   type.equals(ConfigTypes.LIST_END)) {
            return BRACES;
        }

        return EMPTY_ARRAY;
    }
}
