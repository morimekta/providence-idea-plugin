/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.util.ProcessingContext;
import net.morimekta.providence.idea.common.BaseCompletionProvider;
import net.morimekta.providence.idea.config.psi.ConfigField;
import net.morimekta.providence.idea.config.psi.ConfigFieldName;
import net.morimekta.providence.idea.config.psi.ConfigMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;
import net.morimekta.providence.idea.thrift.psi.ThriftType;
import org.jetbrains.annotations.NotNull;

public class ConfigFieldCompletionProvider extends BaseCompletionProvider {
    public static final ConfigFieldCompletionProvider INSTANCE = new ConfigFieldCompletionProvider();

    @Override
    protected void addCompletions(@NotNull CompletionParameters parameters,
                                  ProcessingContext context,
                                  @NotNull CompletionResultSet resultSet) {
        try {
            PsiElement   element = parameters.getPosition();
            // IElementType type    = type(element);
            if (element.getParent() instanceof ConfigFieldName) {
                if (element.getParent().getParent() instanceof ConfigField) {
                    if (element.getParent().getParent().getParent() instanceof ConfigMessage) {
                        ConfigMessage message = (ConfigMessage) element.getParent().getParent().getParent();
                        String        prefix  = getCompletePrefixString(element.getText());
                        for (ThriftFieldType field : message.getPossibleFieldsForPrefix(prefix)) {
                            if (field.getFieldName() != null) {
                                resultSet.addElement(
                                        LookupElementBuilder
                                                .create(field.getFieldName())
                                                .withTypeText(typeString(field.getType()), true));
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String typeString(ThriftType type) {
        if (type == null) {
            return "";
        }
        return type.getText();
    }
}
