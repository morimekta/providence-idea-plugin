/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import net.morimekta.providence.idea.config.ConfigFileType;
import net.morimekta.providence.idea.config.ConfigLanguage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;

public class ConfigFile extends PsiFileBase {
    private Set<String> aliases;

    public ConfigFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, ConfigLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return ConfigFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Providence Config";
    }

    @Nullable
    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }

    @Override
    public void clearCaches() {
        super.clearCaches();
        aliases = null;
    }

    public Set<String> getReferenceNames() {
        if (aliases == null) {
            aliases = new TreeSet<>();
            visitAliases(alias -> aliases.add(alias.getText()));
        }
        return aliases;
    }

    private void visitAliases(Consumer<ConfigAlias> aliasConsumer) {
        for (ConfigInclude include : findChildrenByClass(ConfigInclude.class)) {
            aliasConsumer.accept(include.getAlias());
        }
        for (ConfigDefineSpec define : findChildrenByClass(ConfigDefineSpec.class)) {
            if (define.getDefine() != null) {
                aliasConsumer.accept(define.getDefine().getAlias());
            } else if (define.getDefineMap() != null &&
                       define.getDefineMap().getDefines() != null) {
                for (ConfigDefine def : define.getDefineMap().getDefines().getDefineList()) {
                    aliasConsumer.accept(def.getAlias());
                }
            }
        }
        for (ConfigConfig config : findChildrenByClass(ConfigConfig.class)) {
            if (config.getMessage() != null) {
                visitAliases(config.getMessage().getFields().getFieldList(), aliasConsumer);
            }
        }
    }

    private void visitAliases(List<ConfigField> fields, Consumer<ConfigAlias> aliasConsumer) {
        for (ConfigField field : fields) {
            if (field.getReusableDef() != null) {
                aliasConsumer.accept(field.getReusableDef().getAlias());
            }
            if (field.getValue() instanceof ConfigMessage) {
                visitAliases(((ConfigMessage) field.getValue()).getFields().getFieldList(), aliasConsumer);
            }
        }
    }
}
