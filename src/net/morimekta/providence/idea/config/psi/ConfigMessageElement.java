/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.psi;

import com.intellij.psi.PsiElement;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;
import net.morimekta.providence.idea.thrift.psi.ThriftMessage;

import java.util.List;

public interface ConfigMessageElement extends PsiElement {
    /** If this field set has a possible message type */
    boolean hasPossibleType();
    /** Get a list of types that can represent this message. */
    List<ThriftMessage> getPossibleMessageTypes();
    /** Get a list of possible fields that may be in this message. */
    List<ThriftFieldType> getPossibleFields();
    /** Get a list of possible fields with given name prefixed for this message. */
    List<ThriftFieldType> getPossibleFieldsForPrefix(String prefix);
    /** Get a list of possible fields matching given name for this message. */
    List<ThriftFieldType> getPossibleFieldsForName(String name);

}
