/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.psi.impl;

import com.google.common.collect.ImmutableList;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.morimekta.providence.idea.config.psi.ConfigFieldElement;
import net.morimekta.providence.idea.config.psi.ConfigFields;
import net.morimekta.providence.idea.config.psi.ConfigMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import net.morimekta.providence.idea.thrift.psi.ThriftMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftSpec;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeReference;
import org.jetbrains.annotations.NotNull;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

public abstract class ConfigFieldElementImpl extends ASTWrapperPsiElement implements ConfigFieldElement {
    private long                  timestamp = 0;
    private List<ThriftFieldType> possibleFields;
    private List<ThriftMessage>   possibleMessageTypes;

    public ConfigFieldElementImpl(@NotNull ASTNode node) {
        super(node);
    }

    @Override
    public List<ThriftMessage> getPossibleFieldMessageTypes() {
        if (possibleMessageTypes == null) {
            ArrayList<ThriftMessage> possible = new ArrayList<>();

            for (ThriftFieldType field : getPossibleFields()) {
                if (field.getType() instanceof ThriftTypeReference) {
                    ThriftTypeReference reference = (ThriftTypeReference) field.getType();
                    String              refName   = reference.getTypeName();
                    ThriftFile          refFile   = reference.getProgramThriftFile();

                    for (ThriftSpec spec : refFile.getSpecList()) {
                        if (spec instanceof ThriftMessage &&
                            refName.equals(spec.getDeclaredName())) {
                            possible.add((ThriftMessage) spec);
                        }
                    }
                }
            }

            possibleMessageTypes = ImmutableList.copyOf(possible);
            timestamp = Clock.systemUTC().millis() + 3000;
        }
        return possibleMessageTypes;
    }

    @Override
    public List<ThriftFieldType> getPossibleFields() {
        if (possibleFields == null) {
            ArrayList<ThriftFieldType> possible = new ArrayList<>();
            if (getParent() instanceof ConfigFields &&
                getParent().getParent() instanceof ConfigMessage ) {
                ConfigMessage config = (ConfigMessage) getParent().getParent();
                possible.addAll(config.getPossibleFieldsForName(getFieldName().getName()));
            }
            possibleFields = ImmutableList.copyOf(possible);
            timestamp = Clock.systemUTC().millis() + 3000;
        }
        return possibleFields;
    }

    @Override
    protected void clearUserData() {
        super.clearUserData();
        if (Clock.systemUTC().millis() > timestamp) {
            possibleFields = null;
            possibleMessageTypes = null;
        }
    }
}
