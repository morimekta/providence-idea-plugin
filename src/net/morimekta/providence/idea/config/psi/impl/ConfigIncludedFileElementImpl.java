/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiReference;
import com.intellij.psi.impl.source.resolve.ResolveCache;
import net.morimekta.providence.idea.config.psi.ConfigIncludedFile;
import net.morimekta.providence.idea.config.psi.ConfigIncludedFileElement;
import net.morimekta.providence.idea.config.reference.ConfigIncludeReference;
import org.jetbrains.annotations.NotNull;

public abstract class ConfigIncludedFileElementImpl extends ASTWrapperPsiElement implements ConfigIncludedFileElement {
    private ConfigIncludeReference reference;

    public ConfigIncludedFileElementImpl(@NotNull ASTNode node) {
        super(node);
    }

    @Override
    public PsiReference getReference() {
        if (reference == null) {
            reference = new ConfigIncludeReference((ConfigIncludedFile) this, ResolveCache.getInstance(getProject()));
        }
        return reference;
    }

    @NotNull
    @Override
    public PsiReference[] getReferences() {
        return new PsiReference[]{getReference()};
    }
}
