/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.psi.impl;

import com.google.common.collect.ImmutableList;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.ResolveResult;
import net.morimekta.providence.idea.config.psi.ConfigConfig;
import net.morimekta.providence.idea.config.psi.ConfigConfigType;
import net.morimekta.providence.idea.config.psi.ConfigField;
import net.morimekta.providence.idea.config.psi.ConfigList;
import net.morimekta.providence.idea.config.psi.ConfigMapEntry;
import net.morimekta.providence.idea.config.psi.ConfigMessageElement;
import net.morimekta.providence.idea.config.reference.ConfigTypeReference;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;
import net.morimekta.providence.idea.thrift.psi.ThriftListType;
import net.morimekta.providence.idea.thrift.psi.ThriftMapType;
import net.morimekta.providence.idea.thrift.psi.ThriftMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftType;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeReference;
import net.morimekta.providence.idea.thrift.reference.TypeNameReference;
import org.jetbrains.annotations.NotNull;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

public abstract class ConfigMessageElementImpl extends ASTWrapperPsiElement implements ConfigMessageElement {
    private List<ThriftFieldType> possibleFields;
    private List<ThriftMessage>   possibleMessageTypes;
    private long                  timestamp;

    public ConfigMessageElementImpl(@NotNull ASTNode node) {
        super(node);
    }

    private void possibleFromTypeReference(ConfigConfigType configType, ArrayList<ThriftMessage> possible) {
        ConfigTypeReference reference = (ConfigTypeReference) configType.getTypeName().getReference();
        if (reference != null) {
            for (ResolveResult result : reference.multiResolve(false)) {
                if (result.getElement() instanceof ThriftTypeName) {
                    if (result.getElement().getParent() instanceof ThriftMessage) {
                        possible.add((ThriftMessage) result.getElement().getParent());
                    }
                }
            }
        }
    }

    private void possibleFromThriftType(ThriftTypeReference thriftType, ArrayList<ThriftMessage> possible) {
        if (thriftType != null) {
            TypeNameReference reference = (TypeNameReference) thriftType.getReferenceTypeName().getReference();
            if (reference != null) {
                for (ResolveResult result : reference.multiResolve(false)) {
                    if (result.getElement() instanceof ThriftTypeName) {
                        if (result.getElement().getParent() instanceof ThriftMessage) {
                            possible.add((ThriftMessage) result.getElement().getParent());
                        }
                    }
                }
            }
        }
    }

    @Override
    public List<ThriftMessage> getPossibleMessageTypes() {
        if (possibleMessageTypes == null) {
            ArrayList<ThriftMessage> possible = new ArrayList<>();
            if (getParent() instanceof ConfigConfig) {
                ConfigConfig        config    = (ConfigConfig) getParent();
                possibleFromTypeReference(config.getConfigType(), possible);
            } else if (getParent() instanceof ConfigField) {
                ConfigField field = (ConfigField) getParent();
                possible.addAll(field.getPossibleFieldMessageTypes());
            } else if (getParent() instanceof ConfigList &&
                       getParent().getParent() instanceof ConfigField) {
                ConfigField field = (ConfigField) getParent().getParent();

                for (ThriftFieldType thriftField : field.getPossibleFields()) {
                    // expect set / list type.
                    if (thriftField.getType() instanceof ThriftListType) {
                        ThriftType entryType = ((ThriftListType) thriftField.getType()).getType();
                        if (entryType instanceof ThriftTypeReference) {
                            possibleFromThriftType((ThriftTypeReference) entryType, possible);
                        }
                    }
                }
            } else if (getParent() instanceof ConfigMapEntry &&
                       getParent().getParent() instanceof ConfigField) {
                ConfigField field = (ConfigField) getParent().getParent();

                for (ThriftFieldType thriftField : field.getPossibleFields()) {
                    // expect map type.
                    if (thriftField.getType() instanceof ThriftMapType) {
                        ThriftType valueType = ((ThriftMapType) thriftField.getType()).getValueType();
                        if (valueType instanceof ThriftTypeReference) {
                            possibleFromThriftType((ThriftTypeReference) valueType, possible);
                        }
                    }
                }
            }

            possibleMessageTypes = ImmutableList.copyOf(possible);
            timestamp = Clock.systemUTC().millis() + 3000;
        }
        return possibleMessageTypes;
    }

    @Override
    public boolean hasPossibleType() {
        return getPossibleMessageTypes().size() > 0;
    }

    @Override
    public List<ThriftFieldType> getPossibleFields() {
        if (possibleFields == null) {
            ArrayList<ThriftFieldType> possible = new ArrayList<>();
            for (ThriftMessage message : getPossibleMessageTypes()) {
                if (message.getMessageFields() != null &&
                    message.getMessageFields().getFields() != null) {
                    possible.addAll(message.getMessageFields().getFields().getMessageFieldList());
                }
            }

            possibleFields = ImmutableList.copyOf(possible);
            timestamp = Clock.systemUTC().millis() + 3000;
        }
        return possibleFields;
    }

    @Override
    public List<ThriftFieldType> getPossibleFieldsForPrefix(String prefix) {
        ArrayList<ThriftFieldType> possible = new ArrayList<>();
        for (ThriftFieldType field : getPossibleFields()) {
            if (field.getFieldName() != null &&
                field.getFieldName().getText().startsWith(prefix)) {
                possible.add(field);
            }
        }
        return possible;
    }

    @Override
    public List<ThriftFieldType> getPossibleFieldsForName(String name) {
        ArrayList<ThriftFieldType> possible = new ArrayList<>();
        for (ThriftFieldType field : getPossibleFields()) {
            if (field.getFieldName() != null &&
                field.getFieldName().getText().equals(name)) {
                possible.add(field);
            }
        }
        return possible;
    }

    @Override
    protected void clearUserData() {
        super.clearUserData();
        if (Clock.systemUTC().millis() > timestamp) {
            possibleFields = null;
            possibleMessageTypes = null;
        }
    }
}
