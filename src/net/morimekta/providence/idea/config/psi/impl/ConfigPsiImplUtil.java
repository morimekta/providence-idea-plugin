/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.psi.impl;

import com.google.common.collect.ImmutableList;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import net.morimekta.providence.idea.common.ProvidenceIdeaUtil;
import net.morimekta.providence.idea.config.psi.ConfigConfigType;
import net.morimekta.providence.idea.config.psi.ConfigField;
import net.morimekta.providence.idea.config.psi.ConfigFieldName;
import net.morimekta.providence.idea.config.psi.ConfigFile;
import net.morimekta.providence.idea.config.psi.ConfigInclude;
import net.morimekta.providence.idea.config.psi.ConfigIncludedFile;
import net.morimekta.providence.idea.config.psi.ConfigMessage;
import net.morimekta.providence.idea.config.psi.ConfigTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;

import java.util.List;

public class ConfigPsiImplUtil {
    public static ConfigFile getIncludedConfigFile(ConfigInclude include) {
        if (include.getIncludedFile() == null) return null;
        String filePath = include.getIncludedFile().getText();
        filePath = filePath.substring(1, filePath.length() - 1);
        PsiFile containing = include.getContainingFile().getOriginalFile();
        PsiFile included = ProvidenceIdeaUtil.resolveFile(containing.getContainingDirectory(), filePath);
        if (included instanceof ConfigFile) {
            return (ConfigFile) included;
        }
        // System.err.println("not a config file: " + included);
        return null;
    }

    public static String getConfigProgramName(ConfigConfigType type) {
        return type.getProgramName().getText();
    }

    public static String getConfigTypeName(ConfigConfigType type) {
        return type.getTypeName().getText();
    }

    public static String getQualifiedTypeName(ConfigConfigType type) {
        return getConfigProgramName(type) + "." + getConfigTypeName(type);
    }

    // ------------------------------

    public static boolean hasPossibleContainedType(ConfigFieldName field) {
        if (field.getParent() instanceof ConfigField &&
            field.getParent().getParent() instanceof ConfigMessage) {
            return ((ConfigMessage) field.getParent().getParent()).hasPossibleType();
        }

        return false;
    }

    public static List<ThriftFieldType> getPossibleFields(ConfigFieldName field) {
        if (field.getParent() instanceof ConfigField &&
            field.getParent().getParent() instanceof ConfigMessage) {
            return ((ConfigMessage) field.getParent()
                                         .getParent()).getPossibleFieldsForName(field.getName());
        }
        return ImmutableList.of();
    }

    // ------------------------------

    public static String getName(ConfigIncludedFile element) {
        return element.getText();
    }

    public static PsiElement setName(ConfigIncludedFile element, String name) {
        return element;
    }

    public static String getName(ConfigTypeName element) {
        return element.getText();
    }

    public static PsiElement setName(ConfigTypeName element, String name) {
        return element;
    }

    public static String getName(ConfigFieldName element) {
        return element.getText();
    }

    public static PsiElement setName(ConfigFieldName element, String name) {
        return element;
    }
}
