/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.reference;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.ResolveResult;
import com.intellij.psi.impl.source.resolve.ResolveCache;
import net.morimekta.providence.idea.common.BaseReference;
import net.morimekta.providence.idea.config.psi.ConfigField;
import net.morimekta.providence.idea.config.psi.ConfigFieldName;
import net.morimekta.providence.idea.config.psi.ConfigFields;
import net.morimekta.providence.idea.config.psi.ConfigMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldName;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.ArrayList;

public class ConfigFieldReference extends BaseReference<ConfigFieldName> {
    private String fieldName;

    public ConfigFieldReference(@Nonnull ConfigFieldName element, ResolveCache cache) {
        super(element, cache);
    }

    @Override
    protected void update() {
        this.fieldName = myElement.getText();
        setRangeInElement(new TextRange(0, fieldName.length()));
    }

    @NotNull
    @Override
    public String getCanonicalText() {
        return fieldName;
    }

    @NotNull
    protected ResolveResult[] multiResolveCached(boolean incomplete) {
        update();

        if (myElement.getParent() instanceof ConfigField &&
            myElement.getParent().getParent() instanceof ConfigFields &&
            myElement.getParent().getParent().getParent() instanceof ConfigMessage) {
            ConfigMessage fields = (ConfigMessage) myElement.getParent().getParent().getParent();

            ArrayList<ResolveResult> result = new ArrayList<>();
            if (incomplete) {
                for (ThriftFieldType field : fields.getPossibleFieldsForPrefix(fieldName)) {
                    if (field.getFieldName() != null) {
                        result.add(new PsiElementResolveResult(field.getFieldName()));
                    }
                }
            } else {
                for (ThriftFieldType field : fields.getPossibleFieldsForName(fieldName)) {
                    if (field.getFieldName() != null) {
                        result.add(new PsiElementResolveResult(field.getFieldName()));
                    }
                }
            }
            return result.toArray(ResolveResult.EMPTY_ARRAY);
        }

        return ResolveResult.EMPTY_ARRAY;
    }

    @Override
    public boolean isReferenceTo(PsiElement element) {
        if (element instanceof ThriftFieldName) {
            update();
            if (element.getText().equals(fieldName)) {
                return super.isReferenceTo(element);
            }
        }
        return false;
    }
}
