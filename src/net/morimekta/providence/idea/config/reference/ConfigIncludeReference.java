/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.reference;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.PsiFile;
import com.intellij.psi.ResolveResult;
import com.intellij.psi.impl.source.resolve.ResolveCache;
import net.morimekta.providence.idea.common.BaseReference;
import net.morimekta.providence.idea.common.ProvidenceIdeaUtil;
import net.morimekta.providence.idea.config.psi.ConfigFile;
import net.morimekta.providence.idea.config.psi.ConfigIncludedFile;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.io.File;

public class ConfigIncludeReference extends BaseReference<ConfigIncludedFile> {
    private String filePath;
    private String fileName;

    public ConfigIncludeReference(@Nonnull ConfigIncludedFile element, ResolveCache cache) {
        super(element, cache);
    }

    @Override
    protected void update() {
        this.filePath = myElement.getText().substring(1, myElement.getTextLength() - 1);
        if (filePath.contains("/")) {
            int last = filePath.lastIndexOf("/");
            this.fileName = filePath.substring(last + 1);
            setRangeInElement(new TextRange(last + 1, fileName.length() + last + 1));
        } else {
            this.fileName = filePath;
            setRangeInElement(new TextRange(1, fileName.length() + 1));
        }
    }

    @NotNull
    @Override
    public String getCanonicalText() {
        return filePath;
    }

    @NotNull
    protected ResolveResult[] multiResolveCached(boolean incomplete) {
        update();

        PsiDirectory dir    = myElement.getContainingFile().getOriginalFile().getContainingDirectory();
        PsiFile      target = ProvidenceIdeaUtil.resolveFile(dir, filePath);
        if (target != null) {
            return new ResolveResult[]{new PsiElementResolveResult(target)};
        }

        return ResolveResult.EMPTY_ARRAY;
    }

    @Override
    public boolean isReferenceTo(PsiElement element) {
        if (element instanceof ConfigFile) {
            update();
            String name = element.getContainingFile().getOriginalFile().getName();
            if (name.contains(File.separator)) {
                int idx = name.lastIndexOf(File.separator);
                name = name.substring(idx + File.separator.length());
            }
            if (fileName.equals(name)) {
                return super.isReferenceTo(element);
            }
        }
        return false;
    }
}
