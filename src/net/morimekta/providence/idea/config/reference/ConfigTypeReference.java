/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.config.reference;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.ResolveResult;
import com.intellij.psi.impl.source.resolve.ResolveCache;
import net.morimekta.providence.idea.common.BaseReference;
import net.morimekta.providence.idea.common.ProvidenceIdeaUtil;
import net.morimekta.providence.idea.config.psi.ConfigConfigType;
import net.morimekta.providence.idea.config.psi.ConfigDefineEnum;
import net.morimekta.providence.idea.thrift.psi.ThriftEnum;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import net.morimekta.providence.idea.thrift.psi.ThriftMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftSpec;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeName;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class ConfigTypeReference extends BaseReference<PsiElement> {
    private String typeName;
    private String programName;
    private boolean isEnum;

    public ConfigTypeReference(@Nonnull PsiElement element, ResolveCache cache) {
        super(element, cache);
    }

    protected void update() {
        this.typeName = myElement.getText();
        if (myElement.getParent() instanceof ConfigConfigType) {
            this.programName = ((ConfigConfigType) myElement.getParent()).getProgramName().getText();
            this.isEnum = false;
        } else if (myElement.getParent() instanceof ConfigDefineEnum) {
            this.programName = ((ConfigDefineEnum) myElement.getParent()).getProgramName().getText();
            this.isEnum = true;
        } else {
            this.programName = "";
        }

        setRangeInElement(new TextRange(0, typeName.length()));
    }

    @NotNull
    @Override
    public String getCanonicalText() {
        return programName + "." + typeName;
    }

    @NotNull
    protected ResolveResult[] multiResolveCached(boolean incomplete) {
        update();

        ArrayList<ResolveResult> result = new ArrayList<>();
        List<ThriftFile> files = ProvidenceIdeaUtil.getThriftFilesForTypePrefix(myElement.getProject(),
                                                                                programName + "." + typeName);
        if (incomplete) {
            for (ThriftFile file : files) {
                for (ThriftSpec spec : file.getSpecList()) {
                    ThriftTypeName nameElement = spec.getTypeNameElement();
                    if (nameElement != null && nameElement.getText().startsWith(typeName)) {
                        if (isEnum) {
                            if (spec instanceof ThriftEnum) {
                                result.add(new PsiElementResolveResult(nameElement));
                            }
                        } else if (spec instanceof ThriftMessage) {
                            result.add(new PsiElementResolveResult(nameElement));
                        }
                    }
                }
            }
        } else {
            for (ThriftFile file : files) {
                if (file.getThriftProgramName().equals(programName)) {
                    for (ThriftSpec spec : file.getSpecList()) {
                        ThriftTypeName nameElement = spec.getTypeNameElement();
                        if (nameElement != null && nameElement.getText().equals(typeName)) {
                            if (isEnum) {
                                if (spec instanceof ThriftEnum) {
                                    result.add(new PsiElementResolveResult(nameElement));
                                }
                            } else if (spec instanceof ThriftMessage) {
                                result.add(new PsiElementResolveResult(nameElement));
                            }
                        }
                    }
                }
            }
        }

        return result.toArray(ResolveResult.EMPTY_ARRAY);
    }

    @Override
    public boolean isReferenceTo(PsiElement element) {
        if (element instanceof ThriftTypeName) {
            update();
            if (element.getText().equals(typeName)) {
                return super.isReferenceTo(element);
            }
        }
        return false;
    }
}
