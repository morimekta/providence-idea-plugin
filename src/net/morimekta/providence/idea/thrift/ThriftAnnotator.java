/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.IElementType;
import net.morimekta.providence.idea.thrift.psi.ThriftConstName;
import net.morimekta.providence.idea.thrift.psi.ThriftEnum;
import net.morimekta.providence.idea.thrift.psi.ThriftEnumValue;
import net.morimekta.providence.idea.thrift.psi.ThriftField;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldName;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;
import net.morimekta.providence.idea.thrift.psi.ThriftFields;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import net.morimekta.providence.idea.thrift.psi.ThriftInclude;
import net.morimekta.providence.idea.thrift.psi.ThriftMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftMessageFields;
import net.morimekta.providence.idea.thrift.psi.ThriftMethodName;
import net.morimekta.providence.idea.thrift.psi.ThriftReferenceTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftReferenceValue;
import net.morimekta.providence.idea.thrift.psi.ThriftService;
import net.morimekta.providence.idea.thrift.psi.ThriftServiceExceptions;
import net.morimekta.providence.idea.thrift.psi.ThriftServiceMethod;
import net.morimekta.providence.idea.thrift.psi.ThriftServiceParams;
import net.morimekta.providence.idea.thrift.psi.ThriftSpec;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeReference;
import net.morimekta.providence.idea.thrift.psi.ThriftTypes;
import net.morimekta.providence.idea.thrift.psi.ThriftValueName;
import net.morimekta.providence.reflect.parser.ThriftConstants;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ThriftAnnotator implements Annotator {
    @Override
    public void annotate(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        annotateSyntaxHighlight(element, holder);

        //  ###########################
        //  ##     KEYWORD NAMES     ##
        //  ###########################

        if (element instanceof ThriftTypeName ||
            element instanceof ThriftFieldName ||
            element instanceof ThriftMethodName) {
            String text = element.getText().trim();
            if (ThriftConstants.kThriftKeywords.contains(text)) {
                holder.createErrorAnnotation(element, "\"" + text + "\" is a thrift keyword");
                return;
            }
            if (ThriftConstants.kProvidenceKeywords.contains(text)) {
                holder.createErrorAnnotation(element, "\"" + text + "\" is a providence keyword");
                return;
            }
        }

        ThriftFile file = (ThriftFile) element.getContainingFile();

        //  #############################
        //  ##       DUPLICATION       ##
        //  #############################

        if (isType(element, ThriftTypes.TYPE_NAME)) {
            String typeName = element.getText();
            for (ThriftSpec spec : file.getSpecList()) {
                ThriftTypeName specName = spec.getTypeNameElement();
                if (specName == null || specName == element) {
                    continue;
                }
                if (typeName.equals(spec.getDeclaredName())) {
                    holder.createErrorAnnotation(element, "Duplicate type name: \"" + typeName + "\"");
                    return;
                }
            }
        } else if (isType(element, ThriftTypes.FIELD_NAME)) {
            PsiElement containedElement = element;
            while (containedElement != null && !(
                    containedElement instanceof ThriftMessageFields ||
                    containedElement instanceof ThriftServiceParams ||
                    containedElement instanceof ThriftServiceExceptions)) {
                containedElement = containedElement.getParent();
            }
            List<ThriftFieldType> fields = null;
            String                what   = null;
            if (containedElement instanceof ThriftMessageFields) {
                ThriftMessageFields message = (ThriftMessageFields) containedElement;
                if (message.getFields() != null)
                    fields = (List) message.getFields().getMessageFieldList();
                what = "field";
            } else if (containedElement instanceof ThriftServiceParams) {
                ThriftServiceParams method = (ThriftServiceParams) containedElement;
                if (method.getParams() != null)
                    fields = (List) method.getParams().getMethodParamList();
                what = "param";
            } else if (containedElement != null) {
                ThriftServiceExceptions exceptions = (ThriftServiceExceptions) containedElement;
                if (exceptions.getParams() != null)
                    fields = (List) exceptions.getParams().getMethodParamList();
                what = "thrown";
            }
            if (fields != null) {
                for (ThriftFieldType field : fields) {
                    if (field.getFieldName() == element) {
                        continue;
                    }
                    if (field.getFieldName().getText().equals(element.getText())) {
                        holder.createErrorAnnotation(element,
                                                     "Duplicate " + what + " name: \"" + element.getText() + "\"");
                        return;
                    }
                }
            }
        } else if (isType(element, ThriftTypes.FIELD_ID)) {
            PsiElement containedElement = element;
            while (containedElement != null && !(
                    containedElement instanceof ThriftMessageFields ||
                    containedElement instanceof ThriftServiceParams ||
                    containedElement instanceof ThriftServiceExceptions)) {
                containedElement = containedElement.getParent();
            }
            List<ThriftFieldType> fields = null;
            String                what   = null;
            boolean isInterface = false;
            if (containedElement instanceof ThriftMessageFields) {
                ThriftMessageFields message = (ThriftMessageFields) containedElement;
                if (message.getFields() != null)
                    fields = (List) message.getFields().getMessageFieldList();
                what = "field";
            } else if (containedElement instanceof ThriftServiceParams) {
                ThriftServiceParams method = (ThriftServiceParams) containedElement;
                if (method.getParams() != null)
                    fields = (List) method.getParams().getMethodParamList();
                what = "param";
            } else if (containedElement != null) {
                ThriftServiceExceptions exceptions = (ThriftServiceExceptions) containedElement;
                if (exceptions.getParams() != null)
                    fields = (List) exceptions.getParams().getMethodParamList();
                what = "thrown";
            }
            if (fields != null) {
                for (ThriftFieldType field : fields) {
                    if (field.getFieldId() == element) {
                        continue;
                    }
                    if (field.getFieldId() != null) {
                        if (field.getFieldId().getText().equals(element.getText())) {
                            holder.createErrorAnnotation(
                                    element, "Duplicate " + what + " ID: \"" + element.getText() + "\"");
                            return;
                        }
                    }
                }
            }
        } else if (isType(element, ThriftTypes.VALUE_NAME)) {
            PsiElement me = element;
            while (me != null && !(me instanceof ThriftEnum)) {
                me = me.getParent();
            }
            if (me != null) {
                ThriftEnum enumType = (ThriftEnum) me;
                if (enumType.getEnumValues() != null) {
                    for (ThriftEnumValue field : enumType.getEnumValues().getEnumValueList()) {
                        if (field.getValueName() == element) {
                            continue;
                        }
                        if (field.getValueName().getText().equals(element.getText())) {
                            holder.createErrorAnnotation(
                                    element, "Duplicate enum value: \"" + element.getText() + "\"");
                            return;
                        }
                    }
                }
            }
        } else if (isType(element, ThriftTypes.VALUE_ID)) {
            PsiElement me = element;
            while (me != null && !(me instanceof ThriftEnum)) {
                me = me.getParent();
            }
            if (me != null) {
                ThriftEnum enumType = (ThriftEnum) me;
                if (enumType.getEnumValues() != null) {
                    for (ThriftEnumValue field : enumType.getEnumValues().getEnumValueList()) {
                        if (field.getValueId() == null || field.getValueId() == element) {
                            continue;
                        }
                        if (field.getValueId().getText().equals(element.getText())) {
                            holder.createErrorAnnotation(
                                    element, "Duplicate enum value ID: \"" + element.getText() + "\"");
                            return;
                        }
                    }
                }
            }
        } else if (isType(element, ThriftTypes.METHOD_NAME)) {
            PsiElement me = element;
            while (me != null && !(me instanceof ThriftService)) {
                me = me.getParent();
            }
            if (me != null) {
                ThriftService service = (ThriftService) me;
                if (service.getServiceMethods() != null) {
                    for (ThriftServiceMethod field : service.getServiceMethods().getServiceMethodList()) {
                        if (field.getMethodName() == element ||
                            field.getMethodName() == null) {
                            continue;
                        }
                        if (field.getMethodName().getText().equals(element.getText())) {
                            holder.createErrorAnnotation(
                                    element, "Duplicate method name: \"" + element.getText() + "\"");
                            return;
                        }
                    }
                }
            }

            //  #############################
            //  ##      UNKNOWN TYPES      ##
            //  #############################

        } else if (isType(element, ThriftTypes.TYPE_REFERENCE)) {
            ThriftTypeReference typeReference = ((ThriftTypeReference) element);
            String typeName = typeReference.getTypeName();
            ThriftFile typeFile = typeReference.getProgramThriftFile();
            if (typeFile == null) {
                if (typeReference.getReferenceProgram() != null) {
                    if (file.getIncludedProgramNames().contains(typeReference.getProgramName())) {
                        holder.createWarningAnnotation(typeReference.getReferenceProgram(),
                                                       "Bad include for \"" + typeReference.getProgramName() + "\"");
                    } else {
                        holder.createErrorAnnotation(typeReference.getReferenceProgram(),
                                                     "No include for \"" + typeReference.getProgramName() + "\"");
                    }
                }
                return;
            }

            if (isType(element.getParent(), ThriftTypes.EXTENDS_TYPE)) {
                 if (!typeFile.getDeclaredServicesWithPrefix(typeName).contains(typeName)) {
                    if (typeReference.getReferenceProgram() != null) {
                        holder.createErrorAnnotation(element, "Unknown service \"" + typeName + "\" in " + typeFile.getThriftProgramName());
                    } else {
                        holder.createErrorAnnotation(element, "Unknown service \"" + typeName + "\"");
                    }
                    return;
                }
            } else {
                if (!typeFile.getDeclaredTypesWithPrefix(typeName).contains(typeName)) {
                    if (typeReference.getReferenceProgram() != null) {
                        holder.createErrorAnnotation(element,
                                                     "Unknown type \"" + typeName + "\" in " +
                                                     typeFile.getThriftProgramName());
                    } else {
                        holder.createErrorAnnotation(element, "Unknown type \"" + typeName + "\"");
                    }
                    return;
                }
            }
            //  ###############################
            //  ##      UNKNOWN INCLUDE      ##
            //  ###############################

        } else if (isType(element, ThriftTypes.FILE_PATH)) {
            if (element.getParent() instanceof ThriftInclude) {
                if (element.getReference() != null &&
                    element.getReference().resolve() == null) {
                    holder.createErrorAnnotation(element, "No such thrift file to include " + element.getText());
                }
            }

            //  ################################
            //  ##      MISSING FIELD ID      ##
            //  ################################

        } else if (isType(element, ThriftTypes.MESSAGE_FIELD)) {
            ThriftField field = (ThriftField) element;
            ThriftMessage message = messageContaining(field);
            if (message != null) {
                if (!"interface".equalsIgnoreCase(message.getMessageVariant().getText()) &&
                    field.getFieldId() == null) {
                    holder.createWarningAnnotation(element, "No ID on field " + field.getFieldName().getName());
                }
            }
        } else if (isType(element, ThriftTypes.METHOD_PARAM)) {
            ThriftField field = (ThriftField) element;
            if (field.getFieldId() == null) {
                holder.createWarningAnnotation(element, "No ID on field " + field.getFieldName().getName());
            }
        }

        //  ############################
        //  ##     RESERVED WORDS     ##
        //  ############################

        if (element instanceof ThriftTypeName ||
            element instanceof ThriftFieldName ||
            element instanceof ThriftMethodName ||
            element instanceof ThriftValueName) {
            String text = element.getText().trim();
            if (ThriftConstants.kReservedWords.contains(text)) {
                holder.createWarningAnnotation(element, "\"" + text + "\" is a reserved word");
            }
        }
    }

    private ThriftMessage messageContaining(ThriftField field) {
        PsiElement element = field;
        while (element instanceof ThriftField ||
               element instanceof ThriftFields ||
               element instanceof ThriftMessageFields) {
            element = element.getParent();
        }
        if (element instanceof ThriftMessage) {
            return (ThriftMessage) element;
        }
        return null;
    }

    private void annotateSyntaxHighlight(@NotNull PsiElement element,
                                         @NotNull AnnotationHolder holder) {
        if (element instanceof ThriftReferenceValue ||
            element instanceof ThriftValueName ||
            element instanceof ThriftConstName) {
            holder.createInfoAnnotation(element, null)
                  .setTextAttributes(ThriftSyntaxHighlighter.VALUE_NAME);
        } else if (element instanceof ThriftTypeName ||
                   element instanceof ThriftReferenceTypeName) {
            holder.createInfoAnnotation(element, null)
                  .setTextAttributes(ThriftSyntaxHighlighter.TYPE_NAME);
        } else if (element instanceof ThriftFieldName) {
            holder.createInfoAnnotation(element, null)
                  .setTextAttributes(ThriftSyntaxHighlighter.FIELD_NAME);
        } else if (element instanceof ThriftMethodName) {
            holder.createInfoAnnotation(element, null)
                  .setTextAttributes(ThriftSyntaxHighlighter.METHOD_NAME);
        }
    }

    private boolean isType(PsiElement element, IElementType... types) {
        for (IElementType type : types) {
            if (element.getNode().getElementType().equals(type)) {
                return true;
            }
        }
        return false;
    }
}
