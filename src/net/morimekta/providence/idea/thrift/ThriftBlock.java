/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.formatting.Alignment;
import com.intellij.formatting.Block;
import com.intellij.formatting.Indent;
import com.intellij.formatting.Spacing;
import com.intellij.formatting.SpacingBuilder;
import com.intellij.formatting.Wrap;
import com.intellij.formatting.WrapType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.TokenType;
import com.intellij.psi.formatter.common.AbstractBlock;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.ANNOTATIONS;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.CONST;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.CONST_LIST;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.CONST_LIST_ENTRY;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.CONST_MAP;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.CONST_MAP_ENTRY;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.ENUM;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.ENUM_VALUES;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.FIELD;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.INCLUDE;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.LIST_END;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.LIST_START;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.MAP_END;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.MAP_START;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.MESSAGE;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.MESSAGE_FIELDS;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.NAMESPACE;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.PARAM_END;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.PARAM_START;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.SERVICE;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.SERVICE_EXCEPTIONS;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.SERVICE_METHOD;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.SERVICE_METHODS;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.SERVICE_PARAMS;

public class ThriftBlock extends AbstractBlock {
    private final SpacingBuilder spacingBuilder;

    protected ThriftBlock(@NotNull ASTNode node,
                          @Nullable Wrap wrap,
                          @Nullable Alignment alignment,
                          SpacingBuilder spacingBuilder) {
        super(node, wrap, alignment);
        this.spacingBuilder = spacingBuilder;
    }

    @Override
    protected List<Block> buildChildren() {
        if (isLeaf()) {
            return EMPTY;
        }

        List<Block> blocks = new ArrayList<>();
        ASTNode     child  = myNode.getFirstChildNode();
        if (typeOf(ENUM_VALUES,
                   MESSAGE_FIELDS,
                   SERVICE_PARAMS,
                   SERVICE_EXCEPTIONS,
                   SERVICE_METHODS,
                   ANNOTATIONS,
                   CONST_MAP,
                   CONST_LIST)) {
            while (child != null) {
                if (child.getElementType() != TokenType.WHITE_SPACE) {
                    if (typeOf(child,
                               MAP_START, MAP_END, LIST_START, LIST_END, PARAM_START, PARAM_END)) {
                        Block block = new ThriftBlock(child,
                                                      Wrap.createWrap(WrapType.NORMAL, false),
                                                      myAlignment,
                                                      spacingBuilder);
                        blocks.add(block);
                    } else {
                        Block block = new ThriftBlock(child,
                                                      Wrap.createWrap(WrapType.NORMAL, false),
                                                      Alignment.createAlignment(),
                                                      spacingBuilder);
                        blocks.add(block);
                    }
                }
                child = child.getTreeNext();
            }
        } else if (typeOf(NAMESPACE,
                          INCLUDE,
                          ENUM,
                          MESSAGE,
                          FIELD,
                          SERVICE,
                          SERVICE_METHOD,
                          CONST,
                          CONST_LIST_ENTRY,
                          CONST_MAP_ENTRY)) {
            while (child != null) {
                if (child.getElementType() != TokenType.WHITE_SPACE) {
                    Block block = new ThriftBlock(child,
                                                  Wrap.createWrap(WrapType.NORMAL, false),
                                                  myAlignment,
                                                  spacingBuilder);
                    blocks.add(block);
                }
                child = child.getTreeNext();
            }
        } else {
            while (child != null) {
                if (child.getElementType() != TokenType.WHITE_SPACE) {
                    Block block = new ThriftBlock(child,
                                                  Wrap.createWrap(WrapType.NORMAL, false),
                                                  Alignment.createChildAlignment(myAlignment),
                                                  spacingBuilder);
                    blocks.add(block);
                }
                child = child.getTreeNext();
            }
        }

        return blocks;
    }

    @Override
    public Indent getIndent() {
        if (typeOf(myNode.getTreeParent(),
                   ENUM_VALUES,
                   MESSAGE_FIELDS,
                   SERVICE_PARAMS,
                   SERVICE_EXCEPTIONS,
                   SERVICE_METHODS,
                   ANNOTATIONS,
                   CONST_MAP,
                   CONST_LIST) &&
            !typeOf(MAP_START, MAP_END, LIST_START, LIST_END, PARAM_START, PARAM_END)) {
            return Indent.getSpaceIndent(4);
        }
        return Indent.getNoneIndent();
    }

    @Nullable
    @Override
    public Spacing getSpacing(@Nullable Block firstChild, @NotNull Block secondChild) {
        return spacingBuilder.getSpacing(this, firstChild, secondChild);
    }

    @Override
    public boolean isLeaf() {
        return myNode.getFirstChildNode() == null;
    }

    private boolean typeOf(IElementType... types) {
        return typeOf(myNode, types);
    }

    private boolean typeOf(ASTNode node, IElementType... types) {
        if (node == null) return false;
        for (IElementType type : types) {
            if (node.getElementType().equals(type)) return true;
        }
        return false;
    }
}
