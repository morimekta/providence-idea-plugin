/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class ThriftColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Comment", ThriftSyntaxHighlighter.LINE_COMMENT),
            new AttributesDescriptor("Documentation", ThriftSyntaxHighlighter.DOC_COMMENT),
            new AttributesDescriptor("Braces", ThriftSyntaxHighlighter.BRACE),
            new AttributesDescriptor("Separators", ThriftSyntaxHighlighter.SEPARATOR),
            new AttributesDescriptor("Keyword", ThriftSyntaxHighlighter.KEYWORD),
            new AttributesDescriptor("Number", ThriftSyntaxHighlighter.NUMBER),
            new AttributesDescriptor("String", ThriftSyntaxHighlighter.STRING),
            new AttributesDescriptor("Type Name", ThriftSyntaxHighlighter.TYPE_NAME),
            new AttributesDescriptor("Value Name", ThriftSyntaxHighlighter.VALUE_NAME),
            new AttributesDescriptor("Field Name", ThriftSyntaxHighlighter.FIELD_NAME),
            new AttributesDescriptor("Method Name", ThriftSyntaxHighlighter.METHOD_NAME),
            new AttributesDescriptor("Bad Character", ThriftSyntaxHighlighter.BAD_CHAR),
            };

    @Nullable
    @Override
    public Icon getIcon() {
        return ThriftIcon.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new ThriftSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "/**\n" +
               " * Documentation\n" +
               " */" +
               "namespace java net.morimekta.providence.test\n" +
               "include \"included.thrift\"\n" +
               "\n" +
               "enum EnumName {\n" +
               "  VALUE = 1\n" +
               "  OTHER = 5\n" +
               "}\n" +
               "\n" +
               "struct Test {\n" +
               "" +
               "} (annotation = \"value\");\n" +
               "" +
               "service MyService extends OtherService {\n" +
               "  oneway void testVoid();\n" +
               "  list<i32> content(1: string paramName) throws (\n" +
               "    1: MyException ex1" +
               "  )\n" +
               "} (java.service.methods.extends = \"SomeException\")\n";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Thrift";
    }
}
