/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionType;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.tree.IElementType;
import net.morimekta.providence.idea.thrift.completion.ThriftIncludeCompletionProvider;
import net.morimekta.providence.idea.thrift.completion.ThriftTypeCompletionProvider;
import net.morimekta.providence.idea.thrift.psi.ThriftTypes;

public class ThriftCompletionContributor extends CompletionContributor {
    public ThriftCompletionContributor() {
        extendAll(ThriftIncludeCompletionProvider.INSTANCE,
                  ThriftTypes.LITERAL);
        extendAll(ThriftTypeCompletionProvider.INSTANCE,
                  ThriftTypes.IDENTIFIER,
                  ThriftTypes.DOT);
    }

    private void extendAll(CompletionProvider<CompletionParameters> provider,
                           IElementType... types) {
        for (IElementType type : types) {
            extend(CompletionType.BASIC,
                   PlatformPatterns.psiElement(type).withLanguage(ThriftLanguage.INSTANCE),
                   provider);
        }
    }
}
