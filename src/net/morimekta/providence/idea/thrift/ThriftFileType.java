/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ThriftFileType extends LanguageFileType {
    public static ThriftFileType INSTANCE = new ThriftFileType();

    private ThriftFileType() {
        super(ThriftLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "providence";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Providence Type Definition File";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "providence";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return ThriftIcon.FILE;
    }

    @Override
    public Charset extractCharsetFromFileContent(@Nullable Project project,
                                                 @Nullable VirtualFile file,
                                                 @NotNull CharSequence content) {
        return StandardCharsets.UTF_8;
    }
}
