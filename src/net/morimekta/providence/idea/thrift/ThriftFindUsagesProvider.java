/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.lang.cacheBuilder.DefaultWordsScanner;
import com.intellij.lang.cacheBuilder.WordsScanner;
import com.intellij.lang.findUsages.FindUsagesProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.TokenSet;
import net.morimekta.providence.idea.thrift.psi.ThriftEnum;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldName;
import net.morimekta.providence.idea.thrift.psi.ThriftListType;
import net.morimekta.providence.idea.thrift.psi.ThriftMapType;
import net.morimekta.providence.idea.thrift.psi.ThriftMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftService;
import net.morimekta.providence.idea.thrift.psi.ThriftType;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftTypedef;
import net.morimekta.providence.idea.thrift.psi.ThriftTypes;
import net.morimekta.providence.idea.thrift.psi.ThriftValueName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ThriftFindUsagesProvider implements FindUsagesProvider {
    @Nullable
    @Override
    public WordsScanner getWordsScanner() {
        return new DefaultWordsScanner(new ThriftLexerAdapter(),
                                       TokenSet.create(ThriftTypes.TYPE_NAME,
                                                       ThriftTypes.VALUE_NAME,
                                                       ThriftTypes.FIELD_NAME),
                                       TokenSet.create(ThriftTypes.DOC_COMMENT,
                                                       ThriftTypes.LINE_COMMENT),
                                       TokenSet.EMPTY);
    }

    @Override
    public boolean canFindUsagesFor(@NotNull PsiElement psiElement) {
        return psiElement instanceof ThriftTypeName ||
               psiElement instanceof ThriftValueName ||
               psiElement instanceof ThriftFieldName;
    }

    @Nullable
    @Override
    public String getHelpId(@NotNull PsiElement psiElement) {
        return null;
    }

    @NotNull
    @Override
    public String getType(@NotNull PsiElement psiElement) {
        if (psiElement instanceof ThriftTypeName) {
            if (psiElement.getParent() instanceof ThriftService) {
                return "Service";
            } else if (psiElement.getParent() instanceof ThriftMessage) {
                return "Message";
            } else if (psiElement.getParent() instanceof ThriftTypedef) {
                return "Typedef";
            } else if (psiElement.getParent() instanceof ThriftEnum) {
                return "Enum";
            }
        } else if (psiElement instanceof ThriftValueName) {
            return "Enum Value";
        } else if (psiElement instanceof ThriftFieldName) {
            if (psiElement.getParent() instanceof ThriftFieldType) {
                return fieldTypeString(((ThriftFieldType) psiElement.getParent()));
            }
            return "Field";
        }
        return "";
    }

    private String fieldTypeString(ThriftFieldType field) {
        return "<b>" +
               typeString(field.getType()) +
               "</b> field";
    }

    private String typeString(ThriftType type) {
        if (type instanceof ThriftListType) {
            ThriftListType list = (ThriftListType) type;
            return list.getText() + "&lt;" + typeString(list.getType()) + "&gt;";
        } else if (type instanceof ThriftMapType) {
            ThriftMapType map = (ThriftMapType) type;
            return "map&lt;" + typeString(map.getKeyType()) + "," + typeString(map.getValueType()) + "&gt;";
        } else if (type != null) {
            return type.getText();
        }
        return "…";
    }

    @NotNull
    @Override
    public String getDescriptiveName(@NotNull PsiElement psiElement) {
        return psiElement.getText();
    }

    @NotNull
    @Override
    public String getNodeText(@NotNull PsiElement psiElement, boolean b) {
        return psiElement.getText();
    }
}
