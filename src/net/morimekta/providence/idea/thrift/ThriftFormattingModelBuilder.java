/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.formatting.Alignment;
import com.intellij.formatting.FormattingModel;
import com.intellij.formatting.FormattingModelBuilder;
import com.intellij.formatting.FormattingModelProvider;
import com.intellij.formatting.SpacingBuilder;
import com.intellij.formatting.Wrap;
import com.intellij.formatting.WrapType;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.COLON;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.COMMA;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.DOC_COMMENT;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.ENUM_VALUE;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.EQUALS;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.FIELD;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.FIELD_ID;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.FIELD_NAME;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.IDENTIFIER;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.INCLUDE;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_ENUM;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_EXCEPTION;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_EXTENDS;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_OPTIONAL;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_REQUIRED;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_SERVICE;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_STRUCT;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_THROWS;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.KW_UNION;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.LINE_COMMENT;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.LIST_START;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.MAP_END;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.MAP_START;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.METHOD_NAME;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.NAMESPACE;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.SEMICOLON;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.SERVICE_METHOD;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.SPEC;
import static net.morimekta.providence.idea.thrift.psi.ThriftTypes.TYPE;

public class ThriftFormattingModelBuilder implements FormattingModelBuilder {
    @NotNull
    @Override
    public FormattingModel createModel(PsiElement psiElement, CodeStyleSettings codeStyleSettings) {
        return FormattingModelProvider.createFormattingModelForPsiFile(
                psiElement.getContainingFile(),
                new ThriftBlock(psiElement.getNode(),
                                Wrap.createWrap(WrapType.CHOP_DOWN_IF_LONG, false),
                                Alignment.createAlignment(),
                                createSpaceBuilder(codeStyleSettings)),
                codeStyleSettings);
    }

    private static SpacingBuilder createSpaceBuilder(CodeStyleSettings settings) {
        return new SpacingBuilder(settings, ThriftLanguage.INSTANCE)
                .around(TokenSet.create(EQUALS, KW_OPTIONAL, KW_REQUIRED, KW_THROWS, KW_EXTENDS))
                .spaces(1)
                .before(TokenSet.create(MAP_START, LIST_START, FIELD_ID, FIELD_NAME, METHOD_NAME))
                .spaces(1)
                .before(TokenSet.create(COLON, COMMA, SEMICOLON))
                .none()
                .after(TokenSet.create(COLON, COMMA))
                .spaces(1)

                // Entry Spacing.
                .between(TokenSet.create(KW_ENUM, KW_STRUCT, KW_UNION, KW_EXCEPTION, KW_SERVICE), IDENTIFIER)
                .spaces(1)
                .between(TYPE, FIELD_NAME)
                .spaces(1)
                .between(FIELD, FIELD)
                .spaces(1)
                .between(ENUM_VALUE, ENUM_VALUE)
                .spaces(1)

                // Line spacing.
                .before(TokenSet.create(MAP_END))
                .lineBreakInCode()
                .after(TokenSet.create(INCLUDE, NAMESPACE, SERVICE_METHOD, ENUM_VALUE))
                .lineBreakInCode()
                .between(INCLUDE, TokenSet.create(NAMESPACE, SPEC))
                .blankLines(1)
                .between(NAMESPACE, TokenSet.create(INCLUDE, SPEC))
                .blankLines(1)
                .between(SPEC, TokenSet.create(SPEC, LINE_COMMENT, DOC_COMMENT))
                .blankLines(1);
    }


    @Nullable
    @Override
    public TextRange getRangeAffectingIndent(PsiFile psiFile, int i, ASTNode astNode) {
        return null;
    }
}
