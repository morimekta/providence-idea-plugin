/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiJavaCodeReferenceElement;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiReferenceList;
import net.morimekta.providence.PEnumValue;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.idea.common.ProvidenceIdeaUtil;
import net.morimekta.providence.idea.thrift.psi.ThriftEnum;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import net.morimekta.providence.idea.thrift.psi.ThriftMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftService;
import net.morimekta.providence.idea.thrift.psi.ThriftSpec;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeName;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static net.morimekta.util.Strings.camelCase;

public class ThriftImplementationLineMarkerProvider extends RelatedItemLineMarkerProvider {
    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element,
                                            @NotNull Collection<? super RelatedItemLineMarkerInfo> result) {
        if (element instanceof PsiJavaCodeReferenceElement) {
            if (element.getParent() instanceof PsiReferenceList) {
                if (element.getParent().getParent() instanceof PsiClass) {
                    if (element.getText().equals("Iface") ||
                        element.getText().endsWith(".Iface")) {

                        PsiClass classElement = (PsiClass) element.getParent().getParent();

                        for (PsiClass impl : classElement.getInterfaces()) {
                            String qname = impl.getQualifiedName();
                            if (qname != null && qname.endsWith(".Iface")) {
                                int li = qname.substring(0, qname.length() - 6).lastIndexOf(".");
                                if (li < 0) continue;

                                String qpa     = qname.substring(0, li);
                                String service = qname.substring(li + 1, qname.length() - 6);

                                PsiJavaFile javaFile = (PsiJavaFile) classElement.getContainingFile();
                                if (javaFile.getPackageName().equals(qpa)) {
                                    // Same file, this is the Client and Processor for the service.
                                    return;
                                }

                                List<ThriftTypeName> elementList = new ArrayList<>();
                                for (ThriftFile f : ProvidenceIdeaUtil.getThriftFilesForJavaPackage(element.getProject(),
                                                                                                    qpa)) {
                                    for (ThriftSpec spec : f.getSpecList()) {
                                        if (spec instanceof ThriftService &&
                                            service.equals(spec.getDeclaredName())) {
                                            elementList.add(((ThriftService) spec).getTypeName());

                                        }
                                    }
                                }
                                if (elementList.size() > 0) {
                                    NavigationGutterIconBuilder<PsiElement> builder =
                                            NavigationGutterIconBuilder.create(ThriftIcon.FILE)
                                                                       .setTargets(elementList)
                                                                       .setTooltipText("Providence Service");
                                    result.add(builder.createLineMarkerInfo(element));
                                }
                            }
                        }
                    } else if (element.getText().startsWith(PMessage.class.getName())) {
                        PsiClass classElement = (PsiClass) element.getParent().getParent();
                        PsiJavaFile javaFile = (PsiJavaFile) classElement.getContainingFile();

                        String namespace = javaFile.getPackageName();
                        String className = classElement.getName();
                        if (className != null && !className.contains("_")) {  // not request and responses.

                            List<ThriftTypeName> resultList = new ArrayList<>();
                            for (ThriftFile f : ProvidenceIdeaUtil.getThriftFilesForJavaPackage(element.getProject(),
                                                                                                namespace)) {
                                for (ThriftSpec spec : f.getSpecList()) {
                                    if (spec instanceof ThriftMessage &&
                                        className.equals(camelCase(spec.getDeclaredName()))) {
                                        resultList.add(spec.getTypeNameElement());
                                    }
                                }
                            }
                            if (resultList.size() > 0) {
                                NavigationGutterIconBuilder<PsiElement> builder =
                                        NavigationGutterIconBuilder.create(ThriftIcon.FILE)
                                                                   .setTargets(resultList)
                                                                   .setTooltipText("Providence Message");
                                result.add(builder.createLineMarkerInfo(element));
                            }
                        }
                    } else if (element.getText().startsWith(PEnumValue.class.getName())) {
                        PsiClass classElement = (PsiClass) element.getParent().getParent();
                        PsiJavaFile javaFile = (PsiJavaFile) classElement.getContainingFile();

                        String namespace = javaFile.getPackageName();
                        String className = classElement.getName();
                        if (className != null && !className.contains("_")) {  // not request and responses.

                            List<ThriftTypeName> resultList = new ArrayList<>();
                            for (ThriftFile f : ProvidenceIdeaUtil.getThriftFilesForJavaPackage(element.getProject(),
                                                                                                namespace)) {
                                for (ThriftSpec spec : f.getSpecList()) {
                                    if (spec instanceof ThriftEnum &&
                                        className.equals(camelCase(spec.getDeclaredName()))) {
                                        resultList.add(spec.getTypeNameElement());
                                    }
                                }
                            }
                            if (resultList.size() > 0) {
                                NavigationGutterIconBuilder<PsiElement> builder =
                                        NavigationGutterIconBuilder.create(ThriftIcon.FILE)
                                                                   .setTargets(resultList)
                                                                   .setTooltipText("Providence Enum");
                                result.add(builder.createLineMarkerInfo(element));
                            }
                        }
                    }
                }
            }
        }
    }
}
