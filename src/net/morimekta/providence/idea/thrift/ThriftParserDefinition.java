/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import net.morimekta.providence.idea.thrift.psi.ThriftTypes;
import org.jetbrains.annotations.NotNull;

public class ThriftParserDefinition implements ParserDefinition {
    public static final TokenSet STRING_LITERALS = TokenSet.create(ThriftTypes.LITERAL);
    /** Ignore line comments */
    public static final TokenSet COMMENT = TokenSet.create(ThriftTypes.LINE_COMMENT, ThriftTypes.DOC_COMMENT);

    public static final IFileElementType FILE = new IFileElementType(ThriftLanguage.INSTANCE);


    @NotNull
    @Override
    public Lexer createLexer(Project project) {
        return new ThriftLexerAdapter();
    }

    @Override
    public PsiParser createParser(Project project) {
        return new ThriftParser();
    }

    @Override
    public IFileElementType getFileNodeType() {
        return FILE;
    }

    @NotNull
    @Override
    public TokenSet getCommentTokens() {
        return COMMENT;
    }

    @NotNull
    @Override
    public TokenSet getStringLiteralElements() {
        return STRING_LITERALS;
    }

    @NotNull
    @Override
    public PsiElement createElement(ASTNode astNode) {
        return ThriftTypes.Factory.createElement(astNode);
    }

    @Override
    public PsiFile createFile(FileViewProvider fileViewProvider) {
        return new ThriftFile(fileViewProvider);
    }

    @Override
    public SpaceRequirements spaceExistanceTypeBetweenTokens(ASTNode left, ASTNode right) {
        if (isSymbol(left) || isSymbol(right)) {
            return SpaceRequirements.MAY;
        }
        if ((isDot(left) && right.getElementType().equals(ThriftTypes.IDENTIFIER)) ||
            (isDot(right) && left.getElementType().equals(ThriftTypes.IDENTIFIER))) {
            return SpaceRequirements.MUST_NOT;
        }
        return SpaceRequirements.MUST;
    }

    private static boolean isDot(ASTNode node) {
        return node.getText().equals(".");
    }

    private static boolean isSymbol(ASTNode node) {
        return node.getText().matches("[{}\\[\\]()<>;:,=]");
    }
}
