/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import net.morimekta.providence.idea.thrift.psi.ThriftTokenType;
import net.morimekta.providence.idea.thrift.psi.ThriftTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class ThriftSyntaxHighlighter extends SyntaxHighlighterBase {
    public static final TextAttributesKey LINE_COMMENT = createTextAttributesKey(
            "THRIFT_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
    public static final TextAttributesKey DOC_COMMENT  = createTextAttributesKey(
            "THRIFT_DOCUMENTATION", DefaultLanguageHighlighterColors.DOC_COMMENT);
    public static final TextAttributesKey BRACE        = createTextAttributesKey(
            "THRIFT_BRACE", DefaultLanguageHighlighterColors.BRACES);
    public static final TextAttributesKey SEPARATOR    = createTextAttributesKey(
            "THRIFT_SEPARATOR", DefaultLanguageHighlighterColors.COMMA);

    public static final TextAttributesKey KEYWORD    = createTextAttributesKey(
            "THRIFT_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey NUMBER     = createTextAttributesKey(
            "THRIFT_NUMBER", DefaultLanguageHighlighterColors.NUMBER);
    public static final TextAttributesKey STRING     = createTextAttributesKey(
            "THRIFT_STRING", DefaultLanguageHighlighterColors.STRING);

    public static final TextAttributesKey TYPE_NAME     = createTextAttributesKey(
            "THRIFT_TYPE_NAME", DefaultLanguageHighlighterColors.CLASS_NAME);
    public static final TextAttributesKey VALUE_NAME     = createTextAttributesKey(
            "THRIFT_VALUE_NAME", DefaultLanguageHighlighterColors.STATIC_FIELD);
    public static final TextAttributesKey FIELD_NAME     = createTextAttributesKey(
            "THRIFT_FIELD_NAME", DefaultLanguageHighlighterColors.INSTANCE_FIELD);
    public static final TextAttributesKey METHOD_NAME     = createTextAttributesKey(
            "THRIFT_METHOD_NAME", DefaultLanguageHighlighterColors.INSTANCE_METHOD);

    public static final TextAttributesKey BAD_CHAR = createTextAttributesKey(
            "THRIFT_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);


    private static final TextAttributesKey[] LINE_COMMENTS = new TextAttributesKey[]{LINE_COMMENT};
    private static final TextAttributesKey[] DOCUMENTATION = new TextAttributesKey[]{DOC_COMMENT};
    private static final TextAttributesKey[] BRACES        = new TextAttributesKey[]{BRACE};
    private static final TextAttributesKey[] SEPARATORS    = new TextAttributesKey[]{SEPARATOR};

    private static final TextAttributesKey[] KEYWORDS = new TextAttributesKey[]{KEYWORD};
    private static final TextAttributesKey[] NUMBERS  = new TextAttributesKey[]{NUMBER};
    private static final TextAttributesKey[] STRINGS  = new TextAttributesKey[]{STRING};

    private static final TextAttributesKey[] BAD_CHARACTERS = new TextAttributesKey[]{BAD_CHAR};

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new ThriftLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType type) {
        if (type == null) {
            return EMPTY;
        } else if (type.equals(TokenType.BAD_CHARACTER)) {
            return BAD_CHARACTERS;
        } else if (type.equals(ThriftTypes.LINE_COMMENT)) {
            return LINE_COMMENTS;
        } else if (type.equals(ThriftTypes.DOC_COMMENT)) {
            return DOCUMENTATION;
        } else if (type.equals(ThriftTypes.ZERO) ||
                   type.equals(ThriftTypes.ID) ||
                   type.equals(ThriftTypes.NUMBER)) {
            return NUMBERS;
        } else if (type.equals(ThriftTypes.LITERAL)) {
            return STRINGS;
        } else if (type.equals(ThriftTypes.COMMA) ||
                   type.equals(ThriftTypes.SEMICOLON) ||
                   type.equals(ThriftTypes.COLON) ||
                   type.equals(ThriftTypes.EQUALS)) {
            return SEPARATORS;
        } else if (type.equals(ThriftTypes.MAP_START) ||
                   type.equals(ThriftTypes.MAP_END) ||
                   type.equals(ThriftTypes.PARAM_START) ||
                   type.equals(ThriftTypes.PARAM_END) ||
                   type.equals(ThriftTypes.LIST_START) ||
                   type.equals(ThriftTypes.LIST_END) ||
                   type.equals(ThriftTypes.GENERIC_START) ||
                   type.equals(ThriftTypes.GENERIC_END)) {
            return BRACES;
        } else if (type instanceof ThriftTokenType &&
                   ((ThriftTokenType) type).isKeyword()) {
            return KEYWORDS;
        }

        return EMPTY;
    }
}
