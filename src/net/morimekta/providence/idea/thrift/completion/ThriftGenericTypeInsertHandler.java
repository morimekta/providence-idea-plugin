package net.morimekta.providence.idea.thrift.completion;

import com.intellij.codeInsight.completion.InsertHandler;
import com.intellij.codeInsight.completion.InsertionContext;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.openapi.editor.Document;

import javax.annotation.Nonnull;

public class ThriftGenericTypeInsertHandler implements InsertHandler<LookupElement> {
    static final ThriftGenericTypeInsertHandler INSERT_MAP_GENERIC = new ThriftGenericTypeInsertHandler("<", ",>");
    static final ThriftGenericTypeInsertHandler INSERT_LIST_GENERIC = new ThriftGenericTypeInsertHandler("<", ">");

    private final String before;
    private final String after;

    private ThriftGenericTypeInsertHandler(String before, String after) {
        this.before = before;
        this.after = after;
    }

    @Override
    public void handleInsert(@Nonnull InsertionContext context, @Nonnull LookupElement lookupElement) {
          int      caret    = context.getTailOffset();
          Document document = context.getDocument();

          document.insertString(context.getTailOffset(), before + after);
          context.getEditor().getCaretModel().moveToOffset(caret + before.length());
    }
}
