/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift.completion;

import com.google.common.collect.ImmutableList;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.util.ProcessingContext;
import net.morimekta.providence.idea.common.BaseCompletionProvider;
import net.morimekta.providence.idea.thrift.psi.ThriftFilePath;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ThriftIncludeCompletionProvider extends BaseCompletionProvider {
    public static final ThriftIncludeCompletionProvider INSTANCE = new ThriftIncludeCompletionProvider();

    @Override
    protected void addCompletions(@NotNull CompletionParameters parameters,
                                  ProcessingContext context,
                                  @NotNull CompletionResultSet resultSet) {
        try {
            if (parameters.getPosition().getParent() != null &&
                parameters.getPosition().getParent() instanceof ThriftFilePath) {

                ThriftFilePath filePath    = (ThriftFilePath) parameters.getPosition().getParent();
                String         include     = getCompletePrefixString(filePath.getIncludedFilePath());
                String         stripPrefix = getStripPrefix(resultSet, include);

                PsiDirectory dir = parameters.getOriginalFile().getContainingDirectory();
                if (dir != null) {
                    List<String> options = resolvePath(dir, include, "");
                    for (String option : options) {
                        if (stripPrefix.length() > 0 && option.startsWith(stripPrefix)) {
                            option = option.substring(stripPrefix.length());
                        } else if (include.length() > 0) {
                            option = option.substring(include.length());
                        }

                        if (option.length() > 0) {
                            resultSet.addElement(LookupElementBuilder.create(option));
                        }
                    }
                }

                resultSet.stopHere();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final String DOT                 = ".";
    private static final String DOT_DOT             = "..";
    private static final String DOT_DOT_SLASH       = "../";
    private static final String SLASH               = "/";

    private List<String> resolvePath(@NonNls PsiDirectory dir,
                                     @NonNls String include,
                                     @NonNls String prefix) {
        if (include.isEmpty()) {
            // list files in parent directory.
            List<String> options = new ArrayList<>();
            for (PsiFile file : dir.getFiles()) {
                options.add(create(prefix, file.getName()));
            }
            for (PsiDirectory sub : dir.getSubdirectories()) {
                options.add(create(prefix, sub.getName() + SLASH));
            }
            if (dir.getParent() != null) {
                options.add(DOT_DOT_SLASH);
            }
            return options;
        } else if (include.equals(DOT) || include.equals(DOT_DOT)) {
            return ImmutableList.of(DOT_DOT_SLASH);
        } else if (include.startsWith(DOT_DOT_SLASH)) {
            PsiDirectory parent = dir.getParent();
            if (parent != null) {
                return resolvePath(parent, include.substring(DOT_DOT_SLASH.length()), prefix + DOT_DOT_SLASH);
            }
        } else if (include.contains(SLASH)) {
            int sep = include.indexOf(SLASH);
            String name = include.substring(0, sep);
            String resolve = include.substring(sep + SLASH.length());
            PsiDirectory subdirectory = dir.findSubdirectory(name);
            if (subdirectory != null) {
                return resolvePath(subdirectory, resolve, prefix + name + SLASH);
            }
        } else {
            List<String> options = new ArrayList<>();
            for (PsiFile file : dir.getFiles()) {
                String name = file.getName();
                if (name.startsWith(include)) {
                    options.add(create(prefix, name));
                }
            }
            for (PsiDirectory subDir : dir.getSubdirectories()) {
                String name = subDir.getName();
                if (name.startsWith(include)) {
                    options.add(create(prefix, name + SLASH));
                }
            }
            return options;
        }
        return ImmutableList.of();
    }

    private String create(String prefix, String name) {
        if (prefix.isEmpty()) {
            return name;
        } else if (prefix.endsWith(SLASH)) {
            return prefix + name;
        } else {
            return prefix + SLASH + name;
        }
    }
}
