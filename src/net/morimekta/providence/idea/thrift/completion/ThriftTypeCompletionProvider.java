/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiErrorElement;
import com.intellij.psi.PsiWhiteSpace;
import com.intellij.psi.tree.IElementType;
import com.intellij.util.PlatformIcons;
import com.intellij.util.ProcessingContext;
import net.morimekta.providence.descriptor.PPrimitive;
import net.morimekta.providence.idea.common.BaseCompletionProvider;
import net.morimekta.providence.idea.thrift.psi.ThriftEnum;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldType;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldRequirement;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import net.morimekta.providence.idea.thrift.psi.ThriftKeyType;
import net.morimekta.providence.idea.thrift.psi.ThriftMessage;
import net.morimekta.providence.idea.thrift.psi.ThriftReferenceTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftServiceReturnType;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeReference;
import net.morimekta.providence.idea.thrift.psi.ThriftTypedef;
import net.morimekta.providence.idea.thrift.psi.ThriftTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.codeInsight.lookup.LookupElementBuilder.create;

public class ThriftTypeCompletionProvider extends BaseCompletionProvider {
    public static final ThriftTypeCompletionProvider INSTANCE = new ThriftTypeCompletionProvider();

    @Override
    protected void addCompletions(@NotNull CompletionParameters parameters,
                                  ProcessingContext context,
                                  @NotNull CompletionResultSet resultSet) {
        try {
            PsiElement   element = parameters.getPosition();
            // IElementType type    = type(element);
            if (element.getParent() instanceof PsiErrorElement) {
                if (element.getParent().getParent() instanceof ThriftFile) {
                    String prefix = getCompletePrefixString(element.getText());
                    maybeSuggest(prefix, resultSet, "namespace");
                    maybeSuggest(prefix, resultSet, "include");
                    maybeSuggest(prefix, resultSet, "enum");
                    maybeSuggest(prefix, resultSet, "struct");
                    maybeSuggest(prefix, resultSet, "union");
                    maybeSuggest(prefix, resultSet, "exception");
                    maybeSuggest(prefix, resultSet, "service");
                    maybeSuggest(prefix, resultSet, "const");
                    maybeSuggest(prefix, resultSet, "typedef");
                } else {
                    PsiElement beforeError = prevSibling(element.getParent());
                    if (type(beforeError) == ThriftTypes.ID) {
                        resultSet.addElement(create(": "));
                    }
                }
            } else if (element.getParent() instanceof ThriftReferenceTypeName &&
                       element.getParent().getParent() instanceof ThriftTypeReference) {
                String prefix = getCompletePrefixString(element.getText());

                ThriftTypeReference reference = (ThriftTypeReference) element.getParent().getParent();
                if (reference.getReferenceProgram() != null) {
                    ThriftFile file = (ThriftFile) element.getContainingFile();
                    // only types in that program.
                    ThriftFile thriftFile = file.getIncludedFileMap().get(reference.getProgramName());
                    if (thriftFile != null) {
                        suggestDeclaredTypesFromFile(prefix, resultSet, thriftFile, null);
                    }
                    return;
                }

                // requirement or type.
                if (reference instanceof ThriftKeyType) {
                    suggestPrimitives(prefix, resultSet);
                    suggestDeclaredTypes(prefix, resultSet, reference);
                } else {
                    PsiElement parent      = parent(reference);
                    if (parent instanceof ThriftFieldType) {
                        if (!(prevSibling(reference) instanceof ThriftFieldRequirement)) {
                            maybeSuggest(prefix, resultSet, "optional ");
                            maybeSuggest(prefix, resultSet, "required ");
                        }
                    } else if (parent instanceof ThriftServiceReturnType) {
                        maybeSuggest(prefix, resultSet, "oneway void ");
                        maybeSuggest(prefix, resultSet, "void ");
                    }
                    suggestPrimitives(prefix, resultSet);
                    suggestContainers(prefix, resultSet);
                    suggestDeclaredTypes(prefix, resultSet, reference);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getContainedInMessageType(PsiElement element) {
        if (element == null) return null;
        if (element instanceof ThriftMessage) {
            ThriftMessage message = ((ThriftMessage) element);
            if (message.getTypeName() != null) {
                return message.getTypeName().getText();
            }
        }
        return getContainedInMessageType(element.getParent());
    }

    private IElementType type(PsiElement element) {
        if (element == null) return null;
        else return element.getNode().getElementType();
    }

    private PsiElement prevSibling(PsiElement element) {
        if (element == null || element.getPrevSibling() == null) return null;
        while (element.getPrevSibling() instanceof PsiWhiteSpace) {
            element = element.getPrevSibling();
        }
        return element.getPrevSibling();
    }

    private PsiElement parent(PsiElement element) {
        if (element == null) return null;
        return element.getParent();
    }

    private void suggestPrimitives(String prefix, CompletionResultSet into) {
        maybeSuggest(prefix, into, PPrimitive.BOOL.getName());
        maybeSuggest(prefix, into, PPrimitive.BYTE.getName());
        maybeSuggest(prefix, into, "i8");
        maybeSuggest(prefix, into, PPrimitive.I16.getName());
        maybeSuggest(prefix, into, PPrimitive.I32.getName());
        maybeSuggest(prefix, into, PPrimitive.I64.getName());
        maybeSuggest(prefix, into, PPrimitive.DOUBLE.getName());
        maybeSuggest(prefix, into, PPrimitive.STRING.getName());
        maybeSuggest(prefix, into, PPrimitive.BINARY.getName());
    }

    private void suggestContainers(String prefix, CompletionResultSet into) {
        if ("list".startsWith(prefix)) {
            into.addElement(create("list").withTailText("<…>", true)
                                          .withInsertHandler(ThriftGenericTypeInsertHandler.INSERT_LIST_GENERIC));
        }
        if ("set".startsWith(prefix)) {
            into.addElement(create("set").withTailText("<…>", true)
                                         .withInsertHandler(ThriftGenericTypeInsertHandler.INSERT_LIST_GENERIC));
        }
        if ("map".startsWith(prefix)) {
            into.addElement(create("map").withTailText("<…,…>", true)
                                         .withInsertHandler(ThriftGenericTypeInsertHandler.INSERT_MAP_GENERIC));
        }
    }

    private void suggestDeclaredTypes(String prefix, CompletionResultSet into, ThriftTypeReference element) {
        String containedIn = getContainedInMessageType(element);
        ThriftFile file = (ThriftFile) element.getContainingFile();
        suggestDeclaredTypesFromFile(prefix, into, file, containedIn);
        file.getIncludedProgramNames().forEach(name -> maybeSuggest(prefix, into, name));
    }

    private void suggestDeclaredTypesFromFile(String prefix, CompletionResultSet into, ThriftFile file, String except) {
        file.getSpecList().forEach(spec -> {
            String name = spec.getDeclaredName();
            if (name == null) return;

            if (spec instanceof ThriftMessage) {
                if (name.startsWith(prefix) && !name.equals(except)) {
                    if (((ThriftMessage) spec).getMessageVariant().getText().equals("exception")) {
                        into.addElement(create(spec.getTypeNameElement())
                                                .withIcon(PlatformIcons.EXCEPTION_CLASS_ICON));
                    } else {
                        into.addElement(create(spec.getTypeNameElement())
                                                .withIcon(PlatformIcons.CLASS_ICON));
                    }
                }
            } else if (spec instanceof ThriftEnum) {
                if (name.startsWith(prefix)) {
                    into.addElement(create(spec.getTypeNameElement())
                                            .withIcon(PlatformIcons.ENUM_ICON));
                }
            } else if (spec instanceof ThriftTypedef) {
                if (name.startsWith(prefix)) {
                    into.addElement(create(spec.getTypeNameElement()));
                }
            }
        });
    }

    private void maybeSuggest(String prefix, CompletionResultSet into, String name) {
        if (name.startsWith(prefix)) {
            into.addElement(create(name));
        }
    }
}
