/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift.psi;

import com.intellij.lang.ASTNode;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFileFactory;
import net.morimekta.providence.idea.thrift.ThriftLanguage;

public class ThriftElementFactory {
    private static final String DUMMY_FILE_NAME = "dummy.thrift";

    public static ASTNode createFieldIdentifier(Project project, String name) {
        ThriftFile file = createDummyFile(project, "struct Dummy { 1: i32 " + name + " }");

        ThriftMessage messageSpec = (ThriftMessage) file.getSpecList().iterator().next();
        if (messageSpec != null &&
            messageSpec.getMessageFields() != null &&
            messageSpec.getMessageFields().getFields() != null) {
            ThriftFieldType field = messageSpec.getMessageFields().getFields().getMessageFieldList().iterator().next();
            if (field != null) {
                return field.getFieldName().getNode();
            }
        }
        return null;
    }

    public static ASTNode createTypeIdentifier(Project project, String name) {
        ThriftFile file = createDummyFile(project, "struct " + name + " {}");

        ThriftMessage messageSpec = (ThriftMessage) file.getSpecList().iterator().next();
        if (messageSpec != null && messageSpec.getTypeName() != null) {
            return messageSpec.getTypeName().getNode();
        }
        return null;
    }

    public static ASTNode createValueIdentifier(Project project, String name) {
        ThriftFile file = createDummyFile(project, "enum Dummy { " + name + " = 1 }");

        ThriftEnum enumSpec = (ThriftEnum) file.getFirstChild();
        if (enumSpec != null && enumSpec.getEnumValues() != null) {
            ThriftEnumValue value =  enumSpec.getEnumValues().getEnumValueList().iterator().next();
            if (value != null) {
                return value.getValueName().getNode();
            }
        }
        return null;
    }

    public static ThriftReferenceTypeName createReferenceTypeName(Project project, String name) {
        ThriftFile file = createDummyFile(project, "struct tmp { 1: " + name + " tmp }");
        ThriftMessage messageSpec = (ThriftMessage) file.getFirstChild();
        if (messageSpec != null &&
            messageSpec.getMessageFields() != null &&
            messageSpec.getMessageFields().getFields() != null) {
            ThriftType type = messageSpec.getMessageFields()
                                         .getFields()
                                         .getMessageFieldList()
                                         .iterator().next().getType();
            if (type instanceof ThriftTypeReference) {
                return ((ThriftTypeReference) type).getReferenceTypeName();
            }
        }
        return null;
    }

    public static ThriftReferenceValue createReferenceValue(Project project, String name) {
        ThriftFile file = createDummyFile(project, "const Dummy name = Dummy." + name);
        ThriftConst messageSpec = (ThriftConst) file.getFirstChild();
        if (messageSpec != null &&
            messageSpec.getConstValue() instanceof ThriftConstReference) {
            ThriftConstReference ref = (ThriftConstReference) messageSpec.getConstValue();
            return ref.getReferenceValue();
        }
        return null;
    }

    private static ThriftFile createDummyFile(Project project, String content) {
        return (ThriftFile) PsiFileFactory.getInstance(project).createFileFromText(
                DUMMY_FILE_NAME, ThriftLanguage.INSTANCE, content);
    }
}
