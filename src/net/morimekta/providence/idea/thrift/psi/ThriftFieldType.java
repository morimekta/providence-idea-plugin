package net.morimekta.providence.idea.thrift.psi;

import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ThriftFieldType extends PsiElement {

    @Nullable
    ThriftAnnotations getAnnotations();

    @Nullable
    ThriftConstValue getConstValue();

    @Nullable
    ThriftFieldId getFieldId();

    @NotNull
    ThriftFieldName getFieldName();

    @Nullable
    ThriftFieldRequirement getFieldRequirement();

    @NotNull
    ThriftType getType();
}
