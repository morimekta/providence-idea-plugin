/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift.psi;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.ImmutableSortedSet;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.TokenSet;
import net.morimekta.providence.idea.common.ProvidenceIdeaUtil;
import net.morimekta.providence.idea.thrift.ThriftFileType;
import net.morimekta.providence.idea.thrift.ThriftLanguage;
import net.morimekta.providence.reflect.util.ReflectionUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import javax.swing.*;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class ThriftFile extends PsiFileBase {
    private List<ThriftSpec>        specList;
    private Map<String, ThriftFile> includes;
    private String                  javaNamespace;
    private Set<String>             includedProgramNames;

    public ThriftFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, ThriftLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return ThriftFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Thrift IDL";
    }

    @Nullable
    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }

    @Override
    public void clearCaches() {
        super.clearCaches();
        specList = null;
        includes = null;
        javaNamespace = null;
        includedProgramNames = null;
    }

    public String getJavaNamespace() {
        if (javaNamespace == null) {
            for (ThriftNamespace ns : findChildrenByClass(ThriftNamespace.class)) {
                if (ns.getNamespaceLanguage().equals("java")) {
                    javaNamespace = ns.getNamespaceIdentifier();
                    break;
                }
            }
        }
        return javaNamespace;
    }

    public String getThriftProgramName() {
        return ReflectionUtils.programNameFromPath(getName());
    }

    public Set<String> getIncludedProgramNames() {
        if (includedProgramNames == null) {
            Set<String> fileMap = new TreeSet<>();
            for (ThriftInclude include : getIncludeList()) {
                String path        = include.getIncludedFilePath();
                String programName = ReflectionUtils.programNameFromPath(path);
                fileMap.add(programName);
            }
            includedProgramNames = ImmutableSortedSet.copyOf(fileMap);
        }
        return includedProgramNames;
    }

    public Map<String, ThriftFile> getIncludedFileMap() {
        if (includes == null) {
            Map<String, ThriftFile> fileMap   = new TreeMap<>();
            PsiDirectory            parentDir = getOriginalFile().getContainingDirectory();

            for (ThriftInclude include : getIncludeList()) {
                String path        = include.getIncludedFilePath();
                String programName = ReflectionUtils.programNameFromPath(path);

                PsiFile foundFile = ProvidenceIdeaUtil.resolveFile(parentDir, path);
                if (foundFile instanceof ThriftFile) {
                    fileMap.put(programName, (ThriftFile) foundFile);
                }
            }
            includes = ImmutableSortedMap.copyOf(fileMap);
        }
        return includes;
    }

    private TokenSet SPEC_TOKEN_SET = TokenSet.create(ThriftTypes.ENUM,
                                                      ThriftTypes.MESSAGE,
                                                      ThriftTypes.SERVICE,
                                                      ThriftTypes.CONST,
                                                      ThriftTypes.TYPEDEF);

    public List<ThriftSpec> getSpecList() {
        if (specList == null) {
            ImmutableList.Builder<ThriftSpec> specs = ImmutableList.builder();
            if (getTreeElement() != null) {
                for (ASTNode child : getTreeElement().getChildren(SPEC_TOKEN_SET)) {
                    if (child.getPsi() instanceof ThriftSpec) {
                        specs.add((ThriftSpec) child.getPsi());
                    }
                }
            } else if (getOriginalFile().getNode() != null) {
                for (ASTNode child : getOriginalFile().getNode().getChildren(SPEC_TOKEN_SET)) {
                    if (child.getPsi() instanceof ThriftSpec) {
                        specs.add((ThriftSpec) child.getPsi());
                    }
                }
            }
            specList = specs.build();
        }
        return specList;
    }

    public Set<String> getDeclaredTypesWithPrefix(String prefix) {
        TreeSet<String> typeNames = new TreeSet<>();
        for (ThriftSpec spec : getSpecList()) {
            if (spec instanceof ThriftEnum ||
                spec instanceof ThriftMessage ||
                spec instanceof ThriftTypedef) {
                addIfMatch(prefix, spec.getDeclaredName(), typeNames);
            }
        }
        return ImmutableSortedSet.copyOf(typeNames);
    }

    public Set<String> getDeclaredServicesWithPrefix(String prefix) {
        TreeSet<String> serviceNames = new TreeSet<>();
        for (ThriftSpec spec : getSpecList()) {
            if (spec instanceof ThriftService) {
                addIfMatch(prefix, spec.getDeclaredName(), serviceNames);
            }
        }
        return ImmutableSortedSet.copyOf(serviceNames);
    }

    /// --------------------------------------------

    private void addIfMatch(@Nonnull String prefix,
                            @Nullable String name,
                            @Nonnull Collection<String> collection) {
        if (name != null && (prefix.isEmpty() || name.startsWith(prefix))) {
            collection.add(name);
        }
    }

    private List<ThriftInclude> getIncludeList() {
        ImmutableList.Builder<ThriftInclude> specs = ImmutableList.builder();
        if (getTreeElement() != null) {
            for (ASTNode child : getTreeElement().getChildren(TokenSet.create(ThriftTypes.INCLUDE))) {
                if (child.getPsi() instanceof ThriftInclude) {
                    specs.add((ThriftInclude) child.getPsi());
                }
            }
        } else if (getOriginalFile().getNode() != null) {
            for (ASTNode child : getOriginalFile().getNode().getChildren(TokenSet.create(ThriftTypes.INCLUDE))) {
                if (child.getPsi() instanceof ThriftInclude) {
                    specs.add((ThriftInclude) child.getPsi());
                }
            }
        }
        return specs.build();
    }
}
