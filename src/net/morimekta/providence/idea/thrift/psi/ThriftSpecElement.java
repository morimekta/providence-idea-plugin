package net.morimekta.providence.idea.thrift.psi;

import com.intellij.psi.PsiElement;

public interface ThriftSpecElement extends PsiElement {
    default String getDeclaredName() {
        if (this instanceof ThriftConst) {
            ThriftConstName constName = ((ThriftConst) this).getConstName();
            if (constName != null) {
                return constName.getText();
            }
            return null;
        }
        ThriftTypeName typeName = getTypeNameElement();
        if (typeName != null) {
            return typeName.getText();
        }
        return null;
    }

    default ThriftTypeName getTypeNameElement() {
        if (this instanceof ThriftEnum) {
            return  ((ThriftEnum) this).getTypeName();
        } else if (this instanceof ThriftMessage) {
            return  ((ThriftMessage) this).getTypeName();
        } else if (this instanceof ThriftService) {
            return  ((ThriftService) this).getTypeName();
        } else if (this instanceof ThriftTypedef) {
            return  ((ThriftTypedef) this).getTypeName();
        }
        return null;
    }
}
