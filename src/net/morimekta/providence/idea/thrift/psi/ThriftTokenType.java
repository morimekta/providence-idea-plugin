/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift.psi;

import com.intellij.psi.tree.IElementType;
import net.morimekta.providence.idea.thrift.ThriftLanguage;
import org.jetbrains.annotations.NonNls;

import javax.annotation.Nonnull;
import java.util.Locale;

public class ThriftTokenType extends IElementType {
    public ThriftTokenType(@Nonnull @NonNls String debugName) {
        super(debugName, ThriftLanguage.INSTANCE);
    }

    public boolean isKeyword() {
        return super.toString().startsWith("KW_");
    }

    @Override
    public String toString() {
        if (super.toString().startsWith("KW_")) {
            return super.toString().substring(3).toLowerCase(Locale.US);
        }

        switch (super.toString()) {
            // Symbols
            case "SEMICOLON": return ";";
            case "COLON": return ":";
            case "COMMA": return ",";
            case "DOT": return ".";
            case "EQUALS": return "=";
            case "MAP_START": return "{";
            case "MAP_END": return "}";
            case "LIST_START": return "[";
            case "LIST_END": return "]";
            case "PARAM_START": return "(";
            case "PARAM_END": return ")";
            case "GENERIC_START": return "<";
            case "GENERIC_END": return ">";
            // Numbers:
            case "NUMBER":
            case "ID":
            case "ZERO":
                return super.toString().toLowerCase(Locale.US);
            case "LITERAL":
                return super.toString().toLowerCase(Locale.US);
            // ... Non-Symbols
            case "DOC_COMMENT": return "Documentation";
            case "LINE_COMMENT": return "Comment";
            case "IDENTIFIER": return "Identifier";
        }
        return "ThriftTokenType." + super.toString();
    }
}
