/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiElement;
import net.morimekta.providence.idea.thrift.ThriftIcon;
import net.morimekta.providence.idea.thrift.psi.ThriftConstReference;
import net.morimekta.providence.idea.thrift.psi.ThriftElementFactory;
import net.morimekta.providence.idea.thrift.psi.ThriftFieldName;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import net.morimekta.providence.idea.thrift.psi.ThriftFilePath;
import net.morimekta.providence.idea.thrift.psi.ThriftInclude;
import net.morimekta.providence.idea.thrift.psi.ThriftLanguageName;
import net.morimekta.providence.idea.thrift.psi.ThriftNamespace;
import net.morimekta.providence.idea.thrift.psi.ThriftNamespacePackage;
import net.morimekta.providence.idea.thrift.psi.ThriftReferenceTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftReferenceValue;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeName;
import net.morimekta.providence.idea.thrift.psi.ThriftTypeReference;
import net.morimekta.providence.idea.thrift.psi.ThriftTypes;
import net.morimekta.providence.idea.thrift.psi.ThriftValueName;
import net.morimekta.providence.reflect.util.ReflectionUtils;

import javax.annotation.Nonnull;
import javax.swing.*;

public class ThriftPsiImplUtil {
    @Nonnull
    public static String getIncludedFilePath(ThriftFilePath filePath) {
        ASTNode node = filePath.getNode().findChildByType(ThriftTypes.LITERAL);
        if (node != null) {
            String path = node.getText();
            if (path.startsWith("\"") && path.endsWith("\"")) {
                return path.substring(1, path.length() - 1);
            }
        }
        return "";
    }

    @Nonnull
    public static String getIncludedFilePath(ThriftInclude include) {
        if (include.getFilePath() != null) {
            return include.getFilePath().getIncludedFilePath();
        }
        return "";
    }

    @Nonnull
    public static String getNamespaceIdentifier(ThriftNamespace namespace) {
        ThriftNamespacePackage namespacePackage = namespace.getNamespacePackage();
        if (namespacePackage != null) {
            return namespacePackage.getText();
        }
        return "";
    }

    @Nonnull
    public static String getNamespaceLanguage(ThriftNamespace namespace) {
        ThriftLanguageName languageName = namespace.getLanguageName();
        if (languageName != null) {
            return languageName.getText();
        }
        return "";
    }

    public static PsiElement getNameIdentifier(PsiElement spec) {
        return spec;
    }

    // FIELD NAME

    public static String getName(ThriftFieldName spec) {
        return spec.getText();
    }

    public static PsiElement setName(ThriftFieldName spec, String name) {
        ASTNode replaceWith = ThriftElementFactory.createFieldIdentifier(spec.getProject(), name);
        if (replaceWith != null) {
            spec.getNode().replaceChild(spec.getNode().getFirstChildNode(), replaceWith);
        }
        return spec;
    }

    public static ItemPresentation getPresentation(ThriftFieldName spec) {
        return new ItemPresentation() {
            @Override
            public String getPresentableText() {
                return spec.getText();
            }

            @Override
            public String getLocationString() {
                return spec.getContainingFile().getName();
            }

            @Override
            public Icon getIcon(boolean b) {
                return ThriftIcon.FILE;
            }
        };
    }

    // INCLUDED FILE

    public static String getName(ThriftFilePath spec) {
        return spec.getText();
    }

    public static PsiElement setName(ThriftFilePath spec, String ignored) {
        return spec;
    }

    // TYPE NAME

    public static String getName(ThriftTypeName spec) {
        return spec.getText();
    }

    public static PsiElement setName(ThriftTypeName spec, String name) {
        ASTNode replaceWith = ThriftElementFactory.createTypeIdentifier(spec.getProject(), name);
        if (replaceWith != null) {
            spec.getNode().replaceChild(spec.getNode().getFirstChildNode(), replaceWith);
        }
        return spec;
    }

    public static ItemPresentation getPresentation(ThriftTypeName spec) {
        return new ItemPresentation() {
            @Override
            public String getPresentableText() {
                return spec.getText();
            }

            @Override
            public String getLocationString() {
                return spec.getContainingFile().getName();
            }

            @Override
            public Icon getIcon(boolean b) {
                return ThriftIcon.FILE;
            }
        };
    }

    // TYPE REFERENCE

    public static ThriftFile getProgramThriftFile(ThriftTypeReference spec) {
        ThriftFile file = (ThriftFile) spec.getContainingFile();
        if (spec.getReferenceProgram() == null) {
            return file;
        }
        return file.getIncludedFileMap().get(getProgramName(spec));
    }

    public static String getProgramName(ThriftTypeReference spec) {
        if (spec.getReferenceProgram() != null) {
            return spec.getReferenceProgram().getText();
        }
        ThriftFile file = (ThriftFile) spec.getContainingFile().getOriginalFile();
        return ReflectionUtils.programNameFromPath(file.getName());
    }

    public static String getTypeName(ThriftTypeReference spec) {
        return spec.getReferenceTypeName().getText();
    }

    public static ThriftFile getProgramThriftFile(ThriftConstReference spec) {
        ThriftFile file = (ThriftFile) spec.getContainingFile();
        if (spec.getReferenceProgram() == null) {
            return file;
        }
        return file.getIncludedFileMap().get(getProgramName(spec));
    }

    public static String getProgramName(ThriftConstReference spec) {
        if (spec.getReferenceProgram() != null) {
            return spec.getReferenceProgram().getText();
        }
        ThriftFile file = (ThriftFile) spec.getContainingFile().getOriginalFile();
        return ReflectionUtils.programNameFromPath(file.getName());
    }

    public static String getTypeName(ThriftConstReference spec) {
        return spec.getReferenceTypeName().getText();
    }

    // REFERENCE - TYPE NAME

    public static String getName(ThriftReferenceTypeName spec) {
        return spec.getText();
    }

    public static PsiElement setName(ThriftReferenceTypeName spec, String name) {
        ThriftReferenceTypeName replaceWith = ThriftElementFactory.createReferenceTypeName(spec.getProject(), name);
        if (replaceWith != null) {
            spec.replace(replaceWith);
        }
        return spec;
    }


    // VALUE NAME

    public static String getName(ThriftValueName spec) {
        return spec.getText();
    }

    public static PsiElement setName(ThriftValueName spec, String name) {
        ASTNode replaceWith = ThriftElementFactory.createValueIdentifier(spec.getProject(), name);
        if (replaceWith != null) {
            spec.getNode().replaceChild(spec.getNode().getFirstChildNode(), replaceWith);
        }

        return spec;
    }

    public static ItemPresentation getPresentation(ThriftValueName spec) {
        return new ItemPresentation() {
            @Override
            public String getPresentableText() {
                return spec.getText();
            }

            @Override
            public String getLocationString() {
                return spec.getContainingFile().getName();
            }

            @Override
            public Icon getIcon(boolean b) {
                return ThriftIcon.FILE;
            }
        };
    }

    // REFERENCE - VALUE NAME

    public static String getName(ThriftReferenceValue spec) {
        return spec.getText();
    }

    public static PsiElement setName(ThriftReferenceValue spec, String name) {
        ThriftReferenceValue replaceWith = ThriftElementFactory.createReferenceValue(spec.getProject(), name);
        if (replaceWith != null) {
            spec.replace(replaceWith);
        }

        return spec;
    }
}
