/*
 * Copyright 2018 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.idea.thrift.reference;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.ResolveResult;
import com.intellij.psi.impl.source.resolve.ResolveCache;
import net.morimekta.providence.idea.common.BaseReference;
import net.morimekta.providence.idea.thrift.psi.ThriftConstReference;
import net.morimekta.providence.idea.thrift.psi.ThriftEnum;
import net.morimekta.providence.idea.thrift.psi.ThriftEnumValue;
import net.morimekta.providence.idea.thrift.psi.ThriftFile;
import net.morimekta.providence.idea.thrift.psi.ThriftReferenceValue;
import net.morimekta.providence.idea.thrift.psi.ThriftSpec;
import net.morimekta.providence.idea.thrift.psi.ThriftValueName;
import net.morimekta.providence.reflect.util.ReflectionUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.ArrayList;

public class EnumValueReference extends BaseReference<ThriftReferenceValue> {
    private String programName;
    private String typeName;
    private String valueName;

    public EnumValueReference(@Nonnull ThriftReferenceValue element, ResolveCache cache) {
        super(element, cache);
    }

    @Override
    protected void update() {
        this.valueName = myElement.getText();
        setRangeInElement(new TextRange(0, valueName.length()));

        if (myElement.getParent() instanceof ThriftConstReference) {
            programName = ((ThriftConstReference) myElement.getParent()).getProgramName();
            typeName = ((ThriftConstReference) myElement.getParent()).getTypeName();
        } else {
            programName = ((ThriftFile) myElement.getContainingFile()).getThriftProgramName();
            typeName = "";
        }
    }

    @NotNull
    @Override
    public String getCanonicalText() {
        return programName + "." + typeName + "." + valueName;
    }

    @Override
    public boolean isReferenceTo(@Nonnull PsiElement element) {
        if (element instanceof ThriftValueName) {
            if (element.getText().equals(valueName)) {
                return super.isReferenceTo(element);
            }
        }
        return false;
    }

    @NotNull
    @Override
    protected ResolveResult[] multiResolveCached(boolean incomplete) {
        update();
        if (programName == null || typeName == null) {
            return ResolveResult.EMPTY_ARRAY;
        }

        ThriftFile file = (ThriftFile) myElement.getContainingFile();
        if (!programName.equals(ReflectionUtils.programNameFromPath(file.getName()))) {
            // different file...
            file = file.getIncludedFileMap().get(programName);
            if (file == null) {
                return ResolveResult.EMPTY_ARRAY;
            }
        }

        ArrayList<ResolveResult> result = new ArrayList<>();
        for (ThriftSpec spec : file.getSpecList()) {
            if (spec instanceof ThriftEnum) {
                ThriftEnum enumSpec = (ThriftEnum) spec;
                if (enumSpec.getTypeName() != null &&
                    enumSpec.getEnumValues() != null &&
                    enumSpec.getTypeName().getText().equals(typeName) ) {
                    for (ThriftEnumValue value : enumSpec.getEnumValues().getEnumValueList()) {
                        if (value.getValueName().getText().equals(valueName) ||
                            incomplete && value.getValueName().getText().startsWith(valueName)) {
                            result.add(new PsiElementResolveResult(value.getValueName()));
                        }
                    }
                }
            }
        }

        return result.toArray(ResolveResult.EMPTY_ARRAY);
    }
}
